"""
tbd
"""
from HeatEMS import *
import pyomo.core as pyomo
import pyomo.environ as env
import datetime
import time
os.environ['NEOS_EMAIL'] = 'tbd'


def gen_const_rule(m, tm, heat_pro, boundry):
    pro = m.Heat_dict["HeatProcess"][0]
    if m.global_prop_dict.loc['Model type', 'value'] == 'CLT':
        if heat_pro == pro:
            gen = sum(m.e_pro_in_heat[tm, heat_pro, level]
                      for level in m.heat_levels)
        else:
            return pyomo.Constraint.Skip
    elif m.global_prop_dict.loc['Model type', 'value'] == 'CLV':
        if heat_pro == pro:
            gen = m.e_pro_heat[tm, heat_pro]
        else:
            return pyomo.Constraint.Skip
    else:
        if heat_pro == pro:
            gen = m.e_pro_heat[tm, heat_pro]
        else:
            return pyomo.Constraint.Skip

    if boundry == 'upper':
        gen_max = m.process_dict['cap'][pro] * m.b_gen[tm] * m.dt
        return gen <= gen_max
    else:
        gen_min = m.process_dict['cap'][pro] * m.process_dict['min-fraction'][pro] * m.b_gen[tm] * m.dt
        return gen_min <= gen


def demand_const_rule(m, tm, pro):
    if m.global_prop_dict.loc['Model type', 'value'] == 'CLT':
        if pro in ['Heating', 'HeatDHW']:
            return sum(m.e_pro_out_heat[tm, pro, level]
                       for level in m.heat_levels) == m.demand_dict[pro][tm]
        else:
            return pyomo.Constraint.Skip
    elif m.global_prop_dict.loc['Model type', 'value'] == 'CLV':
        if pro in ['Heating', 'HeatDHW']:
            return m.e_pro_heat[tm, pro] == m.demand_dict[pro][tm]
        else:
            return pyomo.Constraint.Skip
    else:
        if pro in ['Heating', 'HeatDHW']:
            return m.e_pro_heat[tm, pro] == m.demand_dict[pro][tm]
        else:
            return pyomo.Constraint.Skip


def objective_rule(m):
    # minimize costs
    pro = m.Heat_dict["HeatProcess"][0]
    costs = 0
    if m.global_prop_dict.loc['Model type', 'value'] == 'CLT':
        for tm in m.tm:
            for level in m.heat_levels:
                costs += (m.e_pro_in_heat[tm, pro, level] * m.Heat_dict['EfficiencyNorm'].loc[pro, 'ratio'] *
                          m.cost_factor[tm])
    elif m.global_prop_dict.loc['Model type', 'value'] == 'CLV':
        for tm in m.tm:
            costs += m.e_pro_heat[tm, pro] * m.Heat_dict['EfficiencyNorm'].loc[pro, 'ratio'] * m.cost_factor[tm]
    else:
        for tm in m.tm:
            costs += m.e_pro_heat[tm, pro] * m.Heat_dict['EfficiencyNorm'].loc[pro, 'ratio'] * m.cost_factor[tm]
    return costs


def case_study(input_file, mod_type, n_level, milp):
    # read input file
    for n_lev in n_level:
        data = read_input(input_file, False)

        # change data from excel file to validation run specific data
        data['global_prop'].loc['Model type', 'value'] = mod_type

        # create model
        m = pyomo.ConcreteModel()

        m.dt = pyomo.Param(
            initialize=data['global_prop'].loc['dt', 'value'],
            doc='Time step duration (in hours)')

        m.global_prop_dict = data['global_prop']
        m.process_dict = data['process'].to_dict()
        m.demand_dict = data['demand'].to_dict()
        m.cost_factor = data['buy_sell_price']['Elec buy']
        m.mode = identify_mode(data)
        m = input_heat(m, data, n_lev)

        timesteps = range(data['global_prop'].loc['time offset', 'value'], data['global_prop'].loc['time offset', 'value'] +
                          data['global_prop'].loc['optimization length', 'value'] + 1)
        m.t = pyomo.Set(
            initialize=timesteps,
            ordered=True,
            doc='Set of timesteps')
        # modelled (i.e. excluding init time step for storage) time steps
        m.tm = pyomo.Set(
            within=m.t,
            initialize=timesteps[1:],
            ordered=True,
            doc='Set of modelled timesteps')

        m.solver = 'conopt'
        if mod_type == 'CLV':
            m = add_clv_model(m)
        elif mod_type == 'CLT':
            m = add_clt_model(m)
        else:
            m = add_mixed_storage_model(m)

        if milp:
            m.b_gen = pyomo.Var(
                m.tm,
                within=pyomo.Boolean,
                doc='b_gen[t] - Storage binary for on / off behavior of heat generator')
        else:
            m.b_gen = pyomo.Param(
                m.tm,
                initialize=1,
                doc='Binary variables deactivated for on / off behavior of heat generator')

        m.gen_const = pyomo.Constraint(
            m.tm, m.heat_pro, ['lower', 'upper'],
            rule=gen_const_rule,
            doc='Generation constraint: b_gen[t] * Q_min <= Q[t] <= b_gen[t] * Q_max')

        m.demand_const = pyomo.Constraint(
            m.tm, m.heat_pro,
            rule=demand_const_rule,
            doc='Demand time series')

        m.objective_function = pyomo.Objective(
            rule=objective_rule,
            sense=pyomo.minimize,
            doc='minimize costs')

        if mod_type == 'CLV':
            start = time.time()
            res = solve_model(m, 'conopt', neos=True)
            end = time.time()
            ashp = []
            for i in m.tm * m.heat_pro * m.heat_sto_port_connection:
                x = list(i)
                x.append(env.value(m.e_pro_heat_conn[i]))
                ashp.append(x)
            ashp = pd.DataFrame(ashp).set_index(1).drop(index=['HeatDHW', 'Heating']).set_index(2)
            ashp.columns = ['timesteps', 'Energy']
            timesteps = ashp['timesteps'].drop_duplicates().tolist()
            top = ashp.loc['top', 'Energy']
            top.index = timesteps
            bottom = ashp.loc['bottom', 'Energy']
            bottom.index = timesteps
            p_set = (top + bottom) / m.global_prop_dict.loc['dt', 'value'] / m.process_dict['cap']['ASHP']
            t_top = (top / (top + bottom)).fillna(0)

            t_sto = []
            for i in m.tm * m.heat_levels:
                x = list(i)
                x.append(env.value(m.heat_sto_temp_layer[i]))
                t_sto.append(x)
            t_sto = pd.DataFrame(t_sto).set_index(1)
            t_s = pd.DataFrame()
            for i in m.heat_levels:
                t_s.loc[:, i] = t_sto.loc[i, [0, 2]].set_index(0).rename(columns={2: i})
        elif mod_type == 'CLT':
            start = time.time()
            # res = solve_model(m, 'gurobi', seed=random.randint(1, 2 ** 31 - 1), miqp=True)
            res = solve_model(m, 'conopt', neos=True)
            end = time.time()
            bin = []
            for i in m.tm * m.heat_sto_conn_bin_set * m.heat_levels:
                x = list(i)
                x.append(env.value(m.heat_sto_conn_bin[i]))
                bin.append(x)
            port3 = pd.DataFrame(bin).set_index(1).drop(index=['Port0', 'Port1', 'Port2'])
            port2 = pd.DataFrame(bin).set_index(1).drop(index=['Port0', 'Port1', 'Port3'])
            ashp = []
            for i in m.tm * m.heat_gen_set * m.heat_levels:
                x = list(i)
                x.append(env.value(m.e_pro_in_heat[i]))
                ashp.append(x)
            ashp = pd.DataFrame(ashp).set_index(1)
            # ashp.columns = ['timesteps', 'Level', 'Energy']
            timesteps = ashp.iloc[:, 0].drop_duplicates().tolist()
            p_set = pd.Series(0.0, index=timesteps)
            t_top = pd.Series(0.0, index=timesteps)
            for t in timesteps:
                p = 0
                top = 0
                for i in range(0, n_lev):
                    p += ashp.iloc[(t - 1) * n_lev + i, 2]
                    top += ashp.iloc[(t - 1) * n_lev + i, 2] * port3.iloc[(t - 1) * n_lev + i, 2]
                p_set[t] = p / m.global_prop_dict.loc['dt', 'value'] / m.process_dict['cap']['ASHP']
                t_top[t] = top / t
        else:
            res = solve_model(m, 'gurobi', seed=random.randint(1, 2 ** 31 - 1), miqp=True)
            ashp = []
            for i in m.tm * m.heat_pro:
                x = list(i)
                x.append(env.value(m.e_pro_heat[i]))
                ashp.append(x)
            ashp = pd.DataFrame(ashp).set_index(1).drop(index=['HeatDHW', 'Heating'])
            ashp.columns = ['timesteps', 'Energy']
            timesteps = ashp['timesteps'].drop_duplicates().tolist()
            p_set = ashp['Energy'] / m.global_prop_dict.loc['dt', 'value'] / m.process_dict['cap']['ASHP']
            p_set.index = timesteps
            t_top = p_set * 0

            e_sto = []
            for i in m.tm:
                e_sto.append(env.value(m.e_heat_sto[i]))
            e_sto = pd.DataFrame(e_sto)

        setpoints = pd.DataFrame(columns=['time', 'p_set', 't_top'])
        dt = m.global_prop_dict.loc['dt', 'value']
        for i in range(t_top.index[0] - 1, t_top.index[0] + t_top.size - 1):
            setpoints.loc[2 * i, :] = [i * dt * 3600, p_set[i + 1], t_top[i + 1]]
            setpoints.loc[2 * i + 1, :] = [(i + 1) * dt * 3600 - 1, p_set[i + 1], t_top[i + 1]]

        set_str = setpoints.set_index('time').to_string()
        setpoints_str = '#1\ndouble	HeatPump(' + str(setpoints.index.size) + ',3)\n' + set_str[set_str.find('0.0'):]
        text_file = open('result//CaseStudy//' + mod_type + '_' + str(n_lev) + '_Layer.txt', 'w')
        text_file.write(setpoints_str)
        text_file.close()


model_type = 'CLV'  # 'CLV'  # 'mixed'
timestepsize = 300
file = 'Input\\Input_CaseStudy' + str(timestepsize) + 's.xlsx'
number_levels = [5]
integer = False
case_study(file, model_type, number_levels, integer)


def plot_setpoints(t_start=0, t_end=12, fsize=18):
    import matplotlib.pyplot as plt
    file_name = 'result\\CaseStudy\\Results.xlsx'
    tab = 'Setpoints'
    result = pd.read_excel(file_name, sheet_name=tab)

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(14, 2))
    ln = []
    for i in ['Demand', 'Price']:
        if i == 'Price':
            plot = pd.DataFrame(columns=['Time_' + i, i])
            factor = 1
        else:
            plot = pd.DataFrame(columns=['Time_' + i, i + '_Heating', i + '_DHW'])
            factor = 3
        for t in range(t_start * factor * 4, t_end * factor * 4):
            plot.loc[2 * t, :] = result.loc[t, plot.columns.tolist()].tolist()
            plot.loc[2 * t + 1, :] = result.loc[t, plot.columns.tolist()].tolist()
            plot.loc[2 * t + 1, 'Time_' + i] = result.loc[t + 1, 'Time_' + i]
        if i == 'Price':
            ax2 = ax.twinx()
            ln += ax2.plot(plot.set_index('Time_' + i), label='Price', linestyle='dashed', color='C0')
            ax2.set_ylabel('Price [pu]', fontsize=fsize)
            plt.yticks([1.0, 1.5, 2.0], fontsize=fsize)
        else:
            ln += ax.plot(plot.set_index('Time_' + i).loc[:, 'Demand_Heating'], label='Heating', linestyle='dotted',
                          color='C4')
            ln += ax.plot(plot.set_index('Time_' + i).loc[:, 'Demand_DHW'], label='DHW', color='C4')
            ax.set_ylabel('Demand [kW]', fontsize=fsize)
            plt.yticks([0.0, 1.0, 2.0], fontsize=fsize)
            plt.xticks(fontsize=fsize)
    ax.set_xticks([datetime.time(t_start, 0), datetime.time(int((t_end - t_start) / 4), 0),
                   datetime.time(int((t_end - t_start) / 2), 0), datetime.time(int((t_end - t_start) / 4 * 3), 0),
                   datetime.time(t_end, 0)])
    ax.set_xlabel('Time', fontsize=fsize)
    labs = [lab.get_label() for lab in ln]
    ax.legend(ln, labs, fontsize=fsize-4, loc=1)
    plt.title('Input', fontsize=fsize)
    plt.savefig('Setpoints.svg')
    fig.show()


# plot_setpoints()


def plot_results(lay='5', t_start=0, t_end=12, fsize=18):
    import matplotlib.pyplot as plt
    file_name = 'result\\CaseStudy\\Results.xlsx'
    power = pd.read_excel(file_name, sheet_name='Energy').loc[7:727, ['Unnamed: 0', 'Mixed TES', 'CLV - ' + lay +
                                                                      'Layer', 'CLT - ' + lay + 'Layer']]
    power = power.rename(columns={'Unnamed: 0': 'Time', 'Mixed TES': 'E_mixed', 'CLV - ' + lay + 'Layer': 'E_CLV',
                                  'CLT - ' + lay + 'Layer': 'E_CLT'})
    signals = pd.read_excel(file_name, sheet_name='Control').loc[:, ['Time', 'ControlIn_mixed', 'Overwrite_mixed',
                                                                     'ControlIn_CLV' + lay, 'Overwrite_CLV' + lay,
                                                                     'ControlIn_CLT' + lay, 'Overwrite_CLT' + lay]]
    signals = signals.rename(columns={'ControlIn_CLV' + lay: 'ControlIn_CLV', 'Overwrite_CLV' + lay: 'Overwrite_CLV',
                                      'ControlIn_CLT' + lay: 'ControlIn_CLT', 'Overwrite_CLT' + lay: 'Overwrite_CLT'})

    for i in ['mixed', 'CLV', 'CLT']:
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(14, 2))
        ln = []
        ln += ax.plot(power.set_index('Time').loc[:, 'E_' + i], label='Power', linestyle='dashed', color='C0')
        plt.ylim(0, 5)
        plt.xticks(fontsize=fsize)
        plt.yticks([0, 1, 2, 3, 4, 5], fontsize=fsize)
        ax2 = ax.twinx()
        ln += ax2.plot(signals.set_index('Time').loc[:, 'ControlIn_' + i], label='opt. Signal', linestyle='dotted',
                       color='C4')
        ln += ax2.plot(signals.set_index('Time').loc[:, 'Overwrite_' + i], label='res. Signal', color='C4')
        ax.set_ylabel('$P_{el}$ [kW]', fontsize=fsize)
        ax2.set_ylabel('Signal [-]', fontsize=fsize)
        plt.yticks([0, 0.5, 1], fontsize=fsize)
        ax.set_xticks([datetime.time(t_start, 0), datetime.time(int((t_end - t_start) / 4), 0),
                       datetime.time(int((t_end - t_start) / 2), 0), datetime.time(int((t_end - t_start) / 4 * 3), 0),
                       datetime.time(t_end, 0)])
        ax.set_xlabel('Time', fontsize=fsize)
        labs = [lab.get_label() for lab in ln]
        ax.legend(ln, labs, fontsize=fsize-4, loc=1)

        if i == 'mixed':
            plt.title('Mixed Thermal Energy Storage', fontsize=fsize)
        elif i == 'CLV':
            plt.title('Constant Layer Volume', fontsize=fsize)
        else:
            plt.title('Constant Layer Temperature', fontsize=fsize)

        plt.savefig('CaseStudy_' + i + '.svg')
        fig.show()


# plot_results()
