"""
tbd
"""
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from HeatEMS import *
import pyomo.core as pyomo


def run_validation(experiment_type, model_type, date, seed, set_dt=None, set_n_lev=None, solver='gurobi'):

    for mod_type in model_type:
        # prepare result file
        column1 = []
        column2 = []
        column3 = []
        for n in range(0, len(seed)):
            column1 += ['time_' + str(n)]
            column2 += ['MAE_' + str(n)]
            column3 += ['RMSE_' + str(n)]
        results = pd.DataFrame(float('nan'), index=[],
                               columns=['delta_t [s]', 'n_level', 'time [s]', 'MAE [K]', 'RMSE [K]']
                               + column1 + column2 + column3)

        if 'CLT' in mod_type:
            miqp = False
            dt = [300, 600, 900, 1800, 3600]
        else:
            dt = [30, 60, 90, 120, 150, 300, 600, 900, 1800]
            miqp = True
        if set_dt:
            dt = set_dt

        for exp_type in experiment_type:
            i = 0
            if exp_type == 'Dem':
                port = 'Port0'
                pro = 'dem'
            elif exp_type == 'CB':
                port = 'Port2'
                pro = 'gen'
            elif exp_type == 'HX':
                port = 'HeatExchanger0'
                pro = 'gen'
            else:
                port = 'Port3'
                pro = 'gen'

            if 'Simple' in mod_type and exp_type == 'CB':
                n_lev = [6, 12]
            elif 'Simple' in mod_type and exp_type == 'HP':
                n_lev = [9, 12]
            elif 'Simple' in mod_type and exp_type == 'Dem':
                n_lev = [9, 12]
            else:
                n_lev = range(2, 14)
            if set_n_lev:
                n_lev = set_n_lev

            for delta_t in dt:
                for n_level in n_lev:
                    results.loc[i, ['delta_t [s]', 'n_level']] = [delta_t, n_level]

                    prob = create_validation_model('Input\\Input_validation.xlsx', delta_t, n_level, port, mod_type,
                                                   exp_type)
                    timesteps = sorted(get_entity(prob, 'tm').index)
                    t_solve_avg = 0
                    mae_avg = 0
                    rmse_avg = 0
                    j = 0
                    for n in range(0, len(seed)):
                        prob_res = prob
                        prob_res, res = solve_model(prob_res, solver, seed=seed[n], miqp=miqp)
                        if res == 'optimal':
                            results.loc[i, 'time_' + str(n)] = prob_res.t_solve_model
                            results.loc[i, 'MAE_' + str(n)] = calc_t_out(prob, timesteps, exp_type, mod_type, pro,
                                                                         port=port)[2]
                            results.loc[i, 'RMSE_' + str(n)] = calc_t_out(prob, timesteps, exp_type, mod_type, pro,
                                                                          port=port)[3]
                            t_solve_avg += results.loc[i, 'time_' + str(n)]
                            mae_avg += results.loc[i, 'MAE_' + str(n)]
                            rmse_avg += results.loc[i, 'RMSE_' + str(n)]
                            j += 1
                    if j == 0:
                        results.loc[i, 'time [s]'] = float('nan')
                        results.loc[i, 'MAE [K]'] = float('nan')
                        results.loc[i, 'RMSE [K]'] = float('nan')
                    else:
                        results.loc[i, 'time [s]'] = t_solve_avg / j
                        results.loc[i, 'MAE [K]'] = mae_avg / j
                        results.loc[i, 'RMSE [K]'] = rmse_avg / j

                    results.to_excel('result\\Results_' + mod_type + '_' + exp_type + '_' + date + '.xlsx')
                    i += 1


def create_validation_model(input_file, delta_t, n_lev, port, model_type='CLV', exp_type='CB'):
    # create model
    m = pyomo.ConcreteModel()

    # mark that model mode is validation (deactivates heat_storage_state_const_cyclicity Constraint)
    m.validation = True

    # read input file
    data = read_input(input_file, False)

    # change data from excel file to validation run specific data
    data['global_prop'].loc['Model type', 'value'] = model_type
    data['global_prop'].loc['dt', 'value'] = delta_t / 3600
    [setpoints, delta_n, t_max, t_init] = create_validation_setpoints(delta_t, exp_type, n_lev)
    data['global_prop'].loc['optimization length', 'value'] = math.ceil(t_max / delta_t)
    data['heat'].iloc[1:11, 4] = t_init  # initial temperature of the validation experiment
    if exp_type == 'CB':
        if model_type == 'CLV':
            # CLV assumes constant deltaT
            data['process'].loc['SolarThermal', 'deltaT'] = 17.7
    elif exp_type == 'HP':
        if model_type == 'CLV':
            # CLV assumes constant deltaT
            data['process'].loc['SolarThermal', 'deltaT'] = 4.6
    elif exp_type == 'HX':
        if model_type == 'CLV':
            # CLV assumes constant deltaT
            data['process'].loc['SolarThermal', 'deltaT'] = 10
            # heat exchanger require to model buoyancy effect
            data['global_prop'].loc['Buoyancy'] = 'yes'
    elif exp_type == 'Dem':
        if model_type == 'CLV':
            # CLV assumes constant deltaT
            data['process'].loc['SolarThermal', 'deltaT'] = 10
    if exp_type != 'Dem':
        data['process'].loc['SolarThermal', 'connection-low'] = port
    dt = data['global_prop'].loc['dt', 'value']
    m.timesteps = range(data['global_prop'].loc['time offset', 'value'],
                        data['global_prop'].loc['time offset', 'value'] +
                        data['global_prop'].loc['optimization length', 'value'] + 1)

    # generate data for MPC model
    m.global_prop_dict = data['global_prop']
    m.process_dict = data['process'].to_dict()
    m.demand_dict = data['demand'].to_dict()
    m.mode = identify_mode(data)
    m = input_heat(m, data, n_lev)
    if exp_type == 'Dem':
        m.demand = setpoints * delta_t / 3600  # energy demand per timestep [kWh]
        m.generation = pd.Series(0, index=setpoints.index)
        m.Heat_dict['MaximumFlow'] = (setpoints / [20, 15]).dropna().max().max() * delta_t / 4.18 + 20
        # Maximum Flow: Maximum = P / Delta_T * dT / c_p + offset
    else:
        m.generation = setpoints['P_Heat'] * delta_t / 3600  # energy generation per timestep [kWh]
        m.demand = pd.DataFrame(0, index=setpoints.index, columns=['Heating', 'HeatDHW'])
        m.Heat_dict['Delta_n'] = delta_n
        m.Heat_dict['MaximumFlow'] = (setpoints['P_Heat'] / setpoints['Delta_T']).dropna().max() * delta_t / 4.18 + 20
        # Maximum Flow: Maximum = P / Delta_T * dT / c_p + offset

    m.dt = pyomo.Param(
        initialize=dt,
        doc='Time step duration (in hours)')
    # generate ordered time step sets
    m.t = pyomo.Set(
        initialize=m.timesteps,
        ordered=True,
        doc='Set of timesteps')
    # modelled (i.e. excluding init time step for storage) time steps
    m.tm = pyomo.Set(
        within=m.t,
        initialize=m.timesteps[1:],
        ordered=True,
        doc='Set of modelled timesteps')

    m.solver = 'gurobi'
    if m.global_prop_dict.loc['Model type', 'value'] == 'CLV':
        m = add_clv_model(m)
    else:
        if exp_type == 'CB':
            m.Heat_dict['ConnectionGenerator'].loc['SolarThermal', 'HeightOut'] = 0.15
        m = add_clt_model(m)
    m.heat_e_validation = pyomo.Constraint(
        m.tm, m.heat_pro,
        rule=heat_e_validation_rule,
        doc='Energy input/output for validation run')
    m.objective_function = pyomo.Objective(
        rule=objective_rule,
        sense=pyomo.maximize,
        doc='maximise average temperature at the top layer')
    return m


def create_validation_setpoints(delta_t, exp_type, n_level, t_high=80, t_low=20):
    temp_diff = (t_high - t_low) / (n_level - 1)
    df = pd.read_excel('Input\\Input_validation.xlsx', sheet_name='HeatStorageValidation')
    t_init = df.loc[:, exp_type + '_T_init [°C]'].dropna().tolist()
    if exp_type == 'Dem':
        df = df.loc[:, ['Dem_P_Heating [kW]', 'Dem_P_DHW [kW]']].dropna()
    else:
        df = df.loc[:, [exp_type + '_P_heat [kW]', exp_type + '_Delta_T [K]']].dropna()
    i = 0
    if exp_type == 'Dem':
        setpoints = pd.DataFrame(0.0, columns=['Heating', 'HeatDHW'], index=[])
    else:
        setpoints = pd.DataFrame(0.0, columns=['P_Heat', 'Delta_T'], index=[])
    setpoints.loc[0, :] = 0
    while ((i + 1) * delta_t) < df.index.max():
        avg_1 = 0
        avg_2 = 0
        for t in range(0, delta_t):
            if exp_type == 'Dem':
                avg_1 += df.loc[i * delta_t + t, 'Dem_P_Heating [kW]'] / delta_t
                avg_2 += df.loc[i * delta_t + t, 'Dem_P_DHW [kW]'] / delta_t
            else:
                avg_1 += df.loc[i * delta_t + t, exp_type + '_P_heat [kW]'] / delta_t
                avg_2 += df.loc[i * delta_t + t, exp_type + '_Delta_T [K]'] / delta_t
        setpoints.loc[i + 1, :] = [max(avg_1, 0), max(avg_2, 0)]
        i += 1
    avg_1 = 0
    avg_2 = 0
    for t in range(0, df.index.max() - i * delta_t + 1):
        if exp_type == 'Dem':
            avg_1 += df.loc[i * delta_t + t, 'Dem_P_Heating [kW]'] / delta_t
            avg_2 += df.loc[i * delta_t + t, 'Dem_P_DHW [kW]'] / delta_t
        else:
            avg_1 += df.loc[i * delta_t + t, exp_type + '_P_heat [kW]'] / delta_t
            avg_2 += df.loc[i * delta_t + t, exp_type + '_Delta_T [K]'] / (df.index.max() - i * delta_t)
    setpoints.loc[i + 1, :] = [max(avg_1, 0), max(avg_2, 0)]
    setpoints.loc[i + 2, :] = 0

    if exp_type == 'Dem':
        delta_n_pd = pd.DataFrame(columns=['Process', 't', 'Level', 'delta_n'])
    else:
        delta_n = []
        for lev in range(0, n_level):
            for t in setpoints.index:
                delta_n.append(['SolarThermal', lev, t, max(1, round(setpoints.loc[t, 'Delta_T'] / temp_diff))])
        delta_n_pd = pd.DataFrame(delta_n, columns=['Process', 't', 'Level', 'delta_n'])
    return setpoints, delta_n_pd.set_index(['Process', 't', 'Level']), df.index.max(), t_init


def heat_e_validation_rule(m, tm, heat_pro):
    if m.global_prop_dict.loc['Model type', 'value'] == 'CLV':
        if heat_pro == 'SolarThermal':
            return m.e_pro_heat[tm, heat_pro] == m.generation[tm]
        elif heat_pro in ['Heating', 'HeatDHW']:
            return m.e_pro_heat[tm, heat_pro] == m.demand.loc[tm, heat_pro]
        else:
            return m.e_pro_heat[tm, heat_pro] == 0
    else:
        if heat_pro == 'SolarThermal':
            return sum(m.e_pro_in_heat[tm, heat_pro, level]
                       for level in m.heat_levels) == m.generation[tm]
        elif heat_pro in ['Heating', 'HeatDHW']:
            return sum(m.e_pro_out_heat[tm, heat_pro, level]
                       for level in m.heat_levels) == m.demand.loc[tm, heat_pro]
        else:
            return sum(m.e_pro_in_heat[tm, heat_pro, level]
                       for level in m.heat_levels) == 0


def objective_rule(m):
    # objective: Maximize temperature at top layer over all timesteps
    if m.global_prop_dict.loc['Model type', 'value'] == 'CLV':
        return m.heat_sto_temp_layer[m.timesteps[-2], 'Level' + str(m.Heat_dict["NumberHeatLevels"] - 1)]
    else:
        return m.heat_sto_con[m.timesteps[-2], 'Level' + str(m.Heat_dict["NumberHeatLevels"] - 1)]


date = datetime.now().strftime('%Y%m%d')
number_runs = 10
seed = []
for n in range(0, number_runs):
    seed.append(random.randint(1, 2 ** 31 - 1))
model_type = ['CLV', 'CLT']
exp_type = ['CB', 'HP', 'Dem']

run_validation(exp_type, model_type, date, seed)


def plot_heatmap(content, experiment_type, model_type, date):
    for mod_type in model_type:
        for exp_type in experiment_type:
            for con in content:
                file_name = 'result\\Results_' + mod_type + '_' + exp_type + '_' + date + '.xlsx'
                results = pd.read_excel(file_name)
                results.loc[:, 'time_norm [-]'] = results.loc[:, 'time [s]'] / results.loc[:, 'time [s]'].min()
                index = results.drop_duplicates(subset=['delta_t [s]']).loc[:, 'delta_t [s]'].tolist()
                column = results.drop_duplicates(subset=['n_level']).loc[:, 'n_level'].tolist()
                df = pd.DataFrame(float('nan'), index=index, columns=column)
                result = results.set_index(['delta_t [s]', 'n_level'])
                for i in index:
                    for c in column:
                        df.loc[i, :] = result.loc[[i, c], con].tolist()

                fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 2.5))
                x = [30, 90, 150, 300, 600, 900, 1800]
                if mod_type == 'CLV':
                    v_max = 8
                    if exp_type == 'HX':
                        v_max = 80
                    #     df = df.iloc[:, :8]
                    df = df.loc[x, :]
                else:
                    v_max = 200
                    if exp_type == 'HX':
                        v_max = 6
                    #     df = df.iloc[:, :8]

                if 'time' in con:
                    if (mod_type == 'CLV' and exp_type != 'HX') or (exp_type == 'HX' and mod_type == 'CLT'):
                        im = ax.imshow(df[df.columns[::-1]].transpose(),
                                       vmin=1, vmax=v_max,
                                       cmap='coolwarm', aspect='auto')
                    else:
                        im = ax.imshow(df[df.columns[::-1]].transpose(),
                                       norm=colors.LogNorm(vmin=1, vmax=v_max),
                                       cmap='coolwarm', aspect='auto')
                else:
                    im = ax.imshow(df[df.columns[::-1]].transpose(),
                                   cmap='coolwarm', aspect='auto', vmin=0, vmax=15)
                ax.set_yticks(np.arange(len(df.columns)))
                ax.set_yticklabels(df.columns[::-1], fontsize=16)
                for label in ax.yaxis.get_ticklabels()[::2]:
                    label.set_visible(False)
                ax.set_ylabel('Number of Layers', fontsize=18)
                ax.tick_params(axis='x', labelsize=16)
                if mod_type == 'CLV':
                    ax.set_xticks(np.arange(len(x)))
                    ax.set_xticklabels(x)
                else:
                    ax.set_xticklabels([0, 300, 600, 900, 1800, 3600])
                ax.set_xlabel('Timestep Size [s]', fontsize=18)
                cbar = ax.figure.colorbar(im)
                if con == 'time_norm [-]' and exp_type != 'HX':
                    cbar.ax.set_ylabel('Norm. comp. time [-]', fontsize=18)
                    con_name = 'time'
                elif con == 'time_norm [-]' and exp_type == 'HX':
                    cbar.ax.set_ylabel('N. comp. time [-]', fontsize=18)
                    con_name = 'time'
                else:
                    if 'MAE' in con:
                        cbar.ax.set_ylabel('MAE [K]', fontsize=18)
                        con_name = 'MAE'
                    else:
                        cbar.ax.set_ylabel('RMSE [K]', fontsize=18)
                        con_name = 'RMSE'
                    if mod_type == 'CLT':
                        plt.title('Constant Layer Temperature\n', fontsize=18)
                    else:
                        plt.title('Constant Layer Volume\n', fontsize=18)
                cbar.ax.tick_params(labelsize=16)
                plt.savefig(mod_type + '_' + exp_type + '_' + con_name + '.svg')
                fig.show()


# content = ['RMSE [K]']  # ['MAE [K]', 'RMSE [K]', 'time_norm [-]']
# experiment_type = ['CB']  # ['Dem', 'HP', 'CB', 'HX']
# model_type = ['CLT', 'CLV']
# date = '20230627'
# plot_heatmap(content, experiment_type, model_type, date)


def plot_error_over_n(options, exp_type, type='rmse', fsize=16):
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 5))
    for i in range(0, options.index.max() + 1):
        results = pd.read_excel('result\\' + options.loc[i, 'File'])
        lab = options.loc[i, 'label']
        linest = options.loc[i, 'linestyle']
        mark = options.loc[i, 'marker']
        col = options.loc[i, 'color']
        results = results.set_index('delta_t [s]')
        if type == 'mae':
            res = results.loc[options.loc[i, 'delta_t'], 'MAE [K]']
        elif type == 'rmse':
            res = results.loc[options.loc[i, 'delta_t'], 'RMSE [K]']
        else:
            res = results.loc[options.loc[i, 'delta_t'], 'time [s]']
        ax.plot(results.loc[options.loc[i, 'delta_t'], 'n_level'], res, label=lab, linestyle=linest, marker=mark,
                markersize=10, color=col)
    if type == 'mae':
        mae_bm = pd.read_excel('result\\Benchmark.xlsx', sheet_name=exp_type).loc[0, 'MAE_benchmark']
        ax.plot([2, 13], [mae_bm, mae_bm], label='Benchmark', linewidth=2.0, color='C4')
        ax.set_ylabel('Mean Average Error [K]', fontsize=fsize + 3)
        if exp_type in ['CB', 'Dem']:
            plt.ylim([0, math.ceil(mae_bm) + 1])
        elif exp_type == 'HP':
            plt.ylim([0, math.ceil(mae_bm)])
        else:
            plt.ylim([0, 5.5])
        ax.legend(fontsize=fsize, loc=1)
    elif type == 'rmse':
        rmse_bm = pd.read_excel('result\\Benchmark.xlsx', sheet_name=exp_type).loc[0, 'RMSE_benchmark']
        ax.plot([2, 13], [rmse_bm, rmse_bm], label='Benchmark', linewidth=2.0, color='C4')
        ax.set_ylabel('RMSE [K]', fontsize=fsize + 3)
        if exp_type in ['CB', 'Dem']:
            plt.ylim([0, math.ceil(rmse_bm) + 1])
        elif exp_type == 'HP':
            plt.ylim([0, math.ceil(rmse_bm)])
        else:
            plt.ylim([0, 5.5])
        ax.legend(fontsize=fsize, loc=1)
    else:
        ax.set_ylabel('Computation Time [s]', fontsize=fsize + 3)
        ax.set_yscale('log')
        ax.legend(fontsize=fsize, loc=2)
    ax.set_xlabel('Number of Layers', fontsize=fsize + 3)

    plt.xticks(fontsize=fsize + 3)
    plt.yticks(fontsize=fsize + 3)
    if type == 'mae':
        plt.savefig('Comparison_MAE_' + exp_type + '.svg')
    elif type == 'rmse':
        plt.savefig('Comparison_RMSE_' + exp_type + '.svg')
    else:
        plt.savefig('Comparison_Time_' + exp_type + '.svg')
    fig.show()


# date = '20230627'
# model_type = ['CLT', 'CLV']
# experiment_type = ['CB', 'HP', 'Dem']  # , 'HX']
# for exp_type in experiment_type:
#     dt_clv = [90, 300]
#     dt_clt = [300, 900]
#     options = pd.DataFrame(columns=['n', 'File', 'delta_t', 'n_level', 'label', 'linestyle', 'marker', 'color'])
#     linestyle = ['dotted', 'dotted', 'dashed', 'dashed']
#     marker = ['x', 'D', 'o', 's']
#     color = ['saddlebrown', 'sandybrown', 'darkcyan', 'darkturquoise']
#     i = 0
#     for mod_type in model_type:
#         if 'CLT' in mod_type:
#             dt = dt_clt
#             name = 'CLT'
#         else:
#             dt = dt_clv
#             name = 'CLV'
#         for t in dt:
#             df = pd.DataFrame([[i, 'Results_' + mod_type + '_' + exp_type + '_' + date + '.xlsx', t, 'x',
#                                name + ', dt: ' + str(t) + ' s', linestyle[i], marker[i], color[i]]],
#                               columns=['n', 'File', 'delta_t', 'n_level', 'label', 'linestyle', 'marker', 'color'])
#             options = options.append(df)
#             i += 1
#     options = options.set_index('n')
#     plot_error_over_n(options, exp_type)


def plot_t_out_over_time(fsize=18):
    file_name = 'result\\T_out.xlsx'
    result = pd.read_excel(file_name)
    time = result.loc[:, 'Time']
    columns = ['T_exp_out', 'T_clv_out', 'T_clt_out', 'T_avg_out']
    label = ['Experiment', 'CLV', 'CLT', 'Mixed Storage Model']
    linest = ['solid', 'dotted', 'dashed', 'dashdot']
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 5))
    for i in range(0, len(columns)):
        ax.plot(time, result.loc[:, columns[i]], label=label[i], linestyle=linest[i], linewidth=2.0)
    ax.legend(fontsize=fsize)
    ax.set_ylabel('Outlet temperature [°C]', fontsize=fsize)
    ax.set_xlabel('Time [s]', fontsize=fsize)
    plt.xticks(fontsize=fsize)
    plt.yticks(fontsize=fsize)
    plt.savefig('T_out.svg')
    fig.show()


# plot_t_out_over_time()


def plot_options(options, exp_type, error_type='RMSE [K]', fsize=20):
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 5))
    min_time = 1
    max_time = 0
    for i in range(0, options.index.max() + 1):
        results = pd.read_excel('result\\' + options.loc[i, 'File'])
        lab = options.loc[i, 'label']
        linest = options.loc[i, 'linestyle']
        if isinstance(options.loc[i, 'delta_t'], str):
            results = results.set_index('n_level')
            time = results.loc[options.loc[i, 'n_level'], 'time [s]']
            error = results.loc[options.loc[i, 'n_level'], error_type]
        else:
            results = results.set_index('delta_t [s]')
            time = results.loc[options.loc[i, 'delta_t'], 'time [s]']
            error = results.loc[options.loc[i, 'delta_t'], error_type]
        min_time = min(min_time, time.min())
        max_time = max(max_time, time.max())
        ax.plot(time.iloc[:-1], error.iloc[:-1], label=lab, linestyle=linest, linewidth=4.0)
    mae_bm = pd.read_excel('result\\Benchmark.xlsx', sheet_name=exp_type).loc[0, 'MAE_benchmark']
    ax.plot([.9e-1, max_time], [mae_bm, mae_bm], label='Benchmark', linewidth=4.0)
    ax.set_ylabel(error_type, fontsize=fsize + 3)
    if exp_type != 'HX':
        plt.ylim([0, math.ceil(mae_bm)])
        plt.xlim(left=.8e-1)
    else:
        ax.set(xlim=(.8e-1, 1e1), ylim=(0, 4), yticks=(0, 1, 2, 3, 4))
        # plt.ylim([0, 4])
        # plt.xlim([.8e-1, 1e1])
    ax.set_xscale('log')
    ax.set_xlabel('Computational Time [s]', fontsize=fsize + 3)
    ax.legend(fontsize=fsize, loc=1)
    plt.xticks(fontsize=fsize + 3)
    plt.yticks(fontsize=fsize + 3)
    if exp_type == 'CB':
        plt.title('Condensing Boiler\n', fontsize=fsize + 5)
    elif exp_type == 'HP':
        plt.title('Heat Pump\n', fontsize=fsize + 5)
    elif exp_type == 'Dem':
        plt.title('Demand\n', fontsize=fsize + 5)
    plt.savefig('Comparison_' + exp_type + '.svg')
    fig.show()


# date = '20230627'
# model_type = ['CLT', 'CLV']
# experiment_type = ['Dem', 'CB', 'HP']  # , 'HX']
# for exp_type in experiment_type:
#     if exp_type == 'CB':
#         n_clt = [5, 13]
#         n_clv = [5, 13]
#     elif exp_type == 'Dem':
#         n_clt = [6, 13]
#         n_clv = [9, 13]
#     elif exp_type == 'HP':
#         n_clt = [9, 13]
#         n_clv = [7, 12]
#     else:
#         n_clt = [6, 13]
#         n_clv = [2, 6]
#     options = pd.DataFrame(columns=['n', 'File', 'delta_t', 'n_level', 'label', 'linestyle'])
#     linestyle = [(0, (1, 1)), (0, (5, 1)), (0, (3, 1, 1, 1)), (0, (3, 1, 1, 1, 1, 1))]
#     i = 0
#     for mod_type in model_type:
#         if 'CLT' in mod_type:
#             n_level = n_clt
#             name = 'CLT'
#         else:
#             n_level = n_clv
#             name = 'CLV'
#         for n in n_level:
#             df = pd.DataFrame([[i, 'Results_' + mod_type + '_' + exp_type + '_' + date + '.xlsx', 'x', n,
#                                name + ' ' + str(n) + ' Layers', linestyle[i]]],
#                               columns=['n', 'File', 'delta_t', 'n_level', 'label', 'linestyle'])
#             options = options.append(df)
#             i += 1
#     options = options.set_index('n')
#     plot_options(options, exp_type)


def plot_storage_temp(model_type, fsize=16):
    for mod_type in model_type:
        df1 = pd.read_excel('result\\T_Sto.xlsx', sheet_name=mod_type).set_index(['t', 'heat_levels'])
        x_label = [0, 0, 1500, 3000, 4500, 6000]

        if mod_type == 'CLV':
            df = pd.DataFrame(0, index=range(0, 25),
                              columns=['Level0', 'Level1', 'Level2', 'Level3', 'Level4', 'Level5'])
            for l in df.columns:
                for t in df.index:
                    df.loc[t, l] = df1.loc[t, l][0]
            size = (4.5, 3)
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=size)
            im = ax.imshow(df[df.columns[::-1]].transpose(),
                           vmin=20, vmax=80,
                           cmap='coolwarm', aspect='auto')
            ax.set_xticklabels(x_label, fontsize=fsize - 2)
            ax.set_xlabel('Time [s]', fontsize=fsize)
            plt.tick_params(left=False, labelleft = False)
            cbar = ax.figure.colorbar(im, ticks=[20, 32, 44, 56, 68, 80])
            cbar.ax.set_ylabel('Temperature [°C]', fontsize=fsize)
            cbar.ax.tick_params(labelsize=fsize - 3)
            plt.savefig('T_sto_CLV.svg')
        else:
            ind = list(range(0, 7500, 300)) + list(range(299, 7500, 300))
            ind.sort()
            df = pd.DataFrame(0, index=ind,
                              columns=['Level0', 'Level1', 'Level2', 'Level3', 'Level4', 'Level5'])
            for l in df.columns:
                for t in range(0, 25):
                    df.loc[ind[2 * t], l] = df1.loc[t, l][0]
                    df.loc[ind[2 * t + 1], l] = df1.loc[t, l][0]
            size = (3.60, 3)
            fig, ax = plt.subplots(nrows=1, ncols=1, figsize=size)
            cmap = plt.cm.get_cmap('coolwarm')
            col = [cmap(0.0), cmap(0.2), cmap(0.4), cmap(0.6), cmap(0.8), cmap(1.0)]
            ax.stackplot(df.index, df.transpose(), colors=col)
            ax.plot([0, 7500], [90, 90], 'g')
            ax.set(xlim=(0, 7500), xticks=x_label, ylim=(0, 785), yticks=[0, 785 / 2, 785])
            ax.set_xticklabels(x_label, fontsize=fsize-2)
            ax.set_xlabel('Time [s]', fontsize=fsize)
            ax.set_yticklabels([0, 50, 100], fontsize=fsize-2)
            ax.set_ylabel('Height [%]', fontsize=fsize)
            plt.savefig('T_sto_CLT.svg')

        fig.show()


# plot_storage_temp(['CLV', 'CLT'])
