# StraTS-E

StraTS-E is an optimisation model for unit commitment for distributed energy systems with different models of thermal energy storages (TES).
The structure is based on [urbs](https://github.com/tum-ens/urbs) and [ficus](https://github.com/tum-ewk/ficus).

## Thermal Energy Storage Models
* Constant Layer Volume: 1D TES model, where the TES consists of discretized layers with constant layer volume. Energy changes are modeled with varying layer temperatures, depending on inflows and outflows. The model is programmed with quadratic or simpler constraints.
* Constant Layer Temperature: 1D TES model, where the TES consists of discretized layers with constant layer temperature. Energy changes are modeled with varying layer volumes, depending on inflows and outflows. The model is programmed with linear constraints. To restrict heat exchange to layers that are connected to outlet ports, additional integer terms can be activated.
* Two Zone Storage Model: TES model with two zones for different temperature requirements (e.g. domestic hot water at 60°C and heating at 40°C). There is no exchange between the two thermal storages.
* Mixed Storage Model: TES model with one zone, the storage temperature depends on the energy content. Thermal dynamics are described purely by its total energy content and the power flows from losses, charging, and discharging.

## Getting started
* Install the environment (see the [urbs documentation](https://github.com/tum-ens/urbs#installation) for further information)
* Clone or download the repository
* Use the Input.xlsx file to define the optimization problem.

## Copyright
Copyright (C) 2023 TUM EMT

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>