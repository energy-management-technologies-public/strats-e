import pyomo.core as pyomo
import math
import pyomo.environ as aml


def add_clv_model(m):
    # Heat Levels
    indexlist = []
    i = 0
    while i < m.Heat_dict['NumberHeatLevels']:
        indexlist.append(('Level' + str(i),)[0])
        i += 1
    m.heat_levels = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat levels')

    # Heat Generation Type
    indexlist = []
    for key in m.Heat_dict["HeatProcess"]:
        indexlist.append('Heat' + key)
    m.heat_generation_type = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat generation types')

    # Heat Demand Type
    m.heat_demand_type = pyomo.Set(
        initialize=['Heating', 'HeatDHW'],
        doc='Set of heat demand types: Heating only or Heating and DHW')

    indexlist = ['Heating', 'HeatDHW'] + m.Heat_dict["HeatProcess"]
    if 'ElectricHeater0' in m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist():
        indexlist.remove('ElectricHeater')
    m.heat_pro = pyomo.Set(
        initialize=indexlist,
        doc='Set of processes')

    m = clv_heatstorage(m)
    m = clv_heatprocess(m)

    # coupling of heat storage and heat processes
    m.heat_sto_v_couple_const = pyomo.Constraint(
        m.tm, m.heat_sto_conn_set, ['sto_in', 'sto_out'],
        rule=heat_sto_v_couple_const_rule,
        doc='Coupling of volume flow of storage and heat process')

    m.heat_sto_t_couple_const = pyomo.Constraint(
        m.tm, m.heat_sto_conn_set, ['sto_in', 'sto_out'],
        rule=heat_sto_t_couple_const_rule,
        doc='Coupling of temperatures of storage and heat process')

    return m


def clv_heat_surplus(m, tm, com):
    power_surplus = 0
    if com in m.heat_generation_type:
        if 'HeatElectricHeater' == com:
            power_surplus -= m.e_pro_heat[tm, 'ElectricHeater']
        else:
            power_surplus -= m.e_pro_heat[tm, com.replace('Heat', '')]

    if com in m.heat_demand_type:
        power_surplus += m.e_pro_heat[tm, com]
    return power_surplus


# thermal storage
def clv_heatstorage(m):
    indexlist = list(dict.fromkeys(m.Heat_dict['ConnectionDemand'].loc[:, 'Port'].tolist() +
                                   m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist()))
    m.heat_sto_conn_set = pyomo.Set(
        initialize=indexlist,
        doc='Set of storage inlet and outlet connections')
    m.heat_sto_port_set = pyomo.Set(
        initialize=list(s for s in indexlist if 'Port' in s),
        doc='Set of storage inlet and outlet ports')
    m.heat_sto_cond_set = pyomo.Set(
        initialize=list(s for s in indexlist if 'Heater' in s) + list(s for s in indexlist if 'Exchanger' in s),
        doc='Set of storage inlet and outlet ports')
    m.heat_sto_dir_set = pyomo.Set(
        initialize=['positive', 'negative'],
        doc='Set to separate temperature or flow into positive and negative part')

    m.heat_sto_temp_layer = pyomo.Var(
        m.t, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='T[l,t] - Layer temperature [°C]')

    m.heat_sto_temp_in = pyomo.Var(
        m.tm, m.heat_sto_conn_set,
        within=pyomo.Reals,
        doc='T_p,in[t] - Inlet temperature of port [°C]')

    m.heat_sto_temp_out = pyomo.Var(
        m.t, m.heat_sto_conn_set,
        within=pyomo.Reals,
        doc='T_p,out[t] - Outlet temperature of port [°C]')

    m.heat_sto_temp_conv = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.Reals,
        doc='dT_conv[l,t] - Temperature change of the layer due to convection [K]')

    m.heat_sto_temp_cond = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.Reals,
        doc='dT_cond[l,t] - Temperature change of the layer due to conductive heat sources [K]')

    m.heat_sto_temp_buoy = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.Reals,
        doc='dT_buoy[l,t] - Temperature change of the layer due to buoyancy [K]')

    m.heat_sto_temp_loss = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.Reals,
        doc='dT_loss[l,t] - Temperature losses of the layer [K]')

    m.heat_sto_v_in = pyomo.Var(
        m.tm, m.heat_sto_conn_set,
        within=pyomo.NonNegativeReals,
        doc='V_p,in[t] - Heat flow into storage per timestep at port [l]')

    m.heat_sto_v_out = pyomo.Var(
        m.tm, m.heat_sto_conn_set,
        within=pyomo.NonNegativeReals,
        doc='V_p,out[t] - Heat flow out of storage per timestep at port [l]')

    m.heat_sto_layer_flow = pyomo.Var(
        m.tm, m.heat_levels,
        within=pyomo.Reals,
        doc='V_in/out[l,t] - Volume flow between layers [l]')

    m.heat_sto_layer_flow_dir = pyomo.Var(
        m.tm, m.heat_levels, m.heat_sto_dir_set,
        within=pyomo.NonNegativeReals,
        doc='V_in[l,t] / V_out[l,t] - Positive and negative volume flow between layers: '
            'V_in/out[l,t] = V_in[l,t] - V_out[l,t] [l]')

    m.heat_e_cond = pyomo.Var(
        m.tm, m.heat_sto_cond_set,
        within=pyomo.Reals,
        doc='Q_cond[t] - Conductive energy, that was exchanged with the thermal storage [kWh]')

    # storage rules
    m.heat_storage_state = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_storage_state_rule,
        doc='T[l,t] = T[l,t-1] + dT_conv[l,t] + dT_cond[l,t] + dT_buoy[l,t] - dT_loss[l,t]')

    m.heat_storage_initial_state = pyomo.Constraint(
        m.heat_levels,
        rule=heat_storage_initial_state_rule,
        doc='T[l,t_0] == storage.init * capacity')

    if not hasattr(m, 'validation'):
        m.heat_storage_state_cyclicity = pyomo.Constraint(
            rule=heat_storage_state_cyclicity_rule,
            doc='SOC[t_0] <= SOC[t_end]')

    # Convective constraints
    m.heat_sto_v_layer_const = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_sto_v_layer_const_rule,
        doc='V_p,in[t] - V_p,out[t] + V_in/out[l,t] +V_in/out[l+1,t] == 0 - Mass balance of layers')

    m.heat_sto_dv_sep_const = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_sto_dv_sep_const_rule,
        doc='V_in/out[l,t] = V_in[l,t] - V_out[l,t] - Separate the volume flow into its inflow and outflow')

    m.heat_sto_dv_const = pyomo.Constraint(
        m.tm, m.heat_levels, m.heat_sto_dir_set,
        rule=heat_sto_dv_const_rule,
        doc='V_in[l,t] * V_out[l,t] == 0')

    m.heat_sto_temp_conv_const = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_sto_temp_conv_const_rule,
        doc='dT_conv[l,t] == V_p,in[t] / V_layer * (T_p,in[t] - T[l,t-1]) + '
            'V_out[l+1,t] / V_layer * (T[l+1,t-1] - T[l,t-1]) + V_in[l,t] / V_layer * (T[l-1,t-1] - T[l,t-1])'
            'Convective temperature change of the layer, depending on the inflows')

    # Conductive constraints
    m.heat_e_cond_const = pyomo.Constraint(
        m.tm, m.heat_sto_cond_set,
        rule=heat_e_cond_const_rule,
        doc='E_cond[t] = V_hx_in[t] * rho * c_p * (T_hx_in[t] - T_hx_out[t]')

    m.heat_sto_temp_cond_const = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_sto_temp_cond_const_rule,
        doc='Conductive temperature change of the layer, '
            'dT_cond[l,t] = Q_cond[t] / (rho * c_p * V_layer) * h_act[l] / h_hx')

    # Buoyancy constraints
    if m.mode['buoy']:
        m.heat_sto_t_bouy_dir = pyomo.Var(
            m.tm, m.heat_levels, m.heat_sto_dir_set,
            within=pyomo.NonNegativeReals,
            doc='dT_buoy,pos/neg[l,t] Positive and negative temperature difference between layers: '
                'deltaT_buoy[l,t] = dT_buoy,pos[l,t] - dT_buoy,neg[l,t] [K]')

        m.heat_e_buoy = pyomo.Var(
            m.tm, m.heat_levels,
            within=pyomo.NonNegativeReals,
            doc='Q_buoy[l,t] - Buoyancy energy exchange between layers [kWh]')

        m.heat_sto_t_buoy_sep_const = pyomo.Constraint(
            m.tm, m.heat_levels,
            rule=heat_sto_t_buoy_sep_const_rule,
            doc='Separate the temperature difference between layers into its positive and negative part:'
                'dT_[l,t-1] - T_[l+1,t-1] = dT_buoy,pos[l,t] - dT_buoy,neg[l,t]')

        m.heat_sto_t_buoy_const = pyomo.Constraint(
            m.tm, m.heat_levels,
            rule=heat_sto_t_buoy_const_rule,
            doc='dT_buoy,pos/neg[l,t] <= b_pos/neg[l,t] * deltaT_buoy[l,t]')

        m.heat_e_buoy_const = pyomo.Constraint(
            m.tm, m.heat_levels,
            rule=heat_e_buoy_const_rule,
            doc='Q_buoy[l,t] = dT_buoy_pos[l,t] * const * dt')

    m.heat_sto_temp_buoy_const = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_sto_temp_buoy_const_rule,
        doc='Temperature change of the layer due to buoyancy'
            'dT_buoy[l,t] = (Q_buoy[l-1,t] - Q_buoy[l,t]) / (rho * c_p * V_layer')

    # Loss constraints
    m.heat_discharge = pyomo.Constraint(
        m.tm, m.heat_levels,
        rule=heat_discharge_rule,
        doc='dT_loss[t] = U * A_ins * (T[l,t-1] - T_amb[t]) * dt / (rho * cp * V_layer)')

    # Outlet temperature of the thermal storage
    m.heat_sto_temp_out_const = pyomo.Constraint(
        m.tm, m.heat_sto_conn_set,
        rule=heat_sto_temp_out_const_rule,
        doc='Outlet temperature of port,'
            'T_p,out[t] = T[l,t-1], '
            'T_hx,out[t] = Delta_T_hx + sum_(l=l_hx,out)^(l_hx,in) T[l,t-1]')
    return m


def heat_storage_state_rule(m, tm, level):
    # T_layer[t] = T_layer[t-1] + T_conv[t] + T_cond[t] + T_buoy[t] - T_losses[t]'
    return m.heat_sto_temp_layer[tm, level] == (m.heat_sto_temp_layer[tm - 1, level] + m.heat_sto_temp_conv[tm, level]
                                                + m.heat_sto_temp_cond[tm, level] + m.heat_sto_temp_buoy[tm, level]
                                                - m.heat_sto_temp_loss[tm, level])


def heat_storage_initial_state_rule(m, level):
    level = int(level.replace('Level', ''))
    return m.heat_sto_temp_layer[m.t[1], 'Level' + str(level)] == m.Heat_dict['StorageInit'].iloc[level]


def heat_storage_state_cyclicity_rule(m):
    t_0 = 0
    t_end = 0
    for level in m.heat_levels:
        t_0 += m.heat_sto_temp_layer[m.t[1], level]
        t_end += m.heat_sto_temp_layer[m.t[-1], level]
    return t_0 <= t_end


def heat_sto_v_layer_const_rule(m, tm, level):
    level = int(level.replace('Level', ''))
    v_in = 0
    v_out = 0
    for p in m.heat_sto_port_set:
        lev_port_in, lev_port_out = calc_lev_port(m, p)
        if lev_port_in == level:
            v_in += m.heat_sto_v_in[tm, p]
        if lev_port_out == level:
            v_out += m.heat_sto_v_out[tm, p]

    if 'Level' + str(level) == m.heat_levels[-1]:
        # Top level has no level above and thus no exchange to level above
        v_top = 0
    else:
        v_top = m.heat_sto_layer_flow[tm, 'Level' + str(level)]
    if level == 0:
        # Bottom level has no level below and thus no exchange to level below
        v_bottom = 0
    else:
        v_bottom = m.heat_sto_layer_flow[tm, 'Level' + str(level - 1)]

    return v_top == v_in + v_bottom - v_out


def heat_sto_dv_sep_const_rule(m, tm, level):
    return m.heat_sto_layer_flow[tm, level] == (m.heat_sto_layer_flow_dir[tm, level, 'positive'] -
                                                m.heat_sto_layer_flow_dir[tm, level, 'negative'])


def heat_sto_dv_const_rule(m, tm, level, direction):
    if m.solver == 'gurobi' or m.Heat_dict['NumberHeatLevels'] == 5:
        return m.heat_sto_layer_flow_dir[tm, level, direction] ** 2 <= m.heat_sto_layer_flow[tm, level] ** 2
    else:
        return m.heat_sto_layer_flow_dir[tm, level, 'positive'] * m.heat_sto_layer_flow_dir[tm, level, 'negative'] == 0


def calc_lev_port(m, p):
    try:
        h_in = m.Heat_dict['ConnectionDemand'].loc[m.Heat_dict['ConnectionDemand']['Port'] == p, 'HeightIn'][0]
        h_out = m.Heat_dict['ConnectionDemand'].loc[m.Heat_dict['ConnectionDemand']['Port'] == p, 'HeightOut'][0]
    except IndexError:
        h_in = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), p), 'HeightIn'][0]
        h_out = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), p), 'HeightOut'][0]
    lev_port_in = min(int(h_in * (m.Heat_dict["NumberHeatLevels"]) / m.Heat_dict['StorageHeight']),
                      m.Heat_dict["NumberHeatLevels"] - 1)
    lev_port_out = min(int(h_out * (m.Heat_dict["NumberHeatLevels"]) / m.Heat_dict['StorageHeight']),
                       m.Heat_dict["NumberHeatLevels"] - 1)
    return lev_port_in, lev_port_out


def heat_sto_temp_conv_const_rule(m, tm, level):
    level = int(level.replace('Level', ''))

    v_layer = m.Heat_dict['StorageSize'] / (m.Heat_dict["NumberHeatLevels"])  # [l]
    t_layer = m.heat_sto_temp_layer[tm - 1, 'Level' + str(level)]  # [°C]

    t_in = 0
    for p in m.heat_sto_port_set:
        lev_port_in = calc_lev_port(m, p)[0]
        if lev_port_in == level:
            t_in += m.heat_sto_v_in[tm, p] * (m.heat_sto_temp_in[tm, p] - t_layer) / v_layer  # [K]

    if 'Level' + str(level) == m.heat_levels[-1]:
        # Top level has no level above and thus no exchange to level above
        t_in_top = 0  # [K]
        v_in_top = 0  # [l]
    else:
        t_in_top = m.heat_sto_temp_layer[tm - 1, 'Level' + str(level + 1)] - t_layer  # [K]
        v_in_top = m.heat_sto_layer_flow_dir[tm, 'Level' + str(level), 'negative']  # [l]

    if level == 0:
        # Bottom level has no level below and thus no exchange to level below
        t_in_bottom = 0  # [K]
        v_in_bottom = 0  # [l]
    else:
        t_in_bottom = m.heat_sto_temp_layer[tm - 1, 'Level' + str(level - 1)] - t_layer  # [K]
        v_in_bottom = m.heat_sto_layer_flow_dir[tm, 'Level' + str(level - 1), 'positive']  # [l]

    # [K] = ([K] - [l] / [l] * [K] + [l] / [l] * [K]
    return m.heat_sto_temp_conv[tm, 'Level' + str(level)] == (t_in + v_in_top / v_layer * t_in_top +
                                                              v_in_bottom / v_layer * t_in_bottom)


def heat_e_cond_const_rule(m, tm, conn):
    rho = 1  # kg / l
    c_p = 4.18 / 3600  # kWh / kg K
    return m.heat_e_cond[tm, conn] == m.heat_sto_v_in[tm, conn] * rho * c_p * (m.heat_sto_temp_in[tm, conn] -
                                                                               m.heat_sto_temp_out[tm, conn])


def heat_sto_temp_cond_const_rule(m, tm, level):
    # dT_cond[l,t] = Q_cond[t] / (rho * c_p * V_layer) * h_act[l] / h_hx
    level = int(level.replace('Level', ''))
    e_cond = 0
    for cond in m.heat_sto_cond_set:
        h_in = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), cond), 'HeightIn'][0]
        h_out = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), cond), 'HeightOut'][0]
        h_layer_m1 = level * m.Heat_dict['StorageHeight'] / m.Heat_dict['NumberHeatLevels']
        h_layer = (level + 1) * m.Heat_dict['StorageHeight'] / m.Heat_dict['NumberHeatLevels']
        h_hx = h_in - h_out  # m
        h_act = max(0, min(h_layer, h_in) - max(h_layer_m1, h_out))  # m
        e_cond += m.heat_e_cond[tm, cond] * h_act / h_hx  # kWh
    v_layer = m.Heat_dict['StorageSize'] / (m.Heat_dict["NumberHeatLevels"])  # l
    rho = 1  # kg / l
    c_p = 4.18 / 3600  # kWh / kg K
    return m.heat_sto_temp_cond[tm, 'Level' + str(level)] == e_cond / (rho * c_p * v_layer)


def heat_sto_t_buoy_sep_const_rule(m, tm, level):
    level = int(level.replace('Level', ''))
    if 'Level' + str(level) != m.heat_levels[-1]:
        t_buoy = (m.heat_sto_temp_layer[tm - 1, 'Level' + str(level)] -
                  m.heat_sto_temp_layer[tm - 1, 'Level' + str(level + 1)])
    else:
        t_buoy = 0
    return t_buoy == (m.heat_sto_t_bouy_dir[tm, 'Level' + str(level), 'positive'] -
                      m.heat_sto_t_bouy_dir[tm, 'Level' + str(level), 'negative'])


def heat_sto_t_buoy_const_rule(m, tm, level):
    return m.heat_sto_t_bouy_dir[tm, level, 'positive'] * m.heat_sto_t_bouy_dir[tm, level, 'negative'] == 0


def heat_e_buoy_const_rule(m, tm, level):
    buoyancy_constant = m.Heat_dict["BuoyancyConstant"]  # kW / K
    return m.heat_e_buoy[tm, level] == buoyancy_constant * m.dt.value * m.heat_sto_t_bouy_dir[tm, level, 'positive']


def heat_sto_temp_buoy_const_rule(m, tm, level):
    if not m.mode['buoy']:
        return m.heat_sto_temp_buoy[tm, level] == 0
    level = int(level.replace('Level', ''))
    e_buoy = 0
    if level > 0:
        e_buoy += m.heat_e_buoy[tm, 'Level' + str(level - 1)]  # kWh
    if 'Level' + str(level) != m.heat_levels[-1]:
        e_buoy -= m.heat_e_buoy[tm, 'Level' + str(level)]  # kWh
    v_layer = m.Heat_dict['StorageSize'] / (m.Heat_dict["NumberHeatLevels"])  # l
    rho = 1  # kg / l
    c_p = 4.18 / 3600  # kWh / kg K
    return m.heat_sto_temp_buoy[tm, 'Level' + str(level)] == e_buoy / (rho * c_p * v_layer)


def heat_discharge_rule(m, tm, level):
    # T_losses[t] = U * A * (T_layer[t-1] - T_amb) * dt / (rho * c_p * V_layer)
    u = 0.35  # [W/m^2K]
    d = math.sqrt(4 * m.Heat_dict['StorageSize'] / 1000 / m.Heat_dict['StorageHeight'] / math.pi)  # [m]
    a = d * math.pi * m.Heat_dict['StorageHeight'] / m.Heat_dict['NumberHeatLevels']  # [m^2]
    if level == 'Level' + str(m.Heat_dict['NumberHeatLevels'] - 1):
        # top layer has additional losses over the top surface
        a += d ** 2 * math.pi / 4
    t_amb = 18  # [°C]
    rho = 1  # kg / l
    c_p = 4.18 * 1000  # Ws / kg K
    dt = m.dt.value * 3600  # s
    v_layer = m.Heat_dict['StorageSize'] / m.Heat_dict['NumberHeatLevels']  # [l]
    # [K] = [W/m^2K] * [m^2] * [K] * [s] / ([kg/l] * [Ws/kgK] * [l]
    return m.heat_sto_temp_loss[tm, level] == u * a * (m.heat_sto_temp_layer[tm - 1, level] - t_amb) * dt / (rho * c_p *
                                                                                                             v_layer)


def heat_sto_temp_out_const_rule(m, tm, port):
    if port in m.Heat_dict['ConnectionDemand']['Port'].tolist():
        h = m.Heat_dict['ConnectionDemand'].loc[m.Heat_dict['ConnectionDemand']['Port'] == port, 'HeightOut'].iloc[0]
    else:
        h = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), port), 'HeightOut'][0]
    level = min(int(h * (m.Heat_dict["NumberHeatLevels"]) / m.Heat_dict['StorageHeight']),
                m.Heat_dict["NumberHeatLevels"] - 1)

    if 'HeatExchanger' in port:
        delta_t = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), port), 'DeltaT_out'][0]  # [K]
        h_in = m.Heat_dict['ConnectionGenerator'].loc[(slice(None), port), 'HeightIn'][0]
        level_in = min(int(h_in * (m.Heat_dict["NumberHeatLevels"]) / m.Heat_dict['StorageHeight']),
                       m.Heat_dict["NumberHeatLevels"] - 1)
        t_out = sum(m.heat_sto_temp_layer[tm - 1, 'Level' + str(lev)]
                    for lev in range(level, level_in + 1)) / (level_in + 1 - level) + delta_t
    else:
        t_out = m.heat_sto_temp_layer[tm - 1, 'Level' + str(level)]

    return m.heat_sto_temp_out[tm, port] == t_out


# heat process
def clv_heatprocess(m):
    m.heat_sto_port_connection = pyomo.Set(
        initialize=['top', 'bottom'],
        doc='Set of connections, process can be connected to multiple ports')

    m.heat_pro_temp_out = pyomo.Var(
        m.tm, m.heat_pro, m.heat_sto_port_connection,
        within=pyomo.Reals,
        doc='T_g,sup[t] / T_d,ret[t] - Outlet temperature of process (°C) per timestep')

    m.heat_pro_temp_in = pyomo.Var(
        m.tm, m.heat_pro, m.heat_sto_port_connection,
        within=pyomo.Reals,
        doc='T_g,ret[t] / T_d,sup[t] - Inlet temperature of process (°C) per timestep')

    m.heat_pro_flow = pyomo.Var(
        m.tm, m.heat_pro, m.heat_sto_port_connection,
        within=pyomo.NonNegativeReals,
        doc='V_g/d,conn[t] - Water flow into heat generator (l) per timestep')

    m.e_pro_heat_conn = pyomo.Var(
        m.tm, m.heat_pro, m.heat_sto_port_connection,
        within=pyomo.NonNegativeReals,
        doc='Q_g/d,conn[t] - Heat (kWh) through process at top or bottom port')

    m.e_pro_heat = pyomo.Var(
        m.tm, m.heat_pro,
        within=pyomo.NonNegativeReals,
        doc='Q_g/d[t] - Heat (kWh) of generation type (e.g. CHP) per timestep [kWh]')

    m.heat_pro_flow_const = pyomo.Constraint(
        m.tm, m.heat_pro, m.heat_sto_port_connection,
        rule=heat_pro_flow_const_rule,
        doc='Q_g/d,conn[t] = V_g/d,conn[t] * rho * c_p * DeltaT * eta_g')

    m.heat_pro_temp_out_const = pyomo.Constraint(
        m.tm, m.heat_pro, m.heat_sto_port_connection,
        rule=heat_pro_temp_out_const_rule,
        doc='T_g/d,sup[t] = T_g/d,ret[t] +/- DeltaT - outlet temperature of heat process')

    m.heat_pro_connection_const = pyomo.Constraint(
        m.tm, m.heat_pro, m.heat_sto_port_connection,
        rule=heat_pro_connection_const_rule,
        doc='If process has only one connection, the flow through the bottom connection is 0')

    m.heat_pro_distribution = pyomo.Constraint(
        m.tm, m.heat_pro,
        rule=heat_pro_distribution_rule,
        doc='Q_g/d[t] = Q_g/d,top[t] + Q_g/d,bottom[t] (more connections would be possible as well')

    if not hasattr(m, 'validation'):
        m.heat_pro_temp_out_max_const = pyomo.Constraint(
            m.tm, m.heat_pro, m.heat_sto_port_connection,
            rule=heat_pro_temp_out_max_const_rule,
            doc='T_g,ret[t] * V_g[t] <= T_g,ret_max[t] * V_g[t]; T_d,sup[t] * V_d[t] >= T_d,sup_min[t] * V_d[t] - '
                'maximum / minimum temperature constraint')
    return m


def heat_pro_flow_const_rule(m, tm, heat_pro, connection):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    if heat_pro in ['Heating', 'HeatDHW']:
        delta_temp = (m.Heat_dict[heat_pro + 'SupplyTemp'] - m.Heat_dict[heat_pro + 'RetTemp']) + 5  # [K]
        efficiency = 1
    elif 'HeatExch' in connection:
        delta_temp = 20  # [K]
        efficiency = 1  # [-]
    else:
        delta_temp = m.process_dict['deltaT'][heat_pro]  # [K]
        if hasattr(m, 'validation'):
            efficiency = m.Heat_dict['Efficiency'].loc[heat_pro][0][0]  # [-]
        else:
            efficiency = 0
            for i in m.Heat_dict['Efficiency'].loc[heat_pro].index:
                efficiency += (m.Heat_dict['Efficiency'].loc[heat_pro][i][0] /
                               m.Heat_dict['Efficiency'].loc[heat_pro].index.size)
            efficiency = ((7.392 - 0.0762 * m.heat_pro_temp_out[tm, heat_pro, connection]) /
                          m.Heat_dict['EfficiencyNorm'].loc[heat_pro, 'ratio'])
    # [l] = [kWh] * [-] / ([kg/l] * [kWh / kgK] * [K])
    return (m.heat_pro_flow[tm, heat_pro, connection] ==
            m.e_pro_heat_conn[tm, heat_pro, connection] * efficiency / (rho * c_p * delta_temp))


def heat_pro_temp_out_const_rule(m, tm, heat_pro, connection):
    if heat_pro in ['Heating', 'HeatDHW']:
        delta_temp = -(m.Heat_dict[heat_pro + 'SupplyTemp'] - m.Heat_dict[heat_pro + 'RetTemp'] + 5)  # [K]
    else:
        delta_temp = m.process_dict['deltaT'][heat_pro]  # [K]
    return m.heat_pro_temp_out[tm, heat_pro, connection] == m.heat_pro_temp_in[tm, heat_pro, connection] + delta_temp


def heat_pro_connection_const_rule(m, tm, heat_pro, connection):
    if heat_pro in m.Heat_dict['ConnectionDemand'].index:
        no_conn = True
    elif len(m.Heat_dict['ConnectionGenerator'].loc[heat_pro, :]) < 2:
        no_conn = True
    else:
        no_conn = False

    if no_conn and connection == 'bottom':
        return m.e_pro_heat_conn[tm, heat_pro, connection] == 0
    else:
        return pyomo.Constraint.Skip


def heat_pro_distribution_rule(m, tm, heat_pro):
    return m.e_pro_heat[tm, heat_pro] == (m.e_pro_heat_conn[tm, heat_pro, 'bottom'] +
                                          m.e_pro_heat_conn[tm, heat_pro, 'top'])


def heat_pro_temp_out_max_const_rule(m, tm, heat_pro, connection):
    if heat_pro in ['Heating', 'HeatDHW']:
        return (m.e_pro_heat_conn[tm, heat_pro, connection] * m.heat_pro_temp_in[tm, heat_pro, connection] >=
                m.e_pro_heat_conn[tm, heat_pro, connection] * m.Heat_dict['ConnectionDemand'].loc[heat_pro, 'T_min'])
    else:
        return (m.e_pro_heat_conn[tm, heat_pro, connection] * m.heat_pro_temp_out[tm, heat_pro, connection] <=
                m.e_pro_heat_conn[tm, heat_pro, connection] *
                m.Heat_dict['ConnectionGenerator'].loc[heat_pro, 'T_max'][0])


# coupling between thermal storage and heat process
def heat_sto_v_couple_const_rule(m, tm, conn, direction):
    [heat_pro, connection] = find_coupling(m, conn)

    if direction == 'sto_in':
        return m.heat_sto_v_in[tm, conn] == m.heat_pro_flow[tm, heat_pro, connection]
    elif direction == 'sto_out':
        return m.heat_sto_v_out[tm, conn] == m.heat_pro_flow[tm, heat_pro, connection]


def heat_sto_t_couple_const_rule(m, tm, conn, direction):
    [heat_pro, connection] = find_coupling(m, conn)

    if direction == 'sto_in':
        return m.heat_sto_temp_in[tm, conn] == m.heat_pro_temp_out[tm, heat_pro, connection]
    elif direction == 'sto_out':
        return m.heat_sto_temp_out[tm, conn] == m.heat_pro_temp_in[tm, heat_pro, connection]


def find_coupling(m, port):
    for heat_pro in m.heat_pro:
        for connection in m.heat_sto_port_connection:
            if heat_pro in m.Heat_dict['ConnectionDemand'].index and connection == 'top':
                port_pro = m.Heat_dict['ConnectionDemand'].loc[heat_pro, 'Port']
            elif heat_pro in m.Heat_dict['ConnectionDemand'].index:
                port_pro = -1
            elif connection == 'bottom':
                port_pro = m.Heat_dict['ConnectionGenerator'].loc[heat_pro, :].index[0]
            else:
                port_pro = m.Heat_dict['ConnectionGenerator'].loc[heat_pro, :].index[-1]

            if port_pro == port:
                return heat_pro, connection
