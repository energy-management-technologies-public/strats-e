import pyomo.core as pyomo


def add_two_zone_storage_model(m):
    # Heat Generation Type
    indexlist = []
    for key in m.Heat_dict["HeatProcess"]:
        indexlist.append('Heat' + key)
    m.heat_generation_type = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat generation types')

    # Heat Demand Type
    m.heat_demand_type = pyomo.Set(
        initialize=['Heating', 'HeatDHW'],
        doc='Set of heat demand types: Heating only or Heating and DHW')

    indexlist = ['Heating', 'HeatDHW'] + m.Heat_dict["HeatProcess"]
    if 'ElectricHeater0' in m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist():
        indexlist.remove('ElectricHeater')
    m.heat_pro = pyomo.Set(
        initialize=indexlist,
        doc='Set of processes')

    m.zone = pyomo.Set(
        initialize=['top', 'bottom'],
        doc='top or bottom part of the thermal storage')

    m.e_pro_heat = pyomo.Var(
        m.tm, m.heat_pro,
        within=pyomo.NonNegativeReals,
        doc='Q_g[t] - Heat (kWh) of generation type (e.g. CHP) per timestep [kWh]')

    m.heat_gen = pyomo.Var(
        m.tm, m.zone,
        within=pyomo.NonNegativeReals,
        doc='Heat_gen[t,zone] - Generated heat of top or bottom zone')

    m.heat_dem = pyomo.Var(
        m.tm, m.heat_demand_type, m.zone,
        within=pyomo.NonNegativeReals,
        doc='Heat_dem[t,zone] - Heat demand of top or bottom zone')

    m.heat_sto_in = pyomo.Var(
        m.tm, m.zone,
        within=pyomo.NonNegativeReals,
        doc='Heat_sto,in[t,zone] - Heat flow into storage at top or bottom zone')

    m.heat_sto_out = pyomo.Var(
        m.tm, m.zone,
        within=pyomo.NonNegativeReals,
        doc='Heat_sto,out[t,zone] - Heat flow out of storage at top or bottom zone')

    m.e_heat_sto = pyomo.Var(
        m.t, m.zone,
        within=pyomo.NonNegativeReals,
        doc='E_sto[t,zone] - heat within the thermal storage')

    # heat generation
    m.heat_gen_const = pyomo.Constraint(
        m.tm, m.heat_generation_type,
        rule=heat_gen_const_rule,
        doc='Q_g[t] = sum(Heat_gen[t,zone]) - distributes the heat generation into top or bottom part')

    # heat demand
    m.heat_dem_const = pyomo.Constraint(
        m.tm, m.heat_demand_type,
        rule=heat_dem_const_rule,
        doc='Q_d[t] = Heat_dem[t,zone] - distributes the heat extraction from top or bottom part')

    m.heat_dem_dhw_const = pyomo.Constraint(
        m.tm,
        rule=heat_dem_dhw_const_rule,
        doc='Heat_dem[t,DHW]*T[t,DHW] >= Heat_dem[t,DHW]*T_DHWmin - '
            'heat from the bottom storage part can be used for DHW as well if the temperature is high enough')

    m.heat_sto_vertex_const = pyomo.Constraint(
        m.tm, m.zone,
        rule=heat_sto_vertex_const_rule,
        doc='Heat_gen[t,zone] + Heat_sto,out[t,zone] = Heat_dem[t,zone] + Heat_sto,in[t,zone] - '
            'Heat cannot be produced or destroyed')

    m.heat_storage_state = pyomo.Constraint(
        m.tm, m.zone,
        rule=heat_storage_state_rule,
        doc='E[t,zone] = E[t-1,zone] * (1 - d) ** dt) + Heat_sto,in[t,zone] - Heat_sto,out[t,zone]')

    m.heat_storage_state_max = pyomo.Constraint(
        m.tm, m.zone,
        rule=heat_storage_state_max_rule,
        doc='E[t,zone] <= E_max')

    m.heat_storage_initial_state = pyomo.Constraint(
        m.zone,
        rule=heat_storage_initial_state_rule,
        doc='E[t_0,zone] == storage.init * capacity')

    if not hasattr(m, 'validation'):
        m.heat_storage_state_cyclicity = pyomo.Constraint(
            m.zone,
            rule=heat_storage_state_cyclicity_rule,
            doc='E[t_0,zone] <= E[t_end,zone]')
    return m


def two_zone_heat_surplus(m, tm, com):
    power_surplus = 0
    if com in m.heat_generation_type:
        power_surplus -= m.e_pro_heat[tm, com[4:]]
    if com in m.heat_demand_type:
        power_surplus += m.e_pro_heat[tm, com]
    return power_surplus


def heat_gen_const_rule(m, tm, pro):
    heat_gen = 0
    if pro == 'Solarthermal':
        heat_gen += m.heat_gen[tm, 'bottom']
    elif pro == 'HeatASHP':
        rho = 1  # [kg/l]
        c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
        for zone in ['bottom', 'top']:
            if zone == 'bottom':
                t_zone_min = m.Heat_dict["HeatingSupplyTemp"]
                v = m.Heat_dict["StorageSize"] * (m.Heat_dict['BottomZoneLayer'] + 1) / 10  # l
            else:
                t_zone_min = m.Heat_dict["HeatDHWSupplyTemp"]
                v = m.Heat_dict["StorageSize"] * (10 - (m.Heat_dict['BottomZoneLayer'] + 1)) / 10  # l
            t_sto = t_zone_min + m.e_heat_sto[tm - 1, zone] / (rho * v * c_p)
            eff = (m.Heat_dict['EfficiencyNorm'].loc[pro[4:], 'ratio'] * (8.2707e-3 * t_sto - 1.228e-1))
            heat_gen += m.heat_gen[tm, zone] * eff
    return m.e_pro_heat[tm, pro[4:]] == heat_gen


def heat_dem_const_rule(m, tm, dem):
    if dem == 'Heating':
        return m.e_pro_heat[tm, dem] == m.heat_dem[tm, dem, 'bottom']
    else:
        return m.e_pro_heat[tm, dem] == m.heat_dem[tm, dem, 'bottom'] + m.heat_dem[tm, dem, 'top']


def heat_dem_dhw_const_rule(m, tm):
    t_zone_min = m.Heat_dict["HeatDHWSupplyTemp"]
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    v = m.Heat_dict["StorageSize"] * (10 - (m.Heat_dict['BottomZoneLayer'] + 1)) / 10  # l
    t_sto = t_zone_min + m.e_heat_sto[tm - 1, 'top'] / (rho * v * c_p)
    t_dhw_min = m.Heat_dict["HeatDHWSupplyTemp"]
    return m.heat_dem[tm, 'HeatDHW', 'bottom'] * t_sto >= m.heat_dem[tm, 'HeatDHW', 'bottom'] * t_dhw_min


def heat_sto_vertex_const_rule(m, tm, zone):
    return (m.heat_gen[tm, zone] + m.heat_sto_out[tm, zone] ==
            m.heat_dem[tm, 'HeatDHW', zone] + m.heat_dem[tm, 'Heating', zone] + m.heat_sto_in[tm, zone])


def heat_storage_state_rule(m, tm, zone):
    # E[t, zone] = E[t-1, zone] * (1 - d[l+1, zone]) ** dt) + dE_in[t, zone] - E_out[t, zone]
    disch = m.Heat_dict['StorageDischarge'].mean()
    return m.e_heat_sto[tm, zone] == (m.e_heat_sto[tm - 1, zone] * (1 - disch) ** m.dt.value +
                                      m.heat_sto_in[tm, zone] - m.heat_sto_out[tm, zone])


def heat_storage_state_max_rule(m, tm, zone):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    if zone == 'bottom':
        v = m.Heat_dict["StorageSize"] * (m.Heat_dict['BottomZoneLayer'] + 1) / 10  # l
    else:
        v = m.Heat_dict["StorageSize"] * (10 - (m.Heat_dict['BottomZoneLayer'] + 1)) / 10  # l
    if zone == 'bottom':
        t_zone_min = m.Heat_dict["HeatingSupplyTemp"]
    else:
        t_zone_min = m.Heat_dict["HeatDHWSupplyTemp"]
    e_pro_max = rho * c_p * v * (m.Heat_dict["T_high"] - t_zone_min)
    return m.e_heat_sto[tm, zone] <= e_pro_max


def heat_storage_initial_state_rule(m, zone):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    if zone == 'bottom':
        l_0 = 0
        l_n = m.Heat_dict['BottomZoneLayer']
        t_zone_min = m.Heat_dict["HeatingSupplyTemp"]
        v = m.Heat_dict["StorageSize"] * (m.Heat_dict['BottomZoneLayer'] + 1) / 10  # l
    else:
        l_0 = m.Heat_dict['BottomZoneLayer'] + 1
        l_n = m.Heat_dict['NumberHeatLevels']
        t_zone_min = m.Heat_dict["HeatDHWSupplyTemp"]
        v = m.Heat_dict["StorageSize"] * (10 - (m.Heat_dict['BottomZoneLayer'] + 1)) / 10  # l
    t_is = max(t_zone_min, m.Heat_dict['StorageInit'].iloc[l_0:(l_n + 1)].mean())
    e_0 = rho * c_p * v * (t_is - t_zone_min)
    return m.e_heat_sto[m.t[1], zone] == e_0


def heat_storage_state_cyclicity_rule(m, zone):
    return m.e_heat_sto[m.t[1], zone] <= m.e_heat_sto[m.t[-1], zone]
