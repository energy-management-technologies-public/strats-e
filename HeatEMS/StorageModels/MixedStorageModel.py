import pyomo.core as pyomo


def add_mixed_storage_model(m):
    # Heat Generation Type
    indexlist = []
    for key in m.Heat_dict["HeatProcess"]:
        indexlist.append('Heat' + key)
    m.heat_generation_type = pyomo.Set(
        initialize=indexlist,
        ordered=True,
        doc='Set of heat generation types')

    # Heat Demand Type
    m.heat_demand_type = pyomo.Set(
        initialize=['Heating', 'HeatDHW'],
        doc='Set of heat demand types: Heating only or Heating and DHW')

    indexlist = ['Heating', 'HeatDHW'] + m.Heat_dict["HeatProcess"]
    if 'ElectricHeater0' in m.Heat_dict['ConnectionGenerator'].index.get_level_values(1).tolist():
        indexlist.remove('ElectricHeater')
    m.heat_pro = pyomo.Set(
        initialize=indexlist,
        doc='Set of processes')

    m.e_pro_heat = pyomo.Var(
        m.tm, m.heat_pro,
        within=pyomo.NonNegativeReals,
        doc='Q_g[t] - Heat (kWh) of generation type (e.g. CHP) per timestep [kWh]')

    m.heat_gen = pyomo.Var(
        m.tm,
        within=pyomo.NonNegativeReals,
        doc='Heat_gen[t] - Generated heat of top or bottom zone')

    m.heat_dem = pyomo.Var(
        m.tm, m.heat_demand_type,
        within=pyomo.NonNegativeReals,
        doc='Heat_dem[t] - Heat demand of top or bottom zone')

    m.heat_sto_in = pyomo.Var(
        m.tm,
        within=pyomo.NonNegativeReals,
        doc='Heat_sto,in[t] - Heat flow into storage at top or bottom zone')

    m.heat_sto_out = pyomo.Var(
        m.tm,
        within=pyomo.NonNegativeReals,
        doc='Heat_sto,out[t] - Heat flow out of storage at top or bottom zone')

    m.e_heat_sto = pyomo.Var(
        m.t,
        within=pyomo.NonNegativeReals,
        doc='E_sto[t] - heat within the thermal storage')

    # heat generation
    m.heat_gen_const = pyomo.Constraint(
        m.tm, m.heat_generation_type,
        rule=heat_gen_const_rule,
        doc='Q_g[t] = sum(Heat_gen[t]) - distributes the heat generation into top or bottom part')

    # heat demand
    m.heat_dem_const = pyomo.Constraint(
        m.tm, m.heat_demand_type,
        rule=heat_dem_const_rule,
        doc='Q_d[t] = Heat_dem[t] - distributes the heat extraction from top or bottom part')

    m.heat_sto_vertex_const = pyomo.Constraint(
        m.tm,
        rule=heat_sto_vertex_const_rule,
        doc='Heat_gen[t] + Heat_sto,out[t] = Heat_dem[t] + Heat_sto,in[t] - Heat cannot be produced or destroyed')

    m.heat_storage_state = pyomo.Constraint(
        m.tm,
        rule=heat_storage_state_rule,
        doc='E[t] = E[t-1] * (1 - d) ** dt) + Heat_sto,in[t] - Heat_sto,out[t]')

    m.heat_storage_state_max = pyomo.Constraint(
        m.tm,
        rule=heat_storage_state_max_rule,
        doc='E[t] <= E_max')

    m.heat_storage_initial_state = pyomo.Constraint(
        rule=heat_storage_initial_state_rule,
        doc='E[t_0] == storage.init * capacity')

    if not hasattr(m, 'validation'):
        m.heat_storage_state_cyclicity = pyomo.Constraint(
            rule=heat_storage_state_cyclicity_rule,
            doc='E[t_0] <= E[t_end]')
    return m


def mixed_heat_surplus(m, tm, com):
    power_surplus = 0
    if com in m.heat_generation_type:
        power_surplus -= m.e_pro_heat[tm, com[4:]]
    if com in m.heat_demand_type:
        power_surplus += m.e_pro_heat[tm, com]
    return power_surplus


def heat_gen_const_rule(m, tm, pro):
    heat_gen = 0
    if pro == 'Solarthermal':
        heat_gen += m.heat_gen[tm, 'bottom']
    elif pro == 'HeatASHP':
        rho = 1  # [kg/l]
        c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
        t_min = m.Heat_dict["T_low"]
        v = m.Heat_dict["StorageSize"] # l
        t_sto = t_min + m.e_heat_sto[tm - 1] / (rho * v * c_p)
        eff = (m.Heat_dict['EfficiencyNorm'].loc[pro[4:], 'ratio'] * (8.2707e-3 * t_sto - 1.228e-1))
        heat_gen += m.heat_gen[tm] * eff
    return m.e_pro_heat[tm, pro[4:]] == heat_gen


def heat_dem_const_rule(m, tm, dem):
    return m.e_pro_heat[tm, dem] == m.heat_dem[tm, dem]


def heat_sto_vertex_const_rule(m, tm):
    return (m.heat_gen[tm] + m.heat_sto_out[tm] ==
            m.heat_dem[tm, 'HeatDHW'] + m.heat_dem[tm, 'Heating'] + m.heat_sto_in[tm])


def heat_storage_state_rule(m, tm):
    # E[t] = E[t-1] * (1 - d[l+1]) ** dt) + dE_in[t] - E_out[t]
    disch = m.Heat_dict['StorageDischarge'].mean()
    return m.e_heat_sto[tm] == m.e_heat_sto[tm - 1] * (1 - disch) ** m.dt.value + m.heat_sto_in[tm] - m.heat_sto_out[tm]


def heat_storage_state_max_rule(m, tm):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    v = m.Heat_dict["StorageSize"]  # l
    t_min = m.Heat_dict["T_low"]
    e_pro_max = rho * c_p * v * (m.Heat_dict["T_high"] - t_min)
    return m.e_heat_sto[tm] <= e_pro_max


def heat_storage_initial_state_rule(m):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    l_0 = 0
    l_n = m.Heat_dict['StorageInit'].size - 1
    t_min = m.Heat_dict["T_low"]
    v = m.Heat_dict["StorageSize"]  # l
    t_is = max(t_min, m.Heat_dict['StorageInit'].iloc[l_0:(l_n + 1)].mean())
    e_0 = rho * c_p * v * (t_is - t_min)
    return m.e_heat_sto[m.t[1]] == e_0


def heat_storage_state_cyclicity_rule(m):
    return m.e_heat_sto[m.t[1]] <= m.e_heat_sto[m.t[-1]]
