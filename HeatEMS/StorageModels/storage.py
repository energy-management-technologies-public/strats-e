import math
import pyomo.core as pyomo


def add_storage(m):
    # storage (e.g. hydrogen, pump storage)
    indexlist = set()
    for key in m.storage_dict["eff-in"]:
        indexlist.add(tuple(key)[0])
    m.sto = pyomo.Set(
        initialize=indexlist,
        doc='Set of storage technologies')

    # storage tuples
    m.sto_tuples = pyomo.Set(
        within=m.sto * m.com,
        initialize=tuple(m.storage_dict["eff-in"].keys()),
        doc='Combinations of possible storage by site, e.g. (Bat,Elec)')

    # storage tuples for storages with fixed initial state
    m.sto_init_bound_tuples = pyomo.Set(
        within=m.sto * m.com,
        initialize=tuple(m.stor_init_bound_dict.keys()),
        doc='storages with fixed initial state')

    # Variables
    m.e_sto_in = pyomo.Var(
        m.tm, m.sto_tuples,
        within=pyomo.NonNegativeReals,
        doc='Power flow into storage (kW) per timestep')
    m.e_sto_out = pyomo.Var(
        m.tm, m.sto_tuples,
        within=pyomo.NonNegativeReals,
        doc='Power flow out of storage (kW) per timestep')
    m.e_sto_con = pyomo.Var(
        m.t, m.sto_tuples,
        within=pyomo.NonNegativeReals,
        doc='Energy content of storage (kWh) in timestep')

    # storage rules
    m.def_storage_state = pyomo.Constraint(
        m.tm, m.sto_tuples,
        rule=def_storage_state_rule,
        doc='storage[t] = (1 - sd) * storage[t-1] + in * eff_i - out / eff_o')
    m.res_storage_input_by_power = pyomo.Constraint(
        m.tm, m.sto_tuples,
        rule=res_storage_input_by_power_rule,
        doc='storage input <= storage power')
    m.res_storage_output_by_power = pyomo.Constraint(
        m.tm, m.sto_tuples,
        rule=res_storage_output_by_power_rule,
        doc='storage output <= storage power')
    m.res_storage_state_by_capacity = pyomo.Constraint(
        m.t, m.sto_tuples,
        rule=res_storage_state_by_capacity_rule,
        doc='storage content <= storage capacity')
    m.def_initial_storage_state = pyomo.Constraint(
        m.sto_init_bound_tuples,
        rule=def_initial_storage_state_rule,
        doc='storage content initial == and final >= storage.init * capacity')
    m.res_storage_state_cyclicity = pyomo.Constraint(
        m.sto_tuples,
        rule=res_storage_state_cyclicity_rule,
        doc='storage content initial <= final, both variable')
    return m


# storage balance
def storage_balance(m, tm, com):
    """called in commodity balance
    For a given commodity co and timestep tm, calculate the balance of
    storage input and output """

    return sum(m.e_sto_in[(tm, storage, com)] - m.e_sto_out[(tm, storage, com)]
               # usage as input for storage increases consumption
               # output from storage decreases consumption
               for storage, commodity in m.sto_tuples
               if commodity == com)


# storage costs
def storage_cost(m, cost_type):
    """returns storage cost function for the different cost types"""
    if cost_type == 'Variable':
        return sum(m.e_sto_con[tm, s] * m.storage_dict['var-cost-c'][s] +
                   (m.e_sto_in[tm, s] + m.e_sto_out[tm, s]) * m.storage_dict['var-cost-p'][s]
                   for tm in m.tm
                   for s in m.sto_tuples)


# constraints

# storage content in timestep [t] == storage content[t-1] * (1-discharge)
# + newly stored energy * input efficiency
# - retrieved energy / output efficiency
def def_storage_state_rule(m, t, sto, com):
    return (m.e_sto_con[t, sto, com] ==
            m.e_sto_con[t - 1, sto, com] * (1 - m.storage_dict['discharge'][(sto, com)]) ** m.dt.value +
            m.e_sto_in[t, sto, com] * m.storage_dict['eff-in'][(sto, com)] -
            m.e_sto_out[t, sto, com] / m.storage_dict['eff-out'][(sto, com)])


# storage input <= storage power
def res_storage_input_by_power_rule(m, t, sto, com):
    return m.e_sto_in[t, sto, com] <= m.dt * m.storage_dict['inst-cap-p'][sto, com]


# storage output <= storage power
def res_storage_output_by_power_rule(m, t, sto, co):
    return m.e_sto_out[t, sto, co] <= m.dt * m.storage_dict['inst-cap-p'][sto, co]


# storage content <= storage capacity
def res_storage_state_by_capacity_rule(m, t, sto, com):
    return m.e_sto_con[t, sto, com] <= m.storage_dict['inst-cap-c'][sto, com]


# initialization of storage content in first timestep t[1]
# forced minimum  storage content in final timestep t[len(m.t)]
# content[t=1] == storage capacity * fraction <= content[t=final]
def def_initial_storage_state_rule(m, sto, com):
    return m.e_sto_con[m.t[1], sto, com] == m.storage_dict['inst-cap-c'][sto, com] * m.storage_dict['init'][(sto, com)]


def res_storage_state_cyclicity_rule(m, sto, com):
    return m.e_sto_con[m.t[1], sto, com] <= m.e_sto_con[m.t[len(m.t)], sto, com]
