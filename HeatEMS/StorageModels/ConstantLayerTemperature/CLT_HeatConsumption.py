import pyomo.core as pyomo


def clt_heatconsumption(m):
    # Heat Demand Type
    m.heat_demand_type_set = pyomo.Set(
        initialize=['Heating', 'HeatDHW'],
        # initialize=['HeatingDem', 'HeatDHWDem'],
        doc='Set of heat demand types: Heating only or Heating and DHW')

    # commodity
    m.tau_heat_dem = pyomo.Var(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='tau_d[l,t] - Heat flow (l) through process')
    m.m_heat_dem_conv_in = pyomo.Var(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='V_d,conv,in[l,t] - Heat flow of commodity into process (l) per timestep')
    m.m_heat_dem_conv_out = pyomo.Var(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='V_d,conv,out[l,t] - Heat flow out of process (l) per timestep')
    m.e_pro_out_heat = pyomo.Var(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='Q_d[l,t] - Energy flow of heat commodity out of process (kWh) per timestep (e.g. Heating)')

    m.heat_dem_input = pyomo.Constraint(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        rule=demand_input_rule,
        doc='V_d,conv,in[l,t] == tau_d,conv[l,t]')
    m.heat_dem_output = pyomo.Constraint(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        rule=demand_output_rule,
        doc='V_d,conv,out[l,t] == sum_(l=l_dmin)^(l_max) (tau_d,conv[l,t]')
    m.heat_dem_throughput = pyomo.Constraint(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        rule=heat_dem_throughput,
        doc='tau_d[l,t] <= b_d[l,t] * tau_d,max')
    m.heat_dem_distribution = pyomo.Constraint(
        m.tm, m.heat_demand_type_set, m.heat_levels,
        rule=heat_dem_distribution,
        doc='Q_d[l,t] == tau_d[l,t] * rho * c_p * (l - l_d,ret) * delta_T_layer')
    return m


# process input VolumeFlowIn == process throughput
def demand_input_rule(m, tm, heat_dem, level):
    return m.m_heat_dem_conv_in[tm, heat_dem, level] == m.tau_heat_dem[tm, heat_dem, level]


# process output VolumeFlowOut == process throughput
def demand_output_rule(m, tm, heat_dem, level):
    n_ret = m.Heat_dict[heat_dem + 'RetLevel']

    if level == 'Level' + str(n_ret):
        # the whole flow flows back at the return temperature
        return_flow = sum(m.tau_heat_dem[tm, heat_dem, lev]
                          for lev in m.heat_levels)
    else:
        # temperatures apart from the return level don't have any return flow
        return_flow = 0

    return m.m_heat_dem_conv_out[tm, heat_dem, level] == return_flow


def heat_dem_throughput(m, tm, heat_dem, level):
    level = int(level.replace('Level', ''))
    n_min = m.Heat_dict[heat_dem + 'SupplyLevel']

    if level < n_min:
        # if the temperature is below the minimum supply temperature, the volume can't be consumed
        return m.tau_heat_dem[tm, heat_dem, 'Level' + str(level)] == 0
    else:
        # all levels equal or above the minimum temperature can be consumed
        return (m.tau_heat_dem[tm, heat_dem, 'Level' + str(level)] <=
                m.heat_sto_bin[tm, heat_dem, 'Level' + str(level)] * m.Heat_dict["MaximumFlow"])


def heat_dem_distribution(m, tm, heat_dem, level):
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 kWs/(kgK) / 3600 s/h => [kWh/(kgK)]
    level = int(level.replace('Level', ''))
    delta_t = ((m.Heat_dict["T_high"] - m.Heat_dict["T_low"]) / (m.Heat_dict["NumberHeatLevels"] - 1) *
               (level - m.Heat_dict[heat_dem + 'RetLevel']))  # [K]

    return (m.e_pro_out_heat[tm, heat_dem, 'Level' + str(level)] ==
            m.tau_heat_dem[tm, heat_dem, 'Level' + str(level)] * rho * c_p * delta_t)
