

def eff_HP(source, T_sup, modulation, T_source = 5):
    """
        Calculates the efficiency of the Heat Pump
        :param source: Source name
        :param T_sup: Supply temperature
        :param T_ret: Source temperature
        :return: COP
    """
    if source == "Wolf CHA10":
        cop = (7.392 - 0.0762 * T_sup)
    elif source == "Wolf-02 19 - Viss (heat)":
        cop = (4.25 + 0.1914 * T_source) * (1 + (35 - T_sup) * 0.025)
    elif source == "RaTh-01 20 - Viss (heat)":
        cop = (2.63 + 0.114 * T_source + (55 - T_sup) * 0.025)
    elif source == "StiebelEltronWPF10":
        cop = 3.741 * (1 + (50 - T_sup) * 0.027) * (1 + (T_source - 5) * 0.024)
    else:
        print("Source for heat pump efficiency not found")
    return cop


def eff_Boiler(source, T_ret, modulation):
    """
        Calculates the efficiency of the Condensing Boiler
        :param source: Source name
        :param T_ret: Return temperature
        :param modulation: modulation at which the efficiency should be calculated
        :return: eff (in %)
    """

    if source == "WolfCGB2":
        eff = (-0.00000373 * T_ret ** 4 + 0.000836 * T_ret ** 3 - 0.0625175 * T_ret ** 2 + 1.5718725 * T_ret +
               94.5372)/100
        # Change!!!!
    elif source == "Internet":
        eff = (-0.00000373 * T_ret ** 4 + 0.000836 * T_ret ** 3 - 0.0625175 * T_ret ** 2 + 1.5718725 * T_ret +
               94.5372)/100
    elif source == "FFE":
        eff = 107 - 9.5 * (T_ret - 20) / 40
    else:
        print("Source for Condensing Boiler Efficiency not found")
    return eff


def eff_CHP(source, T_ret, p_max, modulation, source_heat = "Internet-01 20"):
    """
        Calculates the efficiency of the CHP
        :param source: Source name
        :param T_ret: Return temperature
        :param p_max: installed electric power
        :param modulation: modulation at which the efficiency should be calculated
        :return: eff_el and eff_heat (in %)
    """

    if source == "neoTower2":
        if modulation < .8:
            eff_el = .18
            eff_heat = 1.088 * 10 ** -4 * T_ret ** 2 - 1.5724 * 10 ** -2 * T_ret + 1.0893
        else:
            eff_el = 0.24
            eff_heat = 5.0782 * 10 ** -5 * T_ret ** 2 - 9.5441 * 10 ** -3 * T_ret + 0.8961
    elif source == "ASUE<10kW":
        eff_el = 0.21794 * p_max ** 0.108
        eff_ges = eff_Boiler(source_heat, T_ret)
        # assumption: overall efficiency same as Condensing Boiler
        eff_heat = eff_ges - eff_el
    elif source == "ASUE<100kW":
        eff_el = 0.2256 * p_max ** 0.1032
        eff_ges = eff_Boiler(source_heat, T_ret)
        # assumption: overall efficiency same as Condensing Boiler
        eff_heat = eff_ges - eff_el
    elif source == "ASUE<1000kW":
        eff_el = 0.25416 * p_max ** 0.0732
        eff_ges = eff_Boiler(source_heat, T_ret)
        # assumption: overall efficiency same as Condensing Boiler
        eff_heat = eff_ges - eff_el
    else:
        print("Source for CHP Efficiency not found")

    return [eff_el, eff_heat]


def effST(source, T_sup, T_env = 10, E_g = 600):
    """
        Calculates the efficiency of the solar thermal unit
        Takes the whole are of the collectors into account, including area for the frame, etc.
        :param source: Source name
        :param T_sup: Supply temperature
        :param T_env: Environment temperature
        :param E_g: Irradiance
        :return: eff_heat (in %)
    """
    if source == "WolfVaccumCollector":
        # Standard values for vacuum plate collectors according to Wolf
        eta_0 = 0.642
        alpha1 = 0.885
        alpha2 = 0.001
        eff_heat = eta_0 - alpha1 * (T_sup - T_env) / E_g - alpha2 * (T_sup - T_env) ** 2 / E_g
    elif source == "WolfFlatPlateCollector":
        # Standard values for flat plate collectors according to Wolf: eta_0=0.81, alpha1=3.492, alpha2=0.016, source from 2018
        eta_0 = 0.707
        alpha1 = 3.037
        alpha2 = 0.014
        eff_heat = eta_0 - alpha1 * (T_sup - T_env) / E_g - alpha2 * (T_sup - T_env) ** 2 / E_g
    else:
        print("Wrong Source for vacuum Solar Thermal Efficiency")
    return eff_heat
