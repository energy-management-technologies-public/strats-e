import pyomo.core as pyomo


def clt_heatgenerators(m):
    m.m_heat_gen_conv_in = pyomo.Var(
        m.tm, m.heat_gen_conv_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='V_g,conv,in[l,t] - Heat flow of commodity into process (l) per timestep')
    m.m_heat_gen_conv_out = pyomo.Var(
        m.tm, m.heat_gen_conv_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='V_g,conv,out[l,t] - Heat flow of commodity out of process (l) per timestep')
    m.e_pro_in_heat = pyomo.Var(
        m.tm, m.heat_gen_set, m.heat_levels,
        within=pyomo.NonNegativeReals,
        doc='Q_g[l,t] - Energy flow of heat commodity into process at specific level per timestep (e.g. CHP, Level0)'
            ' [kWh]')

    m.heat_gen_conv_input = pyomo.Constraint(
        m.tm, m.heat_gen_conv_set, m.heat_levels,
        rule=heat_gen_conv_input_rule,
        doc='V_g,conv,in[l,t] == tau_g,conv[l,t]')
    m.heat_gen_conv_output = pyomo.Constraint(
        m.tm, m.heat_gen_conv_set, m.heat_levels,
        rule=heat_gen_conv_output_rule,
        doc='V_g,conv,out[l,t] == tau_g,conv[l-delta_l,t]')
    m.heat_gen_throughput = pyomo.Constraint(
        m.tm, m.heat_gen_set, m.heat_levels,
        rule=heat_gen_throughput_rule,
        doc='tau_g[l,t] <= b_g[l,t] * tau_g,max')
    m.heat_gen_distribution = pyomo.Constraint(
        m.tm, m.heat_gen_set, m.heat_levels,
        rule=heat_gen_distribution_rule,
        doc='Q_g[l,t] == tau_g[l,t] * rho * c_p * delta_l_g * delta_T_layer * eta_g[l] * TVE_eff_factor')
    return m


# process input VolumeFlowIn == process throughput
def heat_gen_conv_input_rule(m, tm, heat_gen, level):
    return m.m_heat_gen_conv_in[tm, heat_gen, level] == m.tau_heat_gen[tm, heat_gen, level]


# process output VolumeFlowOut == process throughput
def heat_gen_conv_output_rule(m, tm, heat_gen, level):
    level = int(level.replace('Level', ''))

    if 'SupIm' in m.process_dict['deltaT'].values():
        delta_n = int(m.Heat_dict['Delta_n'].loc[heat_gen, level, tm][0])
    else:
        delta_n = int(m.Heat_dict['Delta_n'].loc[heat_gen, level][0])
    n_max = m.Heat_dict['n_max'][heat_gen]

    if level < delta_n:
        # Heat cannot be produced at a level < delta_n
        return m.m_heat_gen_conv_out[tm, heat_gen, 'Level' + str(level)] == 0
    elif level != n_max:
        # Levels, excluding the top level
        return (m.m_heat_gen_conv_out[tm, heat_gen, 'Level' + str(level)] ==
                m.tau_heat_gen[tm, heat_gen, 'Level' + str(level - delta_n)])
    else:
        # The top level can be produced by level n-1, ... n-delta_n
        tau = sum(m.tau_heat_gen[tm, heat_gen, 'Level' + str(lev)]
                  for lev in range(level - delta_n, n_max))
        return m.m_heat_gen_conv_out[tm, heat_gen, 'Level' + str(level)] == tau


# process throughput <= process capacity * heat_sto_gen[0/1] / efficiency
def heat_gen_throughput_rule(m, tm, heat_gen, level):
    level = int(level.replace('Level', ''))
    n_max = min(m.Heat_dict['NumberHeatLevels'] - 1, m.Heat_dict['n_max'][heat_gen])
    if level < n_max:
        if heat_gen in m.heat_gen_conv_set:
            max_value = m.Heat_dict["MaximumFlow"] * 2
        else:
            max_value = m.process_dict['cap'][heat_gen] * m.global_prop_dict.loc['dt', 'value'] * 2
    else:
        # The top level cannot be extracted for the heat generator, since no higher level can be produced
        max_value = 0

    return m.tau_heat_gen[tm, heat_gen, 'Level' + str(level)] <= (m.heat_sto_bin[tm, heat_gen, 'Level' + str(level)] *
                                                                  max_value)


# process input == process throughput * efficiency
def heat_gen_distribution_rule(m, tm, heat_gen, level):
    level = int(level.replace('Level', ''))
    rho = 1  # [kg/l]
    c_p = 4.184 / 3600  # 4.184 [kWs/(kgK)] / 3600 [s/h] => [kWh/(kgK)]
    if 'SupIm' in m.process_dict['deltaT'].values():
        delta_n = int(m.Heat_dict['Delta_n'].loc[heat_gen, level, tm][0])
    else:
        delta_n = int(m.Heat_dict['Delta_n'].loc[heat_gen, level][0])
    n_max = m.Heat_dict['n_max'][heat_gen]

    # Temperature difference in [K]
    if level <= n_max - delta_n:
        # nominal temperature difference can be achieved
        delta_t = (m.Heat_dict["T_high"] - m.Heat_dict["T_low"]) / (m.Heat_dict["NumberHeatLevels"] - 1) * delta_n
    else:
        # when reaching the maximum temperature difference, the nominal temperature difference cannot be achieved
        # (otherwise the maximum temperature would be exceeded), so delta_T = T_max - T_in
        delta_t = (m.Heat_dict["T_high"] - m.Heat_dict["T_low"]) / (m.Heat_dict["NumberHeatLevels"] - 1) * \
                  (n_max + 1 - level)

    modulation = 0  # CHECK!!!

    if heat_gen in m.heat_gen_conv_set:
        lev = level
    else:
        lev = min(level + 1, m.Heat_dict["NumberHeatLevels"] - 1)
    # Heat pumps have an efficiency depending on the ambient temperature, this is considered by the time dependent
    # eff_factor. For all other heat generators, the eff_factor is set to '1'
    if heat_gen in ('ASHP', 'GSHP'):
        try:
            eff_factor = m.eff_factor_heat_df[heat_gen][tm]
        except AttributeError:
            eff_factor = 1
        efficiency = m.Heat_dict["Efficiency"]['level' + str(lev)][heat_gen][modulation] * eff_factor
    else:
        efficiency = m.Heat_dict["Efficiency"]['level' + str(lev)][heat_gen][modulation]

    if heat_gen in m.heat_gen_conv_set:
        return (m.e_pro_in_heat[tm, heat_gen, 'Level' + str(level)] ==
                m.tau_heat_gen[tm, heat_gen, 'Level' + str(level)] * rho * c_p * delta_t / efficiency)
    else:
        return (m.e_pro_in_heat[tm, heat_gen, 'Level' + str(level)] ==
                m.tau_heat_gen[tm, heat_gen, 'Level' + str(level)] / efficiency)


def calc_n(m, heat_gen, tm=0):
    # maximum level of the heat generator
    n_max = m.Heat_dict['NumberHeatLevels'] - int((m.Heat_dict['T_high'] - m.process_dict['Tmax'][heat_gen]) /
                                                  m.Heat_dict['Delta_T']) - 1

    # level difference between supply and return
    if m.process_dict['deltaT'][heat_gen] == 'SupIm':
        delta_n = max(round(m.supim_dict['DeltaT_' + heat_gen][tm] / m.Heat_dict['Delta_T']), 1)
    elif type(m.process_dict['deltaT'][heat_gen]) == str:
        delta_n = 'TBD!!!'
    elif m.process_dict['deltaT'][heat_gen] < m.Heat_dict['Delta_T']:
        delta_n = 1
    elif m.process_dict['deltaT'][heat_gen] < m.Heat_dict['T_high']:
        delta_n = round(m.process_dict['deltaT'][heat_gen] / m.Heat_dict['Delta_T'])
    else:
        delta_n = n_max

    return [n_max, delta_n]
