import math
import pyomo.core as pyomo


def add_time_variable_efficiency(m):
    # process tuples for time variable efficiency
    m.pro_timevar_output_tuples = pyomo.Set(
        within=m.pro * m.com,
        initialize=[(process, commodity)
                    for process in tuple(m.eff_factor_dict.keys())
                    for (pro, commodity) in tuple(m.r_out_dict.keys())
                    if process == pro and commodity not in m.com_env],
        doc='Outputs of processes with time dependent efficiency')

    # time variable efficiency rules
    m.def_process_timevar_output = pyomo.Constraint(
        m.tm, (m.pro_timevar_output_tuples - (m.pro_partial_output_tuples & m.pro_timevar_output_tuples)),
        rule=def_pro_timevar_output_rule,
        doc='e_pro_out = tau_pro * r_out * eff_factor')
    if m.mode['mip_partload']:
        m.def_process_partial_timevar_output = pyomo.Constraint(
            m.tm, m.pro_partial_output_tuples & m.pro_timevar_output_tuples,
            rule=def_pro_partial_timevar_output_rule,
            doc='e_pro_out = tau_pro * r_out * eff_factor')
    return m


# constraints

# process output == process throughput *
#                   input ratio at maximum operation point *
#                   efficiency factor
def def_pro_timevar_output_rule(m, tm, pro, com):
    return(m.e_pro_out[tm, pro, com] ==
           m.tau_pro[tm, pro] * m.r_out_dict[(pro, com)] * m.eff_factor_dict[pro][tm])


def def_pro_partial_timevar_output_rule(m, tm, pro, coo):
    # input ratio at maximum operation point
    # e_pro_out = offset + slope * tau_pro
    return (m.e_pro_out[tm, pro, coo] ==
            (m.dt * m.pro_p_out_offset_spec[pro, coo] * m.process_dict['inst-cap'][pro] +
             m.pro_p_out_slope[(pro, coo)] * m.tau_pro[tm, pro]) * m.eff_factor_dict[pro][tm])