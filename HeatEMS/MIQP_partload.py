import pyomo.core as pyomo
import pandas as pd


def miqp_partload(m):
    # Binary Variable if MILP-cap_min is activated
    m.pro_mode_run = pyomo.Var(
        m.t, m.pro_partial_tuples,
        within=pyomo.Boolean,
        doc='Boolean: True if process in run mode')

    m.res_throughput_by_capacity_min_milp = pyomo.Constraint(
        m.tm, m.pro_partial_tuples,
        rule=res_throughput_by_capacity_min_milp_rule,
        doc='run[0/1] * cap_inst * min-fraction <= tau_pro')

    m.res_throughput_by_capacity_max_milp = pyomo.Constraint(
        m.tm, m.pro_partial_tuples,
        rule=res_throughput_by_capacity_max_milp_rule,
        doc='tau_pro <= run[0/1] * cap_inst')

    # Calculate start-up costs (MIQP):
    m.pro_mode_startup = pyomo.Var(
        m.tm, m.pro_partial_tuples,
        within=pyomo.Boolean,
        doc='Boolean: True if process started')

    m.res_pro_mode_startup = pyomo.Constraint(
        m.tm, m.pro_partial_tuples,
        rule=res_pro_mode_startup_rule,
        doc='switch on >= run[t] - run [t-1]')

    m.pro_p_startup = pyomo.Var(
        m.tm, m.pro_partial_input_tuples,
        within=pyomo.Reals,
        doc='switch on loss for MILP processes')

    m.def_pro_p_startup = pyomo.Constraint(
        m.tm, m.pro_partial_input_tuples,
        rule=def_pro_p_startup_rule,
        doc='pro_p_startup = E_start * cap(t) * R * start[0/1](t)'
            'R = input ratio at maximum operation point')

    m.def_partial_process_input = pyomo.Constraint(
        m.tm, m.pro_partial_input_tuples,
        rule=def_partial_process_input_miqp_rule,
        doc='e_pro_in = offset + slope * tau_pro + startup_costs'
            'slope = (R -  min_fraction * r) / (1 - min_fraction); offset = R - slope')
    m.def_partial_process_output = pyomo.Constraint(
        m.tm, (m.pro_partial_output_tuples - (m.pro_partial_output_tuples & m.pro_timevar_output_tuples)),
        rule=def_partial_process_output_miqp_rule,
        doc='e_pro_out = offset + slope * tau_pro'
            'slope = (R -  min_fraction * r) / (1 - min_fraction); offset = R - slope')
    m.def_process_partial_timevar_output = pyomo.Constraint(
        m.tm, m.pro_partial_output_tuples & m.pro_timevar_output_tuples,
        rule=def_pro_partial_timevar_output_miqp_rule,
        doc='e_pro_out = (offset + slope * tau_pro) * eff_factor'
            'slope = (R -  min_fraction * r) / (1 - min_fraction); offset = R - slope')
    return m


def partload(m):
    m.res_throughput_by_capacity_min = pyomo.Constraint(
        m.tm, m.pro_partial_tuples,
        rule=res_throughput_by_capacity_min_rule,
        doc='cap_pro * min-fraction <= tau_pro')
    m.def_partial_process_input = pyomo.Constraint(
        m.tm, m.pro_partial_input_tuples,
        rule=def_partial_process_input_rule,
        doc='e_pro_in = cap_pro * min_fraction * (r - R) / (1 - min_fraction)'
            ' + tau_pro * (R - min_fraction * r) / (1 - min_fraction)')
    m.def_partial_process_output = pyomo.Constraint(
        m.tm,
        (m.pro_partial_output_tuples -
            (m.pro_partial_output_tuples & m.pro_timevar_output_tuples)),
        rule=def_partial_process_output_rule,
        doc='e_pro_out = '
            ' cap_pro * min_fraction * (r - R) / (1 - min_fraction)'
            ' + tau_pro * (R - min_fraction * r) / (1 - min_fraction)')
    return m


def res_throughput_by_capacity_min_milp_rule(m, tm, pro):
    # run[0/1] * cap_inst * min - fraction <= tau_pro
    return (m.pro_mode_run[tm, pro] * m.process_dict['cap'][pro] * m.process_dict['min-fraction'][pro] * m.dt <=
            m.tau_pro[tm, pro])


def res_throughput_by_capacity_max_milp_rule(m, tm, pro):
    # tau_pro <= run[0/1] * cap_inst
    return m.tau_pro[tm, pro] <= m.pro_mode_run[tm, pro] * m.process_dict['cap'][pro] * m.dt


def calc_offset_slope(m):
    m.pro_p_in_slope = pd.Series(index=m.pro_partial_input_tuples.value_list)
    m.pro_p_in_offset_spec = pd.Series(index=m.pro_partial_input_tuples.value_list)
    for idx in m.pro_p_in_slope.index:
        # input ratio at maximum operation point
        R = m.r_in_dict[idx]
        # input ratio at lowest operation point
        r = m.r_in_min_fraction_dict[idx]
        min_fraction = m.process_dict['min-fraction'][idx[0]]
        m.pro_p_in_slope[idx] = (R - min_fraction * r) / (1 - min_fraction)
        m.pro_p_in_offset_spec[idx] = R - m.pro_p_in_slope[idx]
    m.pro_p_out_slope = pd.Series(index=m.pro_partial_output_tuples.value_list)
    m.pro_p_out_offset_spec = pd.Series(index=m.pro_partial_output_tuples.value_list)
    for idx in m.pro_p_out_slope.index:
        # output ratio at maximum operation point
        R = m.r_out_dict[idx]
        # output ratio at lowest operation point
        r = m.r_out_min_fraction_dict[idx]
        min_fraction = m.process_dict['min-fraction'][idx[0]]
        m.pro_p_out_slope[idx] = (R - min_fraction * r) / (1 - min_fraction)
        m.pro_p_out_offset_spec[idx] = R - m.pro_p_out_slope[idx]
    return m


# offset = offset_spec * cap(t) * run[0/1](t)
def def_partial_process_input_miqp_rule(m, tm, pro, coin):
    # e_pro_in = offset + slope * tau_pro + startup_costs
    return m.e_pro_in[tm, pro, coin] == \
           m.dt * m.pro_p_in_offset_spec[pro, coin] * m.process_dict['cap'][pro] * m.pro_mode_run[tm, pro] + \
           m.pro_p_in_slope[(pro, coin)] * m.tau_pro[tm, pro] + m.dt * m.pro_p_startup[tm, pro, coin]


def def_partial_process_output_miqp_rule(m, tm, pro, coo):
    # e_pro_out = offset + slope * tau_pro
    return m.e_pro_out[tm, pro, coo] == \
           m.dt * m.pro_p_out_offset_spec[pro, coo] * m.process_dict['cap'][pro] * m.pro_mode_run[tm, pro] + \
           m.pro_p_out_slope[(pro, coo)] * m.tau_pro[tm, pro]


def def_pro_partial_timevar_output_miqp_rule(m, tm, pro, coo):
    # e_pro_out = (offset + slope * tau_pro) * eff_factor
    return m.e_pro_out[tm, pro, coo] == \
           (m.dt * m.pro_p_out_offset_spec[pro, coo] * m.process_dict['cap'][pro] * m.pro_mode_run[tm, pro] +
            m.pro_p_out_slope[(pro, coo)] * m.tau_pro[tm, pro]) * m.eff_factor_dict[pro][tm]


def res_pro_mode_startup_rule(m, tm, pro):
    # switch on >= run[t] - run[t - 1]
    return m.pro_mode_startup[tm, pro] >= m.pro_mode_run[tm, pro] - m.pro_mode_run[tm - 1, pro]


def def_pro_p_startup_rule(m, tm, pro, coin):
    # pro_p_startup = startup_spec * cap * R * start[0/1]
    return m.pro_p_startup[tm, pro, coin] == \
           m.process_dict['start-up-energy'][pro] * m.process_dict['cap'][pro] * m.r_in_dict[(pro, coin)] * \
           m.pro_mode_startup[tm, pro]


def res_throughput_by_capacity_min_rule(m, tm, pro):
    return m.tau_pro[tm, pro] >= m.process_dict['cap'][pro] * m.process_dict['min-fraction'][pro] * m.dt


def def_partial_process_input_rule(m, tm, pro, coin):
    # input ratio at maximum operation point
    # e_pro_in = offset + slope * tau_pro
    return (m.e_pro_in[tm, pro, coin] ==
            m.dt * m.pro_p_in_offset_spec[pro, coin] * m.process_dict['cap'][pro] +
            m.pro_p_in_slope[(pro, coin)] * m.tau_pro[tm, pro])


def def_partial_process_output_rule(m, tm, pro, coo):
    # input ratio at maximum operation point
    # e_pro_out = offset + slope * tau_pro
    return (m.e_pro_out[tm, pro, coo] ==
            m.dt * m.pro_p_out_offset_spec[pro, coo] * m.process_dict['cap'][pro] +
            m.pro_p_out_slope[(pro, coo)] * m.tau_pro[tm, pro])
