import pyomo.core as pyomo
from HeatEMS.modelhelper import commodity_subset


def add_buy_sell_price(m):

    # Sets
    m.com_sell = pyomo.Set(
        within=m.com,
        initialize=commodity_subset(m.com_tuples, 'Sell'),
        doc='Commodities that can be sold')
    m.com_buy = pyomo.Set(
        within=m.com,
        initialize=commodity_subset(m.com_tuples, 'Buy'),
        doc='Commodities that can be purchased')

    # Variables
    m.e_co_sell = pyomo.Var(
        m.tm, m.com_tuples,
        within=pyomo.NonNegativeReals,
        doc='Use of sell commodity source (MW) per timestep')
    m.e_co_buy = pyomo.Var(
        m.tm, m.com_tuples,
        within=pyomo.NonNegativeReals,
        doc='Use of buy commodity source (MW) per timestep')

    # Rules
    m.res_sell_step = pyomo.Constraint(
        m.tm, m.com_tuples,
        rule=res_sell_step_rule,
        doc='sell commodity output per step <= commodity.maxperstep')
    m.res_sell_total = pyomo.Constraint(
        m.com_tuples,
        rule=res_sell_total_rule,
        doc='total sell commodity output <= commodity.max')
    m.res_buy_step = pyomo.Constraint(
        m.tm, m.com_tuples,
        rule=res_buy_step_rule,
        doc='buy commodity output per step <= commodity.maxperstep')
    m.res_buy_total = pyomo.Constraint(
        m.com_tuples,
        rule=res_buy_total_rule,
        doc='total buy commodity output <= commodity.max')

    return m


def bsp_surplus(m, tm, com, com_type):
    power_surplus = 0

    # if com is a sell commodity, the commodity source term e_co_sell
    # can supply a possibly positive power_surplus
    if com in m.com_sell:
        power_surplus -= m.e_co_sell[tm, com, com_type]

    # if com is a buy commodity, the commodity source term e_co_buy
    # can supply a possibly negative power_surplus
    if com in m.com_buy:
        power_surplus += m.e_co_buy[tm, com, com_type]

    return power_surplus


def revenue_costs(m):
    sell_tuples = commodity_subset(m.com_tuples, m.com_sell)
    try:
        return -sum(m.e_co_sell[tm, c] * m.buy_sell_price_dict[c[0]][tm] * m.commodity_dict['price'][c]
                    for tm in m.tm
                    for c in sell_tuples)
    except KeyError:
        return -sum(m.e_co_sell[tm, c] * m.buy_sell_price_dict[c[0], ][tm] * m.commodity_dict['price'][c]
                    for tm in m.tm
                    for c in sell_tuples)


def purchase_costs(m):
    buy_tuples = commodity_subset(m.com_tuples, m.com_buy)
    try:
        return sum(m.e_co_buy[tm, c] * m.buy_sell_price_dict[c[0]][tm] * m.commodity_dict['price'][c]
                   for tm in m.tm
                   for c in buy_tuples)
    except KeyError:
        return sum(m.e_co_buy[tm, c] * m.buy_sell_price_dict[c[0], ][tm] * m.commodity_dict['price'][c]
                   for tm in m.tm
                   for c in buy_tuples)


# constraints

# limit sell commodity use per time step
def res_sell_step_rule(m, tm, com, com_type):
    if com not in m.com_sell:
        return pyomo.Constraint.Skip
    else:
        return m.e_co_sell[tm, com, com_type] <= m.dt * m.commodity_dict['maxperhour'][(com, com_type)]


# limit sell commodity use in total
def res_sell_total_rule(m, com, com_type):
    if com not in m.com_sell:
        return pyomo.Constraint.Skip
    else:
        # calculate total sale of commodity com
        total_consumption = 0
        for tm in m.tm:
            total_consumption += m.e_co_sell[tm, com, com_type]
        return total_consumption <= m.commodity_dict['max'][(com, com_type)]


# limit buy commodity use per time step
def res_buy_step_rule(m, tm, com, com_type):
    if com not in m.com_buy:
        return pyomo.Constraint.Skip
    else:
        return m.e_co_buy[tm, com, com_type] <= m.dt * m.commodity_dict['maxperhour'][(com, com_type)]


# limit buy commodity use in total (scaled to annual consumption, thanks
# to m.weight)
def res_buy_total_rule(m, com, com_type):
    if com not in m.com_buy:
        return pyomo.Constraint.Skip
    else:
        # calculate total sale of commodity com
        total_consumption = 0
        for tm in m.tm:
            total_consumption += m.e_co_buy[tm, com, com_type]
        return total_consumption <= m.commodity_dict['max'][(com, com_type)]
