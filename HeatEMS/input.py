from .StorageModels import *
# import math


def read_input(input_file, slack):
    """Read Excel input file and prepare input dict.
    Args:
        input_file: filename to Excel spreadsheets
        slack: use slack variable or not
    Returns:
        a dict of DataFrames
    """

    with pd.ExcelFile(input_file) as xls:

        global_prop = xls.parse('Global').set_index(['Property'])

        # create MILP index
        process = xls.parse('Process').iloc[:, :12]
        prices = xls.parse('Prices').set_index(['Commodity'])
        heat = xls.parse('HeatStorage')
        process, commodity, process_commodity, heat = pro_com_heat(heat, process, prices, slack)
        process_commodity = process_commodity.set_index(['Process', 'Commodity', 'Direction'])
        demand = xls.parse('Demand').set_index(['t']).loc[:, ['Elec', 'Heating', 'HeatDHW']].dropna()
        supim = xls.parse('SupIm').set_index(['t'])
        el_storage = xls.parse('Storage').set_index(['Storage', 'Commodity'])
        buy_sell_price = prices.iloc[:, 3:].set_index(['t'])
        slack = pd.DataFrame(1, index=buy_sell_price.index, columns=['Slack_out', 'Slack_in'])
        buy_sell_price = pd.concat([buy_sell_price, slack], axis=1)
        eff_factor = (xls.parse('TimeVarEff').set_index(['t']))

    data = {
        'global_prop': global_prop,
        'heat': heat,
        'commodity': commodity,
        'process': process,
        'process_commodity': process_commodity,
        'demand': demand,
        'supim': supim,
        'storage': el_storage,
        'buy_sell_price': buy_sell_price.dropna(axis=1, how='all'),
        'eff_factor': eff_factor.dropna(axis=1, how='all')
    }
    return data


def pyomo_model_prep(data, timesteps, n_lev):
    """
    Performs calculations on the data frames in dictionary "data" for
    further usage by the model.

    Args:
        - data: input data dictionary
        - timesteps: range of modeled timesteps

    Returns:
        a rudimentary pyomo.CancreteModel instance
    """

    m = pyomo.ConcreteModel()

    m.mode = identify_mode(data)
    m.timesteps = timesteps
    m.global_prop = data['global_prop']

    # creating list wih cost types
    m.cost_type_list = ['Variable', 'Environmental', 'Revenue', 'Purchase']

    # additional features
    if m.mode['tve']:
        m.eff_factor_dict = data["eff_factor"].dropna(axis=0, how='all').to_dict()

    # process input/output ratios
    m.r_in_dict = (data['process_commodity'].xs('In', level='Direction')
                   ['ratio'].to_dict())
    m.r_out_dict = (data['process_commodity'].xs('Out', level='Direction')
                    ['ratio'].to_dict())

    # input ratios for partial efficiencies
    # only keep those entries whose values are
    # a) positive and
    # b) numeric (implicitely, as NaN or NV compare false against 0)
    r_in_min_fraction = data['process_commodity'].xs('In', level='Direction')
    r_in_min_fraction = r_in_min_fraction['ratio-min']
    r_in_min_fraction = r_in_min_fraction[r_in_min_fraction > 0]
    m.r_in_min_fraction_dict = r_in_min_fraction.to_dict()

    # output ratios for partial efficiencies
    # only keep those entries whose values are
    # a) positive and
    # b) numeric (implicitely, as NaN or NV compare false against 0)
    r_out_min_fraction = data['process_commodity'].xs('Out', level='Direction')
    r_out_min_fraction = r_out_min_fraction['ratio-min']
    r_out_min_fraction = r_out_min_fraction[r_out_min_fraction > 0]
    m.r_out_min_fraction_dict = r_out_min_fraction.to_dict()

    m.stor_init_bound_dict = data['storage'].dropna(axis=0, how='all')['init']

    # Converting Data frames to dictionaries
    m.demand_dict = data['demand'].to_dict()
    m.supim_dict = data['supim'].to_dict()
    m.buy_sell_price_dict = data["buy_sell_price"].dropna(axis=0, how='all').to_dict()
    m.global_prop_dict = data['global_prop'].to_dict()
    m.commodity_dict = data['commodity'].to_dict()
    m.process_dict = data['process'].to_dict()
    m.storage_dict = data['storage'].dropna(axis=0, how='all').to_dict()

    m = input_heat(m, data, n_lev)

    indexlist = []
    for key in m.Heat_dict["HeatProcess"]:
        indexlist.append((key[0], 'Heat' + key[0]))
    m.pro_heat_in_tuple = indexlist

    m.pro_heat_out_tuple = [('HeatingDem', 'Heating'),
                            ('HeatDHWDem', 'HeatDHW')]

    m.eff_factor_heat_df = calc_eff_factor_heat(m, data['process'], data['eff_factor'])

    check_input(m, data)

    return m


def identify_mode(data):
    """
    Identify the mode that is needed for running the current Input

        Args:
            data: input data dictionary

        Features:
            Storage, Buy Sell (Price), Time
            Variable efficiency, Expansion (4 values for process, transmission,
            storage capacity and storage power expansion)

        Returns:
            mode dictionary; contains bool values that define the urbs mode
            m.mode['exp'] will be initialized with 'True' if the corresponing mode
            (e.g. transmission) is also enabled and later updated through
            identify_expansion(m)

        """

    # create modes
    mode = {
        'sto': False,  # storage
        'bsp': False,  # buy sell price
        'tve': False,  # time variable efficiency
        'heat_levels': False,  # Consider heat levels
        'mip_partload': False,  # MILP partload equations
        'mip_heatlevels': False,  # MILP heat level equations
        'model_type': '',  # Model Type
        'buoy': False  # Consider buoyancy effects in the thermal storage model
    }

    if not data['storage'].empty:
        mode['sto'] = True
    if not data['buy_sell_price'].empty:
        mode['bsp'] = True
    if not data['eff_factor'].empty:
        mode['tve'] = True
    # if data['global_prop'].loc['Consider Heat Levels', 'value'] == 'yes':
    #     mode['heat_levels'] = True
    if data['global_prop'].loc['MILP partload', 'value'] == 'yes':
        mode['mip_partload'] = True
    if data['global_prop'].loc['MILP heat levels', 'value'] == 'yes':
        mode['mip_heatlevels'] = True
    if 'Model type' in data['global_prop'].index:
        mode['model_type'] = data['global_prop'].loc['Model type', 'value']
    if data['global_prop'].loc['Buoyancy', 'value'] == 'yes':
        mode['buoy'] = True
    return mode


def input_heat(m, data, n_lev):
    process = data['process']
    general_parameters = data['heat'].iloc[:, :2].set_index('General Parameters').dropna(0)
    # general parameters
    if m.mode['model_type'] in ['2zone', 'mixed']:
        number_heat_levels = 10
    elif n_lev == 0:
        number_heat_levels = general_parameters.loc['NumberHeatLevels'][0]
    else:
        number_heat_levels = n_lev
    T_low = general_parameters.loc['T_low'][0]
    T_high = general_parameters.loc['T_high'][0]
    delta_T = (T_high - T_low) / (number_heat_levels - 1)
    [delta_n, n_max] = calc_delta_n(process, number_heat_levels, delta_T, T_high, data)
    storage_size = general_parameters.loc['StorageSize'][0]
    storage_height = general_parameters.loc['StorageHeight'][0]
    mixing_coefficient = general_parameters.loc['MixingCoefficient'][0]

    # get storage specifications
    [storage_init, storage_discharge] = storageparameters(data, number_heat_levels, T_low, delta_T,
                                                          data['global_prop'].loc['Model type', 'value'])
    bottom_zone_layer = 0  # round(general_parameters.loc['BottomZone'][0] / number_heat_levels) - 1

    # Temperatures
    dhw_supply_level = calc_level(general_parameters.loc['DHWSupplyTemp'][0], number_heat_levels, T_low, T_high)
    DHWRetTemp = general_parameters.loc['DHWRetTemp'][0]
    DHWSupplyTemp = general_parameters.loc['DHWSupplyTemp'][0]
    dhw_ret_level = calc_level(general_parameters.loc['DHWRetTemp'][0], number_heat_levels, T_low, T_high)
    heating_supply_level = calc_level(general_parameters.loc['HeatingSupplyTemp'][0], number_heat_levels, T_low, T_high)
    HeatingSupplyTemp = general_parameters.loc['HeatingSupplyTemp'][0]
    HeatingRetTemp = general_parameters.loc['HeatingRetTemp'][0]
    BuoyancyConstant = general_parameters.loc['BuoyancyConstant'][0]
    heating_ret_level = calc_level(general_parameters.loc['HeatingRetTemp'][0], number_heat_levels, T_low, T_high)

    # ports
    ports = (data['process']['connection-low'].drop_duplicates().dropna().tolist() +
             data['process']['connection-high'].drop_duplicates().dropna().tolist())

    # demand
    conn_dem = connection_demand(general_parameters, data, ports, process, delta_T)

    # heat generators
    heat_process = process[process['Class'].str.match('Heat')]
    heat_process = heat_process.loc[heat_process['cap'] > 0].index.tolist()
    conn_gen = connection_gen(process, data)

    v_max = max(calc_delta_v('gen', data, general_parameters, process, delta_T,
                             conn_gen.index.get_level_values(1).tolist()).max(), conn_dem.iloc[:, 2:].max().max())

    efficiency_norm = calc_eff_norm(data['process_commodity'], heat_process)
    efficiency = calc_eff(process, heat_process, 'heat', efficiency_norm, T_low, T_high,
                          number_heat_levels)

    m.Heat_dict = {
        'NumberHeatLevels': number_heat_levels,
        'T_low': T_low,
        'T_high': T_high,
        'Delta_T': delta_T,
        'Delta_n': delta_n,
        'n_max': n_max,
        'StorageSize': storage_size,
        'StorageHeight': storage_height,
        'MixingCoefficient': mixing_coefficient,
        'StorageDischarge': storage_discharge,
        'StorageInit': storage_init,
        'BottomZoneLayer': bottom_zone_layer,

        'ConnectionDemand': conn_dem,
        'ConnectionGenerator': conn_gen,
        'MaximumFlow': v_max,

        'HeatProcess': heat_process,
        'HeatDHWSupplyLevel': dhw_supply_level,
        'HeatDHWRetLevel': dhw_ret_level,
        'HeatDHWSupplyTemp': DHWSupplyTemp,
        'HeatDHWRetTemp': DHWRetTemp,
        'HeatingSupplyLevel': heating_supply_level,
        'HeatingRetLevel': heating_ret_level,
        'HeatingSupplyTemp': HeatingSupplyTemp,
        'HeatingRetTemp': HeatingRetTemp,
        'BuoyancyConstant': BuoyancyConstant,
        'Efficiency': efficiency,
        'EfficiencyNorm': efficiency_norm
    }

    return m


def pro_com_heat(heat, process, price, slack):
    # Delete heat related processes within ProcessCommodity and change them according to the chosen efficiencies within
    # HeatLevel

    # Add processes
    max_value = 100
    if slack:
        max_value_slack = 100
    else:
        max_value_slack = 0
    feed_in = ['Feed-in', 'Elec', max_value, '', float('NaN'), float('NaN'), '', '', float('inf'), 0, 0, 0]
    purchase = ['Purchase', 'Elec', max_value, '', float('NaN'), float('NaN'), '', '', float('inf'), 0, 0, 0]
    slack_dhw = ['Slack_DHW', 'Slack_in', max_value_slack, '', float('NaN'), float('NaN'), '', '', float('inf'), 0, 0,
                 0]
    slack_heating = ['Slack_Heating', 'Slack_in', max_value_slack, '', float('NaN'), float('NaN'), '', '', float('inf'),
                     0, 0, 0]
    slack_heatgen = ['SlackHeatGen', 'Heat', max_value_slack, '', 5, 100, '', '', float('inf'), 0,
                     0, 0]
    heating = ['HeatingDem', 'Dem', 1, '', float('NaN'), float('inf'), heat.iloc[12, 1], '', float('inf'), 0, 0, 0]
    dhw = ['HeatDHWDem', 'Dem', 1, '', float('NaN'), float('inf'), heat.iloc[9, 1], '', float('inf'), 0, 0, 0]

    df_pro = pd.DataFrame([feed_in, purchase, slack_dhw, slack_heating, slack_heatgen, heating, dhw],
                          columns=process.columns)
    process = process.append(df_pro).set_index(['Process'])
    process = process.loc[process['cap'] > 0]

    column = ['Process', 'Commodity', 'Direction', 'ratio', 'ratio-min']
    procom = [['Feed-in', 'Elec', 'In', 1, float('NaN')], ['Feed-in', 'Elec sell', 'Out', 1, float('NaN')],
              ['Purchase', 'Elec buy', 'In', 1, float('NaN')], ['Purchase', 'Elec', 'Out', 1, float('NaN')],
              ['Purchase', 'CO2', 'Out', 0.0005, float('NaN')],
              ['PV', 'Solar', 'In', 1, float('NaN')], ['PV', 'Elec', 'Out', 1, float('NaN')],
              ['Slack_DHW', 'Slack_in', 'In', 1, float('NaN')], ['Slack_DHW', 'HeatDHW', 'Out', 1, float('NaN')],
              ['Slack_Heating', 'Slack_in', 'In', 1, float('NaN')],
              ['Slack_Heating', 'Heating', 'Out', 1, float('NaN')]]
    process_commodity = pd.DataFrame(procom, columns=column).set_index('Process')

    column = ['Commodity', 'Type', 'price', 'max', 'maxperhour']
    com = [['Solar', 'SupIm',  float('NaN'),  float('NaN'),  float('NaN')],
           ['Elec', 'Demand',  float('NaN'),  float('NaN'),  float('NaN')],
           ['Heating', 'Demand',  float('NaN'),  float('NaN'),  float('NaN')],
           ['HeatDHW', 'Demand',  float('NaN'),  float('NaN'),  float('NaN')],
           ['Gas', 'Buy',  price.loc['Gas', 'price'],  float('inf'),  float('inf')],
           ['CO2', 'Env',  price.loc['CO2', 'price'],  float('inf'),  float('inf')],
           ['Elec sell', 'Sell',  price.loc['Elec sell', 'price'],  float('inf'),  float('inf')],
           ['Elec buy', 'Buy',  price.loc['Elec buy', 'price'],  float('inf'),  float('inf')],
           ['Slack_in', 'Buy',  9999,  float('inf'),  float('inf')]]
    commodity = pd.DataFrame(com, columns=column).set_index(['Commodity', 'Type'])

    for i in process.index:
        if 'Heat' in process.loc[i, 'Class']:
            minmodulation = process.loc[i, 'min-fraction']
            df_com = pd.DataFrame([['Heat' + i, 'Demand', float('NaN'), float('NaN'), float('NaN')]]).set_index([0, 1])
            df_com.set_axis(['price', 'max', 'maxperhour'], axis='columns', inplace=True)

            if i in process_commodity.index:
                print('Values of ' + i + ' within ProcessCommodity rewritten')
                process_commodity = process_commodity.drop(i)
            if 'CHP' == i:
                Tmin = heat.iloc[:10, :2][heat.iloc[:10, :2]['General Parameters'].str.match('T_low')].iloc[0, 1]
                eff_elec = calc_eff(process, i, 'elec')
                eff_heat = calc_eff(process, [i], 'heat', Tmin)
                df_procom = pd.DataFrame([['Gas', 'In', 1 / eff_heat[0], minmodulation / eff_heat[1]],
                                          ['Heat' + i, 'Out', 1, minmodulation],
                                          ['Elec', 'Out', eff_elec[0] / eff_heat[0],
                                           minmodulation * eff_elec[1] / eff_heat[1]],
                                          ['CO2', 'Out', 0.202 / eff_heat[0], minmodulation * 0.202 / eff_heat[1]]],
                                         index=(i, i, i, i),
                                         columns=process_commodity.columns)
            elif 'CB' == i:
                Tmin = heat.iloc[:10, :2][heat.iloc[:10, :2]['General Parameters'].str.match('T_low')].iloc[0, 1]
                eff_heat = calc_eff(process, [i], 'heat', Tmin)
                df_procom = pd.DataFrame([['Gas', 'In', 1 / eff_heat[0], minmodulation / eff_heat[1]],
                                          ['Heat' + i, 'Out', 1, minmodulation],
                                          ['CO2', 'Out', 0.202 / eff_heat[0], minmodulation * 0.202 / eff_heat[1]]],
                                         index=(i, i, i),
                                         columns=process_commodity.columns)
            elif 'SolarThermal' == i:
                df_procom = pd.DataFrame([['Solar', 'In', 1, 1], ['Heat' + i, 'Out', 1, 1]],
                                         index=(i, i),
                                         columns=process_commodity.columns)
            elif 'ASHP' == i or 'GSHP' == i or 'ElectricHeater' == i:
                Tmin = heat.iloc[:10, :2][heat.iloc[:10, :2]['General Parameters'].str.match('T_low')].iloc[0, 1] + \
                       process.loc[i, 'deltaT']
                eff_heat = calc_eff(process, [i], 'heat', Tmin)
                df_procom = pd.DataFrame([['Elec', 'In', 1 / eff_heat[0], 1 / eff_heat[1]],
                                          ['Heat' + i, 'Out', 1, float('NaN')]],
                                         index=(i, i),
                                         columns=process_commodity.columns)
            elif 'SlackHeatGen' == i:
                df_procom = pd.DataFrame([['Slack_in', 'In', 10, 10],
                                          ['Heat' + i, 'Out', 1, 1]],
                                         index=(i, i),
                                         columns=process_commodity.columns)
            else:
                print('Wrong name for heat process ' + i)
                df_procom = pd.DataFrame(columns=process_commodity.columns)

            process_commodity = process_commodity.append(df_procom)
            commodity = commodity.append(df_com)

    process_commodity = process_commodity.reset_index()
    process_commodity = process_commodity.rename(columns={'index': 'Process'})

    return process, commodity, process_commodity, heat


def calc_delta_v(gen_dem, data, general_parameters, process, deltaT, ports=None):
    rho = 1  # kg / l
    c_p = 4.18  # kWs / kg K
    dt = data['global_prop'].loc['dt', 'value']  # h
    dh = []
    index = []

    if gen_dem == 'dem':
        v_connection = data['demand'][['Heating', 'HeatDHW']].transpose() / (rho * c_p / 3600 * deltaT)

    else:
        ports = list(dict.fromkeys(ports))
        process_a = process.dropna(subset=['connection-low'])
        process_b = process.dropna(subset=['connection-high'])
        for p in ports:
            if 'Integrated' in p:
                dh.append(process.loc['ElectricHeater', 'cap'] / general_parameters.loc['NumberElHeater'][0] *
                          dt / (rho * c_p / 3600 * deltaT))
                index.append(p.replace('Integrated', ''))
            else:
                try:
                    a = process_a[process_a['connection-low'].str.match(p)]['cap'].sum() * dt / (rho * c_p / 3600 *
                                                                                                 deltaT)
                except AttributeError:
                    a = 0
                try:
                    b = process_b[process_b['connection-high'].str.match(p)]['cap'].sum() * dt / (rho * c_p / 3600 *
                                                                                                  deltaT)
                except AttributeError:
                    b = 0
                dh.append(a+b)
                index.append(p)
        v_connection = pd.Series(dh, name='delta_V', index=index)
    return v_connection


def calc_eff(process, heat_process, com, eff_norm=None, T_low=30, T_high=70, number_heat_levels=1):
    """
    Calculates the efficiency of the different heat process regarding to the efficiency defined in the input sheet
    :param process: dataframe of the processes
    :param heat_process: Name of the process to calculate the efficiency
    :param com: 'electric' or 'heat' efficiency
    :param eff_norm: norm efficiency of the process
    :param T_low: lower temperature for defining the level of which the efficiency should be calculated for
    :param T_high: higher temperature for defining the level of which the efficiency should be calculated for
    :param number_heat_levels: Number of heat levels for which the efficiency should be calculated for
    :return:
    """
    if com == 'elec':
        source = process.loc[heat_process, 'heat-eff']  # efficiency source within the input file
        modulation = process.loc[heat_process, 'min-fraction']  # minimum modulation as defined in the input file
        p_max = process.loc[heat_process, 'cap']  # maximum power, required for some sources
        eff = [eff_CHP(source, T_low, p_max, 1)[0], eff_CHP(source, T_low, p_max, modulation)[0]]
        # efficiency at nominal and minimal modulation
    else:
        eff_pro = []
        index = []
        for pro in heat_process:
            source = process.loc[pro, 'heat-eff']  # efficiency source within the input file
            modulation = process.loc[pro, 'min-fraction']  # minimum modulation as defined in the input file
            p_max = process.loc[pro, 'cap']  # maximum power, required for some sources

            index.append(pro)
            eff_level = []
            level = 0
            column = []
            while level < number_heat_levels:
                if number_heat_levels == 1:
                    # if only one level is defined, the efficiency should be calculated for the 'pro_com_heat' with
                    # regard to the lowest temperature and no norm efficiency
                    temperature = T_low
                    eff_norm_pro = pd.Series([1, 1], index=['ratio', 'ratio-min'])
                else:
                    # if more levels are defined, the efficiency should be calculated for the different heat levels of
                    # the processes, the temperature should sweep between minimum and maximum temperature and the
                    # efficiency should be deficed by the norm efficiency
                    temperature = T_low + (T_high - T_low) / (number_heat_levels - 1) * level
                    eff_norm_pro = eff_norm.loc[pro]
                column.append('level' + str(level))
                if pro == 'CHP':
                    eff_level.append([eff_CHP(source, temperature, p_max, 1)[1] / eff_norm_pro.loc['ratio'],
                                      eff_CHP(source, temperature, p_max, modulation)[1] / eff_norm_pro.loc['ratio-min']
                                      ])
                elif pro == 'CB':
                    eff_level.append([eff_Boiler(source, temperature, 1) / eff_norm_pro.loc['ratio'],
                                      eff_Boiler(source, temperature, modulation) / eff_norm_pro.loc['ratio-min']])
                elif pro in ('ASHP', 'GSHP'):
                    eff_level.append([eff_HP(source, temperature, 1) / eff_norm_pro.loc['ratio'],
                                      eff_HP(source, temperature, modulation) / eff_norm_pro.loc['ratio-min']])
                elif pro in ('SolarThermal', 'ElectricHeater', 'SlackHeatGen'):
                    eff_level.append([1, 1])
                level += 1
            eff_pro.append(eff_level)

        if number_heat_levels == 1:
            eff = eff_pro[0][0]
        else:
            eff = pd.DataFrame(eff_pro, index=index, columns=column)

    return eff


def calc_eff_norm(procom, heat_process):
    eff_norm = []
    index = []
    for pro in heat_process:
        index.append(pro)
        if pro in ('CHP', 'CB'):
            eff_ratio = 1 / procom.loc[(pro, 'Gas', 'In'), 'ratio']
            if procom.loc[(pro, 'Gas', 'In'), 'ratio-min'] == 0:
                eff_ratio_min = eff_ratio
            else:
                eff_ratio_min = (procom.loc[(pro, 'Heat' + pro, 'Out'), 'ratio-min'] /
                                 procom.loc[(pro, 'Gas', 'In'), 'ratio-min'])
            eff_norm.append([eff_ratio, eff_ratio_min])
        if pro in ('SolarThermal', 'ElectricHeater', 'SlackHeatGen'):
            eff_norm.append([1, 1])
        if pro in ('ASHP', 'GSHP'):
            eff_ratio = 1 / procom.loc[(pro, 'Elec', 'In'), 'ratio']
            if procom.loc[(pro, 'Elec', 'In'), 'ratio-min'] == 0:
                eff_ratio_min = eff_ratio
            else:
                eff_ratio_min = (procom.loc[(pro, 'Heat' + pro, 'Out'), 'ratio-min'] /
                                 procom.loc[(pro, 'Elec', 'In'), 'ratio-min'])
            eff_norm.append([eff_ratio, eff_ratio_min])

    eff = pd.DataFrame(eff_norm, index=index, columns=['ratio', 'ratio-min'])

    return eff


def calc_eff_factor_heat(m, process, eff_factor):
    T_low = m.Heat_dict["T_low"]
    column = []
    eff = []
    eff_air = []
    eff_ground = []
    if 'ASHP' in m.Heat_dict['HeatProcess']:
        airtemp = eff_factor.loc[:, 'AirTemperature'].tolist()
        eff_norm = m.Heat_dict['EfficiencyNorm']['ratio']['ASHP']
        source = process.loc['ASHP', 'heat-eff']
        column.append('ASHP')
        for T_env in airtemp:
            eff_air.append(eff_HP(source, T_low, 1, T_env) / eff_norm)
        eff.append(eff_air)

    if 'GSHP' in m.Heat_dict['HeatProcess']:
        groundtemp = eff_factor.loc[:, 'GroundTemperature'].tolist()
        eff_norm = m.Heat_dict['EfficiencyNorm']['ratio']['GSHP']
        source = process.loc['GSHP', 'heat-eff']
        column.append('GSHP')
        for T_env in groundtemp:
            eff_ground.append(eff_HP(source, T_low, 1, T_env) / eff_norm)
        eff.append(eff_ground)

    eff = pd.DataFrame(eff, index=column).transpose()
    return eff


def calc_delta_n(process, number_heat_levels, delta_T, T_high, data):
    process = process[process['Class'].str.match('Heat')]
    delta_n = []
    n_max = []
    if 'SupIm' in process['deltaT'].tolist():
        # if temperature difference of processes varies over time
        for i in process.index:
            n_max.append(number_heat_levels - int((T_high - process['Tmax'][i]) / delta_T) - 1)
            for lev in range(0, number_heat_levels):
                for t in range(data['global_prop'].loc['time offset', 'value'],
                               data['global_prop'].loc['time offset', 'value'] +
                               data['global_prop'].loc['optimization length', 'value'] + 1):
                    if process['deltaT'][i] == 'SupIm':
                        delta_n.append([i, lev, t, max(round(data['supim'].loc[t, 'DeltaT_' + i] / delta_T), 1)])
                    elif type(process['deltaT'][i]) == str:
                        delta_n.append([i, lev, t, 'TBD'])
                    elif process['deltaT'][i] < delta_T:
                        delta_n.append([i, lev, t, 1])
                    elif process['deltaT'][i] < T_high:
                        delta_n.append([i, lev, t, round(process['deltaT'][i] / delta_T)])
                    else:
                        delta_n.append([i, lev, t, n_max[-1]])
        delta_n_pd = pd.DataFrame(delta_n, columns=['Process', 't', 'Level', 'delta_n'])
        delta_n_pd = delta_n_pd.set_index(['Process', 't', 'Level'])
    else:
        # if temperature difference of processes is the same over time
        for i in process.index:
            n_max.append(number_heat_levels - int((T_high - process['Tmax'][i]) / delta_T) - 1)
            for lev in range(0, number_heat_levels):
                if type(process['deltaT'][i]) == str:
                    delta_n.append([i, lev, 'TBD'])
                elif process['deltaT'][i] < delta_T:
                    delta_n.append([i, lev, 1])
                elif process['deltaT'][i] < T_high:
                    delta_n.append([i, lev, round(process['deltaT'][i] / delta_T)])
                else:
                    delta_n.append([i, lev, n_max[-1]])
        delta_n_pd = pd.DataFrame(delta_n, columns=['Process', 'Level', 'delta_n'])
        delta_n_pd = delta_n_pd.set_index(['Process', 'Level'])
    n_max_pd = pd.Series(n_max, index=process.index)
    return delta_n_pd, n_max_pd


def storageparameters(data, n_level, t_low, delta_t, model_type='CLT'):
    t_init = data['heat'].iloc[1:, 4].dropna(0)
    sto = pd.DataFrame(0.0, index=range(0, n_level), columns=['index', 'init', 'losses'])
    if 'CLV' in model_type:
        for lev in range(0, n_level):
            for i in range(0, t_init.size):
                if lev / n_level >= i / 10 and (lev + 1) / n_level <= (i + 1) / 10:
                    sto.loc[lev, 'init'] += t_init.iloc[i]
                elif i / 10 >= lev / n_level and (i + 1) / 10 <= (lev + 1) / n_level:
                    sto.loc[lev, 'init'] += t_init.iloc[i] / 10 * n_level
                elif i / 10 < lev / n_level < (i + 1) / 10 < (lev + 1) / n_level:
                    sto.loc[lev, 'init'] += ((i + 1) / 10 - lev / n_level) * t_init.iloc[i] * n_level
                elif lev / n_level < i / 10 < (lev + 1) / n_level < (i + 1) / 10:
                    sto.loc[lev, 'init'] += ((lev + 1) / n_level - i / 10) * t_init.iloc[i] * n_level
    else:
        t_level = []
        for lev in range(0, n_level):
            t_level.append(t_low + delta_t * lev)
        for lev in range(0, n_level):
            for i in range(0, t_init.size):
                if lev == 0 and t_level[lev] >= t_init.iloc[i]:
                    sto.loc[lev, 'init'] += 1 / 10
                elif lev == (n_level - 1) and t_level[lev] <= t_init.iloc[i]:
                    sto.loc[lev, 'init'] += 1 / 10
                elif (t_level[lev]) < t_init.iloc[i] <= (t_level[lev] + delta_t):
                    sto.loc[lev, 'init'] += (1 - (t_init.iloc[i] - t_level[lev]) / delta_t) / 10
                    sto.loc[lev + 1, 'init'] += (t_init.iloc[i] - t_level[lev]) / delta_t / 10
            sto.loc[lev, 'losses'] = 0.004 + 0.0111 / 52 * (t_level[lev] - 26)
            sto.loc[lev, 'index'] = 'Level' + str(lev)
    sto = sto.set_index('index')
    return sto.loc[:, 'init'], sto.loc[:, 'losses']


def calc_level(temperature, number_heat_levels, T_low, T_high):
    level = (temperature - T_low) / (T_high - T_low) * (number_heat_levels - 1)
    return int(level)


def connection_demand(general_parameters, data, ports, process, delta_T):
    conn_dem = general_parameters.loc[['DHWSupplyPort', 'HeatingSupplyPort']].iloc[:, 0].rename('Port')
    conn_dem = conn_dem.rename(index={'HeatingSupplyPort': 'Heating', 'DHWSupplyPort': 'HeatDHW'})
    port_dem_height = data['heat'].iloc[1:, 6:10].dropna().set_index('Unnamed: 7').drop('Gen').set_index(
        'Storage Ports').rename(columns={'Unnamed: 8': 'HeightOut', 'Unnamed: 9': 'HeightIn'})
    port_dem_height = port_dem_height.loc[port_dem_height.index.intersection(ports), :]
    port_dem_height = port_dem_height.rename(index={general_parameters.loc['HeatingSupplyPort'][0]: 'Heating',
                                                    general_parameters.loc['DHWSupplyPort'][0]: 'HeatDHW'})
    T_min = general_parameters.loc[['DHWSupplyTemp', 'HeatingSupplyTemp']].iloc[:, 0].rename('T_min')
    T_min = T_min.rename(index={'DHWSupplyTemp': 'HeatDHW', 'HeatingSupplyTemp': 'Heating'})
    delta_v_dem = calc_delta_v('dem', data, general_parameters, process, delta_T)
    conn_dem = pd.concat([conn_dem, port_dem_height, T_min, delta_v_dem], axis=1, sort=True)
    return conn_dem


def connection_gen(process, data):
    conn_gen = process[process['Class'].str.match('Heat')]['connection-low'].dropna().append(
        process[process['Class'].str.match('Heat')]['connection-high'].dropna()).to_frame().rename(columns={0: 'Name'})
    if 'SlackHeatGen' in conn_gen.index:
        conn_gen.loc['SlackHeatGen'] = conn_gen.iloc[0, 0]
    port_gen_height = data['heat'].iloc[1:, 6:10].dropna().set_index('Unnamed: 7').drop('Dem').set_index(
        'Storage Ports').rename(columns={'Unnamed: 8': 'HeightOut', 'Unnamed: 9': 'HeightIn'})
    heatexch = data['heat'].iloc[1:, 10:14].dropna(0).set_index('Heat Exchanger').rename(
        columns={'Unnamed: 11': 'HeightOut', 'Unnamed: 12': 'HeightIn', 'Unnamed: 13': 'DeltaT_out'})
    height_gen1 = []
    height_gen2 = []
    delta_T = []
    for i in conn_gen['Name']:
        if 'Port' in i:
            height_gen1.append(port_gen_height.loc[i, 'HeightIn'])
            height_gen2.append(port_gen_height.loc[i, 'HeightOut'])
            delta_T.append(0)
        if 'HeatExchanger' in i:
            height_gen1.append(heatexch.loc[i, 'HeightIn'])
            height_gen2.append(heatexch.loc[i, 'HeightOut'])
            delta_T.append(heatexch.loc[i, 'DeltaT_out'])
    conn_gen['HeightIn'] = height_gen1
    conn_gen['HeightOut'] = height_gen2
    conn_gen['DeltaT_out'] = delta_T
    for pro in conn_gen.index:
        conn_gen.loc[pro, 'T_max'] = data['process'].loc[pro, 'Tmax']
    conn_gen_int_eh = data['heat'].iloc[1:, 14:16].dropna(0)
    for i in conn_gen_int_eh.index:
        conn_gen_int_eh = conn_gen_int_eh.rename(index={i: 'ElectricHeater'})
    conn_gen_int_eh = conn_gen_int_eh.rename(columns={'Integrated Electric Heater': 'Name', 'Unnamed: 15': 'HeightEH'})
    # conn_gen_int_eh['Height'] = 0
    if not conn_gen_int_eh.empty:
        try:
            conn_gen = conn_gen.drop('ElectricHeater')
        except KeyError:
            pass
    conn_gen = conn_gen.append(conn_gen_int_eh, sort=False).reset_index().rename(
        columns={'index': 'Process'}).set_index(['Process', 'Name'])
    return conn_gen


def check_input(m, data):
    # Check if DHWSupplyTemp, HeatingSupplyTemp and maximum temperature for storage init can be produced by standard
    #       heat generators or introduce slack variable
    # Check if Heat_storage init = 1
    # Check if NumberElHeater < 2
    # Check if demand/generation port = demand/generation port
    # Check if Process is filled
    return
