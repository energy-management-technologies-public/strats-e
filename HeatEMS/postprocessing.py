import pandas as pd
import os
import time
from datetime import datetime
import pyomo.core as pyomo
import numpy as np


def report(prob, result_dir, input_file, result_name, commodity, process, exp_type=''):
    now = datetime.now().strftime('%Y%m%dT%H%M')
    result_name += '_' + str(round(prob.dt.value, 3)) + 'h_' + str(prob.Heat_dict['NumberHeatLevels']) + 'L'
    resultfile = os.path.join(result_dir, '{}.xlsx'.format(result_name, now))
    writer = pd.ExcelWriter(resultfile)

    # Create Information sheet
    info = pd.Series()
    info.loc['Date'] = time.asctime()
    info.loc['Input File Name'] = input_file
    info.loc['timebase'] = prob.dt.value
    info.loc['time offset'] = prob.global_prop.loc['time offset', 'value']
    info.loc['Consider Heat Levels'] = prob.mode['heat_levels']
    if prob.mode['heat_levels']:
        info.loc['Active Levels'] = prob.mode['mip_heatlevels']
        if prob.mode['mip_heatlevels']:
            info.loc['Model type'] = prob.mode['model_type']
        info.loc['Number of Levels'] = prob.Heat_dict['NumberHeatLevels']
        info.loc['MILP partload'] = prob.mode['mip_partload']
    info.loc['Model preparation time'] = prob.t_create_model
    info.loc['Model solving time'] = prob.t_solve_model
    info.loc['Model solving wall time'] = prob.t_wall_solve_model
    info.loc['Slack used'] = prob.slack
    info.to_excel(writer, 'Info', merge_cells=False)

    for com in commodity:
        calc_temp_out = False
        com_df = get_timeseries_com(prob, com, calc_temp_out, exp_type)
        if not com_df.empty:
            com_df.to_excel(writer, com, merge_cells=False)
    for pro in process:
        calc_temp_out = True
        pro_df = get_timeseries_pro(prob, pro, calc_temp_out, exp_type)
        if not pro_df.empty:
            pro_df.to_excel(writer, pro, merge_cells=False)

    heatstorage_df = get_timeseries_heatstorage(prob) #, calc_temp=True)
    heatstorage_df.to_excel(writer, 'HeatStorage', merge_cells=False)
    if prob.mode['heat_levels']:
        for level in prob.heat_levels:
            heat_df = get_timeseries_heat(prob, level)
            heat_df.to_excel(writer, 'Heat_' + level, merge_cells=False)

    writer.save()  # save


def get_timeseries_com(prob, com, calc_temp_out=False, exp_type='', timesteps=None):
    """Return DataFrames of all timeseries referring to given commodity
    Args:
        - prob: a pyomo model
        - com: a commodity name
        - timesteps: optional list of timesteps, default: all modelled timesteps

    Returns:
        Dataframe with all results of the commodity
    """
    if timesteps is None:
        # default to all simulated timesteps
        timesteps = sorted(get_entity(prob, 'tm').index)
    else:
        timesteps = sorted(timesteps)  # implicit: convert range to list
    time_h = [t * prob.dt.value for t in timesteps]
    time_h = pd.DataFrame(time_h, index=timesteps, columns=['time [h]'])

    # DEMAND
    demand = prob._data['demand'].loc[timesteps]
    try:
        demand = demand[com] * (-1)
        demand.name = 'Demand'
    except KeyError:
        demand = pd.DataFrame(index=timesteps[1:])

    created = get_pro_data(prob, 'e_pro_out', timesteps, com)

    # PROCESS
    if com in ['Heating', 'HeatDHW']:
        if prob.mode['model_type'] == 'CLT':
            for level in prob.heat_levels:
                rho = 1  # [kg/l]
                c_p = 4.184 / 3600  # 4.184 kWs/(kgK) / 3600 s/h
                created_heat = get_pro_data(prob, 'm_heat_dem_conv_in', timesteps, level, 'heat_levels',
                                            'heat_demand_type_set')
                created_heat = created_heat.loc[:, com]
                level = int(level.replace('Level', ''))
                delta_T = (level - prob.Heat_dict[com + 'RetLevel']) * prob.Heat_dict['Delta_T']
                created_heat = created_heat * (rho * c_p * delta_T)
                created['Level' + str(level)] = created_heat
        else:
            created_heat = get_pro_data(prob, 'e_pro_heat_conn', timesteps, 'Heating', 'heat_sto_port_connection')

    consumed = get_pro_data(prob, 'e_pro_in', timesteps, com) * (-1)

    # Buy / Sell
    buy = get_buy_sell_data(prob, 'e_co_buy', timesteps, com, 'Buy')
    sell = get_buy_sell_data(prob, 'e_co_sell', timesteps, com, 'Sell') * (-1)

    # STORAGE
    # group storage energies by commodity
    # select all entries with desired commodity co
    stored = get_entities(prob, ['e_sto_con', 'e_sto_in', 'e_sto_out'])
    try:
        stored = stored.loc[timesteps].xs([com], level=['com'])
        stored.columns = ['Storage Level', 'Stored', 'Retrieved']
        stored.index = timesteps
        stored['Stored'] = -stored['Stored']
        stored = stored[['Stored', 'Retrieved', 'Storage Level']]
    except (KeyError, ValueError):
        stored = pd.DataFrame(0, index=timesteps, columns=['Stored', 'Retrieved', 'Storage Level'])
    com_df = pd.concat([demand, buy, sell, created, consumed, stored], axis=1).dropna(axis='columns')
    com_df = com_df.loc[:, (com_df != 0).any(axis=0)]
    if not com_df.empty:
        if calc_temp_out:
            T_out = calc_t_out(prob, timesteps, exp_type, com)
            com_df = pd.concat([com_df, T_out], axis=1)
        com_df = pd.concat([time_h, com_df], axis=1)
    return com_df


def get_timeseries_pro(prob, pro, calc_temp_out=False, exp_type='', timesteps=None):
    """Return DataFrames of all timeseries referring to given commodity
        Args:
            - prob: a pyomo model
            - pro: a process name
            - timesteps: optional list of timesteps, default: all modelled timesteps

        Returns:
            Dataframe with all results of the process
        """
    if timesteps is None:
        # default to all simulated timesteps
        timesteps = sorted(get_entity(prob, 'tm').index)
    else:
        timesteps = sorted(timesteps)  # implicit: convert range to list
    time_h = [t * prob.dt.value for t in timesteps]
    time_h = pd.DataFrame(time_h, index=timesteps, columns=['time [h]'])

    pro_df = pd.DataFrame(0, index=timesteps,
                          columns=(['Consumed Energy', 'Total Heat Output', 'Efficiency'] +
                                   prob.Heat_dict['StorageInit'].index.tolist()))

    if pro in prob.Heat_dict["HeatProcess"]:
        if prob.mode['model_type'] == 'CLT':
            for level in prob.heat_levels:
                created = get_pro_data(prob, 'e_pro_in_heat', timesteps, level, 'heat_levels', 'heat_gen_set')
                try:
                    created = created.loc[:, pro]
                    pro_df.loc[:, level] = created
                    pro_df.loc[:, 'Total Heat Output'] += created
                except (KeyError, ValueError):
                    print('No values in created results for ' + pro)
        else:
            clv_timesteps = sorted(get_entity(prob, 'tm_clv').index)
            created = get_pro_data(prob, 'e_pro_heat_conn', clv_timesteps, pro, 'heat_pro', 'heat_sto_port_connection')

        if pro in ['ASHP', 'GSHP', 'ElectricHeater']:
            com = 'Elec'
        elif pro in ['CHP', 'CB']:
            com = 'Gas'
        elif pro in ['SolarThermal']:
            com = 'Solar'
        else:
            com = 'Slack_in'
        consumed = get_pro_data(prob, 'e_pro_in', timesteps, com)
        consumed = consumed[pro]
        try:
            pro_df['Consumed Energy'] = pro_df['Consumed Energy'].add(consumed)
        except (KeyError, ValueError):
            print('No values in consumed results for ' + pro)
        pro_df['Efficiency'] = pro_df['Total Heat Output'] / pro_df['Consumed Energy']

        if pro in ['SolarThermal', 'ElectricHeater']:
            pro_df = pro_df.drop(['Consumed Energy', 'Efficiency'], axis=1)
    if not pro_df.empty:
        pro_df = pd.concat([time_h, pro_df], axis=1)
        if calc_temp_out:
            T_out = calc_t_out(prob, timesteps, exp_type)
            pro_df = pd.concat([pro_df, T_out], axis=1)
    return pro_df


def calc_t_out(prob, timesteps, exp_type='', model_type='', process='gen', port=''):
    if not exp_type:
        return pd.DataFrame(0.0, index=timesteps, columns=['T_out'])

    if 'CLT' in model_type:
        t_out_mpc = pd.DataFrame(0.0, index=timesteps, columns=['T_out'])
        t_lev = pd.Series(0.0, index=prob.heat_levels)
        v_out = pd.DataFrame(0.0, index=timesteps, columns=prob.heat_levels)
        for level in prob.heat_levels:
            if process == 'dem' or process == 'HeatDHW' or process == 'Heating':
                v = get_pro_data(prob, 'm_heat_dem_conv_in', timesteps, level, 'heat_levels', 'heat_demand_type_set')
                v_out[level] = v.loc[:, ['HeatDHW', 'Heating']].sum(axis=1)
            elif exp_type == 'HX':
                if level != prob.heat_levels[-1]:
                    lev = int(level.replace('Level', ''))
                    v_out['Level' + str(lev + 1)] = \
                        get_pro_data(prob, 'tau_heat_gen', timesteps, level, 'heat_levels', 'heat_gen_set')
            else:
                v_out[level] = get_pro_data(prob, 'm_heat_gen_conv_in', timesteps, level, 'heat_levels',
                                            'heat_gen_conv_set')
            t_lev[level] = prob.Heat_dict['T_low'] + prob.Heat_dict['Delta_T'] * int(level.replace('Level', ''))
        for t in timesteps:
            t_out_mpc.loc[t, 'T_out'] = sum(v_out.loc[t, :] / v_out.loc[t, :].sum() * t_lev)
    else:
        temp_out = get_entity(prob, 'heat_sto_temp_out')
        if process == 'dem':
            v = get_entity(prob, 'heat_pro_flow')
            v_tot = v.loc[1:, 'HeatDHW', 'top'] + v.loc[1:, 'Heating', 'top'].tolist()
            temp_out = (temp_out.loc[1:, 'Port0'] * v.loc[1:, 'HeatDHW', 'top'] / v_tot +
                        (temp_out.loc[1:, 'Port1'] * v.loc[1:, 'Heating', 'top'] / v_tot).tolist())
        t_out_mpc = pd.DataFrame(temp_out.loc[1:, port].tolist(), index=timesteps, columns=['T_out'])

    t_out_exp = pd.read_excel('Input/Input_validation.xlsx', sheet_name='HeatStorageValidation'
                              ).loc[:, exp_type + '_T_out [°C]'].dropna()

    df = t_out_mpc
    for i in range(1, int(prob.global_prop_dict.loc['dt', 'value'] * 3600)):
        df = df.append(t_out_mpc)
    df = df.sort_index()
    df = df.iloc[0:t_out_exp.index.max() + 1].set_index(t_out_exp.index).squeeze()
    mae = abs(df - t_out_exp).mean()
    rmse = ((df - t_out_exp) ** 2).mean() ** 0.5

    # import matplotlib.pyplot as plt
    # fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(8, 5))
    # ax.plot(df.index, df, label='Model')
    # ax.plot(t_out_exp.index, t_out_exp, label='Experiment')
    # ax.legend()
    # fig.show()

    return t_out_mpc, t_out_exp, mae, rmse


def calculate_t_sto(prob, t_sto):
    t_level = np.arange(prob.Heat_dict['T_low'], prob.Heat_dict['T_high'] + 1, prob.Heat_dict['Delta_T'])
    h_layer = np.arange(0, prob.Heat_dict['StorageSize'] + 1, prob.Heat_dict['StorageSize'] / 10)
    for i in t_sto.index:
        for l in range(0, 10):
            t_sto.loc[i, 'T_L' + str(l)] = 0
        h_volume = [0]
        for n in range(0, 10):
            h_volume.append(h_volume[-1] + t_sto.loc[i, 'Level' + str(n)])

        for l in range(0, 10):
            for n in range(0, 10):
                if h_volume[n] <= h_layer[l] and h_volume[n + 1] >= h_layer[l + 1]:
                    t_sto.loc[i, 'T_L' + str(l)] = t_level[n]
                elif h_volume[n] <= h_layer[l] <= h_volume[n + 1]:
                    t_sto.loc[i, 'T_L' + str(l)] += (h_volume[n + 1] - h_layer[l]) / h_layer[1] * t_level[n]
                elif h_volume[n] >= h_layer[l] and h_volume[n + 1] <= h_layer[l + 1]:
                    t_sto.loc[i, 'T_L' + str(l)] += (h_volume[n + 1] - h_volume[n]) / h_layer[1] * t_level[n]
                elif h_layer[l] <= h_volume[n] <= h_layer[l + 1]:
                    t_sto.loc[i, 'T_L' + str(l)] += (h_layer[l + 1] - h_volume[n]) / h_layer[1] * t_level[n]
    return t_sto



def get_timeseries_heat(prob, level, timesteps=None):
    """Return DataFrames of all timeseries referring to given commodity
        Args:
            - prob: a pyomo model
            - level: heat level
            - timesteps: optional list of timesteps, default: all modelled timesteps

        Returns:
            Dataframe with all results of the commodity
        """
    if timesteps is None:
        # default to all simulated timesteps
        timesteps = sorted(get_entity(prob, 'tm').index)
    else:
        timesteps = sorted(timesteps)  # implicit: convert range to list
    time_h = [t * prob.dt.value for t in timesteps]
    time_h = pd.DataFrame(time_h, index=timesteps, columns=['time [h]'])

    # PROCESS
    outflow_gen = get_pro_data(prob, 'm_heat_gen_conv_out', timesteps, level, 'heat_levels', 'heat_gen')
    outflow_dem = get_pro_data(prob, 'm_heat_dem_conv_out', timesteps, level, 'heat_levels', 'heat_demand_type')
    inflow_gen = get_pro_data(prob, 'm_heat_gen_conv_in', timesteps, level, 'heat_levels', 'heat_gen') * (-1)
    inflow_dem = get_pro_data(prob, 'm_heat_dem_conv_in', timesteps, level, 'heat_levels', 'heat_demand_type') * (-1)

    # STORAGE
    # group storage energies by commodity
    # select all entries with desired commodity co
    stored = get_entities(prob, ['heat_sto_con', 'heat_sto_conv_in', 'heat_sto_conv_out', 'heat_sto_loss'])
    mix = get_entities(prob, ['heat_sto_mix_in', 'heat_sto_mix_out_up', 'heat_sto_mix_out_down'])
    stored['heat_sto_mix'] = mix['heat_sto_mix_in'] - mix['heat_sto_mix_out_up'] - mix['heat_sto_mix_out_down']
    try:
        stored = stored.loc[timesteps].xs([level], level=['heat_levels'])
        stored.columns = ['Storage Level', 'Stored', 'Retrieved', 'Discharge', 'Mixing']
        stored.index = timesteps
        stored['Stored'] = -stored['Stored']
        stored = stored[['Stored', 'Retrieved', 'Storage Level', 'Discharge', 'Mixing']]
    except (KeyError, ValueError):
        stored = pd.DataFrame(0, index=timesteps, columns=['Stored', 'Retrieved', 'Storage Level'])

    pro_df = pd.concat([outflow_gen, outflow_dem, inflow_gen, inflow_dem, stored], axis=1).dropna(axis='columns')
    pro_df = pro_df.loc[:, (pro_df != 0).any(axis=0)]

    pro_df = pd.concat([time_h, pro_df], axis=1)
    return pro_df


def get_timeseries_heatstorage(prob, calc_mix=False, calc_temp=False, timesteps=None):
    """Return DataFrames of all timeseries referring to given commodity
        Args:
            - prob: a pyomo model
            - timesteps: optional list of timesteps, default: all modelled timesteps

        Returns:
            Dataframe with all results of the heat storage
        """
    if timesteps is None:
        # default to all simulated timesteps
        timesteps = sorted(get_entity(prob, 't').index)
    else:
        timesteps = sorted(timesteps)  # implicit: convert range to list

    if prob.mode['model_type'] == 'CLT':
        time_h = [t * prob.dt.value for t in timesteps]
        time_h = pd.DataFrame(time_h, index=timesteps, columns=['time [h]'])

        sto_df = pd.DataFrame(index=timesteps)
        stored = get_entity(prob, 'heat_sto_con')
        for level in prob.heat_levels:
            lev = (int(level.replace('Level', '')))
            DeltaT = (prob.Heat_dict["T_high"] - prob.Heat_dict["T_low"]) / (
                        prob.Heat_dict["NumberHeatLevels"] - 1) * lev
            if level == 'Level0':
                sto_df['SOC'] = stored.loc[timesteps].xs([level], level=['heat_levels']) * DeltaT
            else:
                sto_df['SOC'] = sto_df['SOC'] + stored.loc[timesteps].xs([level], level=['heat_levels']) * DeltaT
            sto_df[level] = stored.loc[timesteps].xs([level], level=['heat_levels'])
        e_sto_max = prob.Heat_dict['StorageSize'] * (prob.Heat_dict["T_high"] - prob.Heat_dict["T_low"])
        sto_df['SOC'] = sto_df['SOC'] / e_sto_max
        if calc_temp:
            T_sensor = [[0] * 10 for _ in sto_df.index.values]
            for t in sto_df.index:
                for i in range(0, 10):
                    h_min = max(0.0, 0.1 * i)
                    h_max = min(1.0, 0.1 * (i + 1))
                    sto = 0
                    for lev in prob.heat_levels:
                        T_lev = prob.Heat_dict['T_low'] + prob.Heat_dict['Delta_T'] * int(lev.replace('Level', ''))
                        sto_lev = sto_df.loc[t, lev] / prob.Heat_dict['StorageSize']
                        sto += sto_lev
                        if sto >= h_max and (sto - sto_lev) <= h_min:
                            T_sensor[t][i] += T_lev
                        elif sto >= h_max and (sto - sto_lev) > h_min and (sto - sto_lev) < h_max:
                            T_sensor[t][i] += T_lev * (h_max - (sto - sto_lev)) / (h_max - h_min)
                        elif sto < h_max and (sto - sto_lev) > h_min:
                            T_sensor[t][i] += T_lev * sto_lev / (h_max - h_min)
                        elif sto > h_min and (sto - sto_lev <= h_min):
                            T_sensor[t][i] += T_lev * (sto - h_min) / (h_max - h_min)
            T_sensor = pd.DataFrame(T_sensor, index=timesteps, columns=['T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8',
                                                                        'T9', 'T10'])
            sto_df = pd.concat([sto_df, T_sensor], axis=1)
        sto_df = pd.concat([time_h, sto_df], axis=1)
    else:
        time_h = [t * prob.dt.value for t in timesteps]
        sto_df = get_entity(prob, 'heat_sto_temp_layer').unstack(level=-1)
        sto_df['SOC'] = sto_df.mean(numeric_only=True, axis=1) / (prob.Heat_dict["T_high"] - prob.Heat_dict["T_low"])
        sto_df['time_h'] = time_h

    if calc_mix:
        mix = []
        for t in sto_df.index:
            n_min = sto_df.loc[:, sto_df.loc[t] != 0].iloc[t, 1:].index[0]
            T_min = prob.Heat_dict['T_low'] + prob.Heat_dict['Delta_T'] * int(n_min.replace('Level', ''))
            n_max = sto_df.loc[:, sto_df.loc[t] != 0].iloc[t, 1:].index[-1]
            T_max = prob.Heat_dict['T_low'] + prob.Heat_dict['Delta_T'] * int(n_max.replace('Level', ''))
            T_avg = sum((prob.Heat_dict['T_low'] + prob.Heat_dict['Delta_T'] * int(lev.replace('Level', ''))) *
                        sto_df.loc[t, lev]
                        for lev in prob.heat_levels) / prob.Heat_dict['StorageSize']
            M_mix = (1 / 2 * prob.Heat_dict['StorageSize'] * T_avg)
            M_stratified = (1 / 2 * prob.Heat_dict['StorageSize'] / (T_max - T_min) ** 2 *
                            ((T_max - T_avg) ** 2 * T_min +
                             (2 * T_max - T_min - T_avg) * (T_avg - T_min) * T_max))
            M_actual = (sum((prob.Heat_dict['T_low'] + prob.Heat_dict['Delta_T'] * int(lev.replace('Level', ''))) *
                            (sto_df.loc[t, 'Level0':lev].sum() - sto_df.loc[t, lev] / 2) /
                            prob.Heat_dict['StorageSize'] * sto_df.loc[t, lev]
                            for lev in prob.heat_levels))
            mix.append((M_stratified - M_actual) / (M_stratified - M_mix))
        sto_df['MIX'] = mix

    return sto_df


def get_pro_data(prob, name, timesteps, com, com_type='com', process='pro'):
    """
    :param prob: a pyomo model
    :param name: name of a Set, Param, Var, Constraint or Objective
    :param timesteps: list of timesteps
    :param com: a commodity name
    :param com_type: com or heat
    :param process: process name: pro, heat_gen, heat_dem
    :return: DataFrame
    """
    df = get_entity(prob, name)
    try:
        df = df.xs([com], level=[com_type]).loc[timesteps]
        df = df.unstack(level=process)
    except KeyError:
        df = pd.DataFrame(index=timesteps[1:])
    return df


def get_buy_sell_data(prob, name, timesteps, com, com_name):
    """
    :param prob: a pyomo model
    :param name: name of a Set, Param, Var, Constraint or Objective
    :param timesteps: list of timesteps
    :param com: a commodity name
    :param com_name: Buy or Sell
    :return: DataFrame
    """
    df = get_entity(prob, name)
    try:
        df = df.xs([com], level=['com']).loc[timesteps]
        df.index = timesteps
    except KeyError:
        df = pd.DataFrame(index=timesteps[1:])
    df.name = com_name
    return df


def get_entities(prob, names):
    """ Return one DataFrame with entities in columns and a common index.

    Works only on entities that share a common domain (set or set_tuple), which
    is used as index of the returned DataFrame.

    Args:
        prob: a Pyomo ConcreteModel prob
        names: list of entity names (as returned by list_entities)

    Returns:
        a Pandas DataFrame with entities as columns and domains as index
    """

    df = pd.DataFrame()
    for name in names:
        other = get_entity(prob, name)

        if df.empty:
            df = other.to_frame()
        else:
            index_names_before = df.index.names

            df = df.join(other, how='outer')

            if index_names_before != df.index.names:
                df.index.names = index_names_before

    return df


def get_entity(prob, name):
    """ Retrieve values (or duals) for an entity in a model prob.

    Args:
        prob: a Pyomo ConcreteModel prob
        name: name of a Set, Param, Var, Constraint or Objective

    Returns:
        a Pandas Series with domain as index and values (or 1's, for sets) of
        entity name. For constraints, it retrieves the dual values
    """
    # magic: short-circuit if problem contains a result cache
    if hasattr(prob, '_result') and name in prob._result:
        return prob._result[name].copy(deep=True)

    # retrieve entity, its type and its onset names
    try:
        entity = prob.__getattribute__(name)
        labels = _get_onset_names(entity)
    except AttributeError:
        return pd.Series(name=name)

    # extract values
    if isinstance(entity, pyomo.Set):
        if entity.dimen > 1:
            results = pd.DataFrame([v + (1,) for v in entity.value])
        else:
            # Pyomo sets don't have values, only elements
            results = pd.DataFrame([(v, 1) for v in entity.value])

        # for unconstrained sets, the column label is identical to their index
        # hence, make index equal to entity name and append underscore to name
        # (=the later column title) to preserve identical index names for both
        # unconstrained supersets
        if not labels:
            labels = [name]
            name = name + '_'

    elif isinstance(entity, pyomo.Param):
        if entity.dim() > 1:
            results = pd.DataFrame(
                [v[0] + (v[1],) for v in entity.iteritems()])
        elif entity.dim() == 1:
            results = pd.DataFrame(
                [(v[0], v[1]) for v in entity.iteritems()])
        else:
            results = pd.DataFrame(
                [(v[0], v[1].value) for v in entity.iteritems()])
            labels = ['None']

    elif isinstance(entity, pyomo.Expression):
        if entity.dim() > 1:
            results = pd.DataFrame(
                [v[0]+(v[1](),) for v in entity.iteritems()])
        elif entity.dim() == 1:
            results = pd.DataFrame(
                [(v[0], v[1]()) for v in entity.iteritems()])
        else:
            results = pd.DataFrame(
                [(v[0], v[1]()) for v in entity.iteritems()])
            labels = ['None']

    elif isinstance(entity, pyomo.Constraint):
        if entity.dim() > 1:
            # check whether all entries of the constraint have
            # an existing dual variable
            # in that case add to results
            results = pd.DataFrame(
                [key + (prob.dual[entity.__getitem__(key)],)
                 for (id, key) in entity.id_index_map().items()
                 if id in prob.dual._dict.keys()])
        elif entity.dim() == 1:
            results = pd.DataFrame(
                [(v[0], prob.dual[v[1]]) for v in entity.iteritems()])
        else:
            results = pd.DataFrame(
                [(v[0], prob.dual[v[1]]) for v in entity.iteritems()])
            labels = ['None']

    else:
        # create DataFrame
        if entity.dim() > 1:
            # concatenate index tuples with value if entity has
            # multidimensional indices v[0]
            results = pd.DataFrame(
                [v[0] + (v[1].value,) for v in entity.iteritems()])
        elif entity.dim() == 1:
            # otherwise, create tuple from scalar index v[0]
            results = pd.DataFrame(
                [(v[0], v[1].value) for v in entity.iteritems()])
        else:
            # assert(entity.dim() == 0)
            results = pd.DataFrame(
                [(v[0], v[1].value) for v in entity.iteritems()])
            labels = ['None']

    # check for duplicate onset names and append one to several "_" to make
    # them unique, e.g. ['sit', 'sit', 'com'] becomes ['sit', 'sit_', 'com']
    for k, label in enumerate(labels):
        if label in labels[:k] or label == name:
            labels[k] = labels[k] + "_"

    if not results.empty:
        # name columns according to labels + entity name
        results.columns = labels + [name]
        results.set_index(labels, inplace=True)

        # convert to Series
        results = results[name]
    else:
        # return empty Series
        results = pd.Series(name=name)
    return results


def _get_onset_names(entity):
    """ Return a list of domain set names for a given model entity

    Args:
        entity: a member entity (i.e. a Set, Param, Var, Objective, Constraint)
                of a Pyomo ConcreteModel object

    Returns:
        list of domain set names for that entity

    Example:
        >>> data = read_excel('mimo-example.xlsx')
        >>> model = create_model(data, range(1,25))
        >>> _get_onset_names(model.e_co_stock)
        ['t', 'sit', 'com', 'com_type']
    """
    # get column titles for entities from domain set names
    labels = []

    if isinstance(entity, pyomo.Set):
        if entity.dimen > 1:
            # N-dimensional set tuples, possibly with nested set tuples within
            if entity.domain:
                # retreive list of domain sets, which itself could be nested
                domains = entity.domain.set_tuple
            else:
                try:
                    # if no domain attribute exists, some
                    domains = entity.set_tuple
                except AttributeError:
                    # if that fails, too, a constructed (union, difference,
                    # intersection, ...) set exists. In that case, the
                    # attribute _setA holds the domain for the base set
                    try:
                        domains = entity._setA.domain.set_tuple
                    except AttributeError:
                        # if that fails, too, a constructed (union, difference,
                        # intersection, ...) set exists. In that case, the
                        # attribute _setB holds the domain for the base set
                        domains = entity._setB.domain.set_tuple

            for domain_set in domains:
                labels.extend(_get_onset_names(domain_set))

        elif entity.dimen == 1:
            if entity.domain:
                # 1D subset; add domain name
                labels.append(entity.domain.name)
            else:
                # unrestricted set; add entity name
                labels.append(entity.name)
        else:
            # no domain, so no labels needed
            pass

    elif isinstance(entity, (pyomo.Param, pyomo.Var, pyomo.Expression,
                    pyomo.Constraint, pyomo.Objective)):
        try:
            if entity.dim() > 0 and entity._index_set:
                labels = _get_onset_names(entity._index_set)
            else:
                # zero dimensions, so no onset labels
                pass
        except AttributeError:
            if entity.dim() > 0 and entity._index:
                labels = _get_onset_names(entity._index)
            else:
                # zero dimensions, so no onset labels
                pass

    else:
        raise ValueError("Unknown entity type!")

    return labels

