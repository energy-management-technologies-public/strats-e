import os
from pyomo.opt.base import SolverFactory
from datetime import datetime
from .model import create_model
# from .report import *
# from .plot import *
from .input import *
from .validation import *
# from .saveload import *
import random as random
import time
from pyomo import environ as env
# from pyomo.gdp import *


def run_ems(input_file, solver, objective, neos, slack=False):
    # create model
    prob = model(input_file, objective, solver, slack)

    # solve model and read results
    prob, result = solve_model(prob, solver, neos=neos, miqp=True)
    return prob, result


def model(input_file, objective, solver, slack, n_lev=0):
    data = read_input(input_file, slack)
    # validate_input(data)
    if 'CO2 limit' not in data['global_prop'].index:
        data['global_prop'].loc['CO2 limit', :] = np.inf

    dt = data['global_prop'].loc['dt', 'value']
    timesteps = range(data['global_prop'].loc['time offset', 'value'], data['global_prop'].loc['time offset', 'value'] +
                      data['global_prop'].loc['optimization length', 'value'] + 1)

    # create model
    start = time.time()
    prob = create_model(data, dt, timesteps, objective, solver, n_lev)
    prob.slack = slack
    end = time.time()
    prob.t_create_model = end - start
    return prob


def solve_model(prob, solver, neos=False, seed=random.randint(1, 2 ** 31 - 1), miqp=False):
    # solve model and read results
    if neos:
        optim = env.SolverManagerFactory('neos')
        result = optim.solve(prob, solver=solver, tee=True)
    else:
        optim = SolverFactory(solver)  # cplex, glpk, gurobi, ...
        optim = setup_solver(optim, seed, miqp, hasattr(prob, 'validation'))
        try:
            result = optim.solve(prob, tee=True)
            prob.t_solve_model = result.solver.Time
            prob.t_wall_solve_model = result.solver.wall_time
            result = str(result.solver.termination_condition)
        except ValueError:
            result = 'TimeOut'
    return prob, result


def prepare_result_directory(result_name):
    # """ create a time stamped directory within the result folder.
    #
    # Args:
    #     result_name: user specified result name
    #     input_path
    #     input_dir
    #     input_files
    #
    # Returns:
    #     a subfolder in the result folder
    #
    # """
    # timestamp for result directory
    now = datetime.now().strftime('%Y%m%dT%H%M')

    # create result directory if not existent
    result_dir = os.path.join('result', '{}-{}'.format(result_name, now))
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    return result_dir


def setup_solver(optim, seed, miqp, validation=False, logfile='solver.log'):
    if optim.name == 'gurobi':
        optim.set_options("logfile={}".format(logfile))
        if seed > 0:
            optim.set_options('seed=' + str(seed))
        if miqp:
            optim.set_options('NonConvex=2')
            # if validation:
            #     optim.set_options('timelimit=20')
        optim.set_options("mipgap=1e-2")
    elif optim.name == 'glpk':
        optim.set_options("log={}".format(logfile))
    elif optim.name == 'cplex':
        optim.set_options("log={}".format(logfile))
    else:
        print("Warning from setup_solver: no options set for solver "
              "'{}'!".format(optim.name))
    return optim
