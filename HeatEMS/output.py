import pandas as pd
import pyomo.environ as env


def output_2zone(prob):
    # P_HP, P_AUX, %DHW,
    df_output = pd.DataFrame(columns=['HP_mod', 'AUX_mod', 'HP3WV'])
    df_res = pd.DataFrame(columns=['HP_mod', 'AUX_mod', 'P_top', 'P_bottom', 'P_el_ASHP', 'P_el_PV', 'SOC_top', 'SOC_bottom'])
    pro = ['ASHP', 'ElectricHeater']
    for n in range(0, len(pro)):
        x = []
        for i in prob.tm * [pro[n]]:
            y = list(i)
            y.append(env.value(prob.e_pro_heat[i]))
            x.append(y)
        df_res.iloc[:, n] = pd.DataFrame(x).iloc[:, -1].tolist()

    zone = ['top', 'bottom']
    for n in range(0, len(zone)):
        x = []
        for i in prob.tm * [zone[n]]:
            y = list(i)
            y.append(env.value(prob.heat_gen[i]))
            x.append(y)
        df_res.iloc[:, 2 + n] = pd.DataFrame(x).iloc[:, -1].tolist()

    x = []
    for i in prob.tm * ['ASHP'] * ['Elec']:
        y = list(i)
        y.append(env.value(prob.e_pro_in[i]))
        x.append(y)
    df_res.iloc[:, 4] = pd.DataFrame(x).iloc[:, -1].tolist()

    x = []
    for i in prob.tm * ['PV'] * ['Elec']:
        y = list(i)
        y.append(env.value(prob.e_pro_out[i]))
        x.append(y)
    df_res.iloc[:, 5] = pd.DataFrame(x).iloc[:, -1].tolist()

    for n in range(0, len(zone)):
        x = []
        for i in prob.tm * [zone[n]]:
            y = list(i)
            y.append(env.value(prob.e_heat_sto[i]))
            x.append(y)
        df_res.iloc[:, 6 + n] = pd.DataFrame(x).iloc[:, -1].tolist()
    return
