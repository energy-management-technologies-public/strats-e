from datetime import datetime
from .input import *
from .StorageModels import *
from .BuySellPrice import *
from .MIQP_partload import *
from .TimeVarEff import *
from .modelhelper import *


def create_model(data, dt, timesteps, objective, solver, n_lev):
    m = pyomo_model_prep(data, timesteps, n_lev)  # preparing pyomo model
    m.name = 'HeatEMS'
    m.solver = solver
    m.created = datetime.now().strftime('%Y%m%dT%H%M')
    m._data = data

    # Parameters
    # ====
    # dt = spacing between timesteps. Required for storage equation that
    # converts between energy (storage content, e_sto_con) and power (all other
    # quantities that start with "e_")
    m.dt = pyomo.Param(
        initialize=dt,
        doc='Time step duration (in hours)')

    # import objective function information
    m.obj = pyomo.Param(
        initialize=objective,
        doc='Specification of minimized quantity, default: "cost"')

    # Sets
    # ====
    # Syntax: m.{name} = Set({domain}, initialize={values})
    # where name: set name
    #       domain: set domain for tuple sets, a cartesian set product
    #       values: set values, a list or array of element tuples

    # generate ordered time step sets
    m.t = pyomo.Set(
        initialize=m.timesteps,
        ordered=True,
        doc='Set of timesteps')

    # modelled (i.e. excluding init time step for storage) time steps
    m.tm = pyomo.Set(
        within=m.t,
        initialize=m.timesteps[1:],
        ordered=True,
        doc='Set of modelled timesteps')

    # commodity (e.g. solar, wind, coal...)
    indexlist = set()
    for key in m.commodity_dict["price"]:
        indexlist.add(tuple(key)[0])
    m.com = pyomo.Set(
        initialize=indexlist,
        doc='Set of commodities')

    # commodity type (i.e. SupIm, Demand, Env)
    indexlist = set()
    for key in m.commodity_dict["price"]:
        indexlist.add(tuple(key)[1])
    m.com_type = pyomo.Set(
        initialize=indexlist,
        doc='Set of commodity types')

    # process (e.g. CHP)
    indexlist = set()
    for key in m.process_dict["cap"]:
        indexlist.add(key)
    m.pro = pyomo.Set(
        initialize=indexlist,
        doc='Set of conversion processes')

    # cost_type (e.g. variable)
    m.cost_type = pyomo.Set(
        initialize=m.cost_type_list,
        doc='Set of cost types (hard-coded)')

    # tuple sets
    m.com_tuples = pyomo.Set(
        within=m.com * m.com_type,
        initialize=tuple(m.commodity_dict["price"].keys()),
        doc='Combinations of defined commodities, e.g. (Elec,Demand)')
    m.pro_tuples = pyomo.Set(
        within=m.pro,
        initialize=tuple(m.process_dict["cap"].keys()),
        doc='Combinations of possible processes, e.g. (CHP)')

    # commodity type subsets
    m.com_supim = pyomo.Set(
        within=m.com,
        initialize=commodity_subset(m.com_tuples, 'SupIm'),
        doc='Commodities that have intermittent (timeseries) input')
    m.com_demand = pyomo.Set(
        within=m.com,
        initialize=commodity_subset(m.com_tuples, 'Demand'),
        doc='Commodities that have a demand (implies timeseries)')
    m.com_env = pyomo.Set(
        within=m.com,
        initialize=commodity_subset(m.com_tuples, 'Env'),
        doc='Commodities that (might) have a maximum creation limit')

    # process input/output
    m.pro_input_tuples = pyomo.Set(
            within=m.pro * m.com,
            initialize=[(process, commodity)
                        for process in m.pro_tuples
                        for (pro, commodity) in tuple(m.r_in_dict.keys())
                        if process == pro],
            doc='Commodities consumed by process, e.g. (PV,Solar)')

    m.pro_output_tuples = pyomo.Set(
        within=m.pro * m.com,
        initialize=[(process, commodity)
                    for process in m.pro_tuples
                    for (pro, commodity) in tuple(m.r_out_dict.keys())
                    if process == pro],
        doc='Commodities produced by process by site, e.g. (PV,Elec)')

    # process tuples for maximum gradient feature
    m.pro_maxgrad_tuples = pyomo.Set(
        within=m.pro,
        initialize=[pro
                    for pro in m.pro_tuples
                    if m.process_dict['max-grad'][pro] < 1.0 / dt],
        doc='Processes with maximum gradient smaller than timestep length')

    # process tuples for partial feature
    m.pro_partial_tuples = pyomo.Set(
        within=m.pro,
        initialize=[process
                    for process in m.pro_tuples
                    for (pro, _) in tuple(m.r_in_min_fraction_dict.keys())
                    if process == pro],
        doc='Processes with partial input, e.g. CHP')

    m.pro_partial_input_tuples = pyomo.Set(
        within=m.pro * m.com,
        initialize=[(process, commodity)
                    for process in m.pro_partial_tuples
                    for (pro, commodity) in tuple(m.r_in_min_fraction_dict.keys())
                    if process == pro],
        doc='Commodities with partial input ratio, e.g. (CHP,Gas)')

    m.pro_partial_output_tuples = pyomo.Set(
        within=m.pro * m.com,
        initialize=[(process, commodity)
                    for process in m.pro_partial_tuples
                    for (pro, commodity) in tuple(m.r_out_min_fraction_dict.keys())
                    if process == pro],
        doc='Commodities with partial input ratio, e.g. (CHP,CO2)')

    # Variables
    # ====
    # costs
    m.costs = pyomo.Var(
        m.cost_type,
        within=pyomo.Reals,
        doc='Costs by type (EUR)')

    # commodity
    m.tau_pro = pyomo.Var(
        m.t, m.pro_tuples,
        within=pyomo.NonNegativeReals,
        doc='Power flow (kW) through process')
    m.e_pro_in = pyomo.Var(
        m.tm, m.pro_input_tuples,
        within=pyomo.NonNegativeReals,
        doc='Power flow of commodity into process (kW) per timestep')
    m.e_pro_out = pyomo.Var(
        m.tm, m.pro_output_tuples,
        within=pyomo.NonNegativeReals,
        doc='Power flow out of process (kW) per timestep')

    # Add additional features
    # called features are declared in distinct files in features folder
    if m.mode['sto']:
        m = add_storage(m)
    m = add_buy_sell_price(m)
    m = calc_offset_slope(m)
    if m.mode['tve']:
        m = add_time_variable_efficiency(m)
    else:
        m.pro_timevar_output_tuples = pyomo.Set(
            within=m.pro * m.com,
            doc='empty set needed for (partial) process output')

    if m.mode['model_type'] == 'CLT':
        m = add_clt_model(m)
    elif m.mode['model_type'] == 'CLV':
        m = add_clv_model(m)
    elif m.mode['model_type'] == '2zone':
        m = add_two_zone_storage_model(m)
    elif m.mode['model_type'] == 'mixed':
        m = add_mixed_storage_model(m)

    # Equation declarations
    # equation bodies are defined in separate functions, referred to here by
    # their name in the "rule" keyword.
    # ====
    # commodity
    m.res_vertex = pyomo.Constraint(
        m.tm, m.com_tuples,
        rule=res_vertex_rule,
        doc='storage + process + source + buy - sell == demand')
    m.res_env_step = pyomo.Constraint(
        m.tm, m.com_tuples,
        rule=res_env_step_rule,
        doc='environmental output per step <= commodity.maxperstep')
    m.res_env_total = pyomo.Constraint(
        m.com_tuples,
        rule=res_env_total_rule,
        doc='total environmental commodity output <= commodity.max')

    # process
    m.def_process_input = pyomo.Constraint(
        m.tm, m.pro_input_tuples - m.pro_partial_input_tuples,
        rule=def_process_input_rule,
        doc='process input = process throughput * input ratio')
    m.def_process_output = pyomo.Constraint(
        m.tm, (m.pro_output_tuples - m.pro_partial_output_tuples -
               m.pro_timevar_output_tuples),
        rule=def_process_output_rule,
        doc='process output = process throughput * output ratio')
    m.def_intermittent_supply = pyomo.Constraint(
        m.tm, m.pro_input_tuples,
        rule=def_intermittent_supply_rule,
        doc='process output = process capacity * supim timeseries')
    m.res_process_throughput_by_capacity = pyomo.Constraint(
        m.tm, m.pro_tuples,
        rule=res_process_throughput_by_capacity_rule,
        doc='process throughput <= total process capacity')
    m.res_process_maxgrad_lower = pyomo.Constraint(
        m.tm, m.pro_maxgrad_tuples,
        rule=res_process_maxgrad_lower_rule,
        doc='throughput may not decrease faster than maximal gradient')
    m.res_process_maxgrad_upper = pyomo.Constraint(
        m.tm, m.pro_maxgrad_tuples,
        rule=res_process_maxgrad_upper_rule,
        doc='throughput may not increase faster than maximal gradient')

    if m.mode['mip_partload']:
        m = miqp_partload(m)
    else:
        m = partload(m)

    # costs
    m.def_costs = pyomo.Constraint(
        m.cost_type,
        rule=def_costs_rule,
        doc='main cost function by cost type')

    # objective and global constraints
    if m.obj.value == 'cost':
        m.res_global_co2_limit = pyomo.Constraint(
            rule=res_global_co2_limit_rule,
            doc='total co2 commodity output <= Global CO2 limit')
        m.objective_function = pyomo.Objective(
            rule=cost_rule,
            sense=pyomo.minimize,
            doc='minimize(cost = sum of all cost types)')

    elif m.obj.value == 'CO2':
        m.res_global_cost_limit = pyomo.Constraint(
            rule=res_global_cost_limit_rule,
            doc='total costs <= Global cost limit')
        m.objective_function = pyomo.Objective(
            rule=co2_rule,
            sense=pyomo.minimize,
            doc='minimize total CO2 emissions')

    return m


# Constraints

# commodity

# vertex equation: calculate balance for given commodity and site;
# contains implicit constraints for process activity, import/export and
# storage activity (calculated by function commodity_balance);
def res_vertex_rule(m, tm, com, com_type):
    # environmental or supim commodities don't have this constraint (yet)
    if com in m.com_env:
        return pyomo.Constraint.Skip
    if com in m.com_supim:
        return pyomo.Constraint.Skip

    # helper function commodity_balance calculates balance from input to
    # and output from processes, storage and transmission.
    # if power_surplus > 0: production/storage/imports create net positive
    #                       amount of commodity com
    # if power_surplus < 0: production/storage/exports consume a net
    #                       amount of the commodity com
    power_surplus = - commodity_balance(m, tm, com)

    # Heat processes
    if m.mode['model_type'] == 'CLV':
        power_surplus += clv_heat_surplus(m, tm, com)
    elif m.mode['model_type'] == 'CLT':
        power_surplus += clt_heat_surplus(m, tm, com)
    elif m.mode['model_type'] == '2zone':
        power_surplus += two_zone_heat_surplus(m, tm, com)
    elif m.mode['model_type'] == 'mixed':
        power_surplus += mixed_heat_surplus(m, tm, com)

    # Buy and sell prices
    power_surplus += bsp_surplus(m, tm, com, com_type)

    # if com is a demand commodity, the power_surplus is reduced by the
    # demand value; no scaling by m.dt or m.weight is needed here, as this
    # constraint is about power (MW), not energy (MWh)
    if com in m.com_demand:
        try:
            power_surplus -= m.demand_dict[com][tm]
        except KeyError:
            pass

    return power_surplus == 0


# environmental commodity creation == - commodity_balance of that commodity
# used for modelling emissions (e.g. CO2) or other end-of-pipe results of
# any process activity;
# limit environmental commodity output per time step
def res_env_step_rule(m, tm, com, com_type):
    if com not in m.com_env:
        return pyomo.Constraint.Skip
    else:
        environmental_output = - commodity_balance(m, tm, com)
        return (environmental_output <=
                m.dt * m.commodity_dict['maxperhour'][(com, com_type)])


# limit environmental commodity output in total (scaled to annual
# emissions, thanks to m.weight)
def res_env_total_rule(m, com, com_type):
    if com not in m.com_env:
        return pyomo.Constraint.Skip
    else:
        # calculate total creation of environmental commodity com
        env_output_sum = 0
        for tm in m.tm:
            env_output_sum -= commodity_balance(m, tm, com)
        return env_output_sum <= m.commodity_dict['max'][(com, com_type)]


# process input power == process throughput * input ratio
def def_process_input_rule(m, tm, pro, com):
    return (m.e_pro_in[tm, pro, com] ==
            m.tau_pro[tm, pro] * m.r_in_dict[(pro, com)])


# process output power == process throughput * output ratio
def def_process_output_rule(m, tm, pro, com):
    return (m.e_pro_out[tm, pro, com] ==
            m.tau_pro[tm, pro] * m.r_out_dict[(pro, com)])


# process input (for supim commodity) = process capacity * timeseries
def def_intermittent_supply_rule(m, tm, pro, coin):
    if coin in m.com_supim:
        return (m.e_pro_in[tm, pro, coin] ==
                m.process_dict['cap'][pro] * m.supim_dict[coin][tm] * m.dt)
    else:
        return pyomo.Constraint.Skip


# process throughput <= process capacity
def res_process_throughput_by_capacity_rule(m, tm, pro):
    return m.tau_pro[tm, pro] <= m.dt * m.process_dict['cap'][pro]


# throughput may not decrease faster than maximal gradient
def res_process_maxgrad_lower_rule(m, t, pro):
    return (m.tau_pro[t - 1, pro] - m.process_dict['cap'][pro] * m.process_dict['max-grad'][pro] * m.dt <=
            m.tau_pro[t, pro])


# throughput may not increase faster than maximal gradient
def res_process_maxgrad_upper_rule(m, t, pro):
    return (m.tau_pro[t - 1, pro] + m.process_dict['cap'][pro] * m.process_dict['max-grad'][pro] * m.dt >=
            m.tau_pro[t, pro])


# Costs and emissions
def def_costs_rule(m, cost_type):
    # Calculate total costs by cost type.
    # Sums up process activity and capacity expansions and sums them in the cost types that are specified in the set
    # m.cost_type. To change or add cost types, add/change entries there and modify the if/elif cases in this function
    # accordingly.
    # Cost types are
    #  - Variables costs for usage of processes, storage and transmission.
    #  - Fuel costs for stock commodity purchase.

    if cost_type == 'Variable':
        cost = sum(m.tau_pro[tm, p] * m.process_dict['var-cost'][p]
                   for tm in m.tm
                   for p in m.pro_tuples)
        if m.mode['sto']:
            cost += storage_cost(m, cost_type)
        return m.costs[cost_type] == cost

    elif cost_type == 'Environmental':
        return m.costs[cost_type] == sum(- commodity_balance(m, tm, com) * m.commodity_dict['price'][(com, com_type)]
                                         for tm in m.tm
                                         for com, com_type in m.com_tuples
                                         if com in m.com_env)

    # Revenue and Purchase costs defined in BuySellPrice.py
    elif cost_type == 'Revenue':
        return m.costs[cost_type] == revenue_costs(m)

    elif cost_type == 'Purchase':
        return m.costs[cost_type] == purchase_costs(m)

    else:
        raise NotImplementedError("Unknown cost type.")


# total CO2 output <= Global CO2 limit
def res_global_co2_limit_rule(m):
    if math.isinf(m.global_prop_dict['value']['CO2 limit']):
        return pyomo.Constraint.Skip
    elif m.global_prop_dict['value']['CO2 limit'] >= 0:
        co2_output_sum = 0
        for tm in m.tm:
            # minus because negative commodity_balance represents creation of that commodity.
            co2_output_sum += (- commodity_balance(m, tm, 'CO2'))
        return co2_output_sum <= m.global_prop_dict['value']['CO2 limit']
    else:
        return pyomo.Constraint.Skip


# minimize(cost = sum of all cost types)
def cost_rule(m):
    return pyomo.summation(m.costs)


# total cost of one year <= Global cost limit
def res_global_cost_limit_rule(m):
    if math.isinf(m.global_prop_dict["value"]["Cost limit"]):
        return pyomo.Constraint.Skip
    elif m.global_prop_dict["value"]["Cost limit"] >= 0:
        return pyomo.summation(m.costs) <= m.global_prop_dict["value"]["Cost limit"]
    else:
        return pyomo.Constraint.Skip


# CO2 output in entire period <= Global CO2 budget
def co2_rule(m):
    co2_output_sum = 0
    for tm in m.tm:
        # minus because negative commodity_balance represents
        # creation of that commodity.
        co2_output_sum += (- commodity_balance(m, tm, 'CO2'))
    return co2_output_sum
