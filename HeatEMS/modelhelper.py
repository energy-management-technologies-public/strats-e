from HeatEMS.StorageModels.storage import *


def commodity_subset(com_tuples, type_name):
    """ Unique list of commodity names for given type.
    Args:
        com_tuples: a list of (commodity, commodity type) tuples
        type_name: a commodity type or a list of a commodity types
    Returns:
        The set (unique elements/list) of commodity names of the desired type
    """
    if type(type_name) is str:
        # type_name: ('Stock', 'SupIm', 'Env' or 'Demand')
        return set(com for com, com_type in com_tuples
                   if com_type == type_name)
    else:
        # type(type_name) is a class 'pyomo.base.sets.SimpleSet'
        # type_name: ('Buy')=>('Elec buy', 'Heat buy')
        return set((com, com_type) for com, com_type
                   in com_tuples if com in type_name)


def commodity_balance(m, tm, com):
    """Calculate commodity balance at given timestep.
    For a given commodity co and timestep tm, calculate the balance of
    consumed (to process/storage/transmission, counts positive) and provided
    (from process/storage/transmission, counts negative) commodity flow. Used
    as helper function in create_model for constraints on demand and stock
    commodities.
    Args:
        m: the model object
        tm: the timestep
        com: the commodity
    Returns
        balance: net value of consumed (positive) or provided (negative) power
    """
    balance = (sum(m.e_pro_in[(tm, process, com)]
                   # usage as input for process increases balance
                   for process in m.pro_tuples
                   if (process, com) in m.r_in_dict) -
               sum(m.e_pro_out[(tm, process, com)]
                   # output from processes decreases balance
                   for process in m.pro_tuples
                   if (process, com) in m.r_out_dict))
    if m.mode['sto']:
        balance += storage_balance(m, tm, com)

    return balance
