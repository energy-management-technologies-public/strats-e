"""
tbd
"""

import os
import shutil
import HeatEMS
# from HeatEMS import *

input_file = 'Input.xlsx'
input_dir = 'Input'
input_path = os.path.join(input_dir, input_file)
# rolling_horizon = False
result_name = 'Test'
# objective function
objective = 'cost'  # set either 'cost' or 'CO2' as objective

# Choose Solver (cplex, glpk, gurobi, ...)
neos = False
solver = 'gurobi'  # 'gurobi', 'conopt', 'cplex'

slack = False
os.environ['NEOS_EMAIL'] = 'd.zinsmeister@tum.de'
prob, result = HeatEMS.run_ems(input_path, solver, objective, neos)

if result != 'optimal':
    # If the optimization is infeasible, include slack variables
    slack = True
    prob, result = HeatEMS.run_ems(input_path, solver, objective, neos, slack)


if result == 'optimal' and result_name:
    result_dir = HeatEMS.prepare_result_directory(result_name)  # name + time stamp
    # copy input file to result directory
    try:
        shutil.copytree(input_path, os.path.join(result_dir, input_dir))
    except NotADirectoryError:
        shutil.copyfile(input_path, os.path.join(result_dir, input_file))
    # copy run file to result directory
    shutil.copy(__file__, result_dir)

    if slack:
        com = ['Slack_in', 'Elec', 'Heating', 'HeatDHW']
    else:
        com = ['Elec', 'Heating', 'HeatDHW']
    pro = prob.Heat_dict["HeatProcess"]
    HeatEMS.report(prob, result_dir, input_file, result_name, com, pro)

